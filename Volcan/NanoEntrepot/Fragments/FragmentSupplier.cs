﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Android.Content;
using Android.OS;
using Android.Support.V4.View;
using Android.Support.V7.Widget;
using Android.Views;
using Java.Interop;
using Volcan.NanoApp.Entrepot.Helpers;
using Volcan.NanoApp.Entrepot.Models;
using Volcan.NanoApp.Entrepot.Models.Adapter;
using SearchView = Android.Support.V7.Widget.SearchView;

namespace Volcan.NanoApp.Entrepot.Fragments {
    public class FragmentSupplier : BaseFragment {

        private RecyclerView _recyclerView;
        private SupplierAdapter _supplierAdapter;
        private List<Supplier> _suppliers;
        private SearchView _searchView;
        private int _pos;
        private Task _apiCall;

        public static FragmentSupplier NewInstance(string title) {
            var frag = new FragmentSupplier { Arguments = new Bundle(), Title = title };
            return frag;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            var view = inflater.Inflate(Resource.Layout.fragmentSupplier, container, false);
           

            
                _suppliers = new List<Supplier>();
                _apiCall = CallApi.Get<List<Supplier>>(MainActivity, $"inventoryreception/GetSuppliers?seasonId={CallApi.CurrentSeason.Id}", SuccessSupplier, ErrorSupplier, UpdateSupplier);

            

            _recyclerView = view.FindViewById<RecyclerView>(Resource.Id.recyclerViewSupplier);

            _supplierAdapter = new SupplierAdapter(_suppliers, Context, MainActivity.SupportFragmentManager);
            _supplierAdapter.ItemClick += OnItemClick;
            SetupRecyclerView(Context);


            return view;

        }

       
        private async void LoadShipmentVoucher() {
            await CallApi.Get<List<ShipmentVoucher>>(MainActivity, $"inventoryreception/GetShipmentVouchers?seasonId={CallApi.CurrentSeason.Id}&supplierId={_supplierAdapter.ItemClicked().Id}", SuccessShipmentVoucher, ErrorShipmentVoucher, UpdateSupplierVoucher);
        }

        //Invoker via l'adapter
        private void OnItemClick(object sender, int position) {
            _pos = position;
            if (_supplierAdapter.ItemClicked().ShipmentVouchers.Count == 0) {
                LoadShipmentVoucher();
            } else {
                _supplierAdapter.ItemClicked().ShipmentVouchers = new List<ShipmentVoucher>();
                _supplierAdapter.NotifyItemChanged(position);
            }
        }

        #region Callback Get Suppliers
        private void SuccessSupplier(object obj) {
            if (obj == null) {
                return;
            }
            foreach (var supplier in (List<Supplier>)obj) {
                supplier.ShipmentVouchers = new List<ShipmentVoucher>();
                _suppliers.Add(supplier);
            }
        }
        private void UpdateSupplier() {
            _supplierAdapter.ResetItems();
            _supplierAdapter.NotifyDataSetChanged();
        }

        private void UpdateSupplierVoucher() {
            _supplierAdapter.NotifyItemChanged(_pos);
        }

        private void ErrorSupplier(object obj, string msg) {
            MainActivity.CallDialogOkCancel(Resources.GetString(Resource.String.titleError), $"{Resources.GetString(Resource.String.readSupplier)} \n \n {msg}", Resources.GetString(Resource.String.btnRetry), Resources.GetString(Resource.String.btnCancel), Positive, Negative);
        }

        #endregion

        #region Callback DialogAlert Erreur Get Suppliers

        private void Positive() {
            _apiCall = CallApi.Get<List<Supplier>>(MainActivity, $"inventoryreception/GetSuppliers?seasonId={CallApi.CurrentSeason.Id}", SuccessSupplier, ErrorSupplier, UpdateSupplier);
        }

        private void Negative() {
            MainActivity.HideLoader();
        }

        #endregion

        #region Callback Get ShipmentVouchers

        private void SuccessShipmentVoucher(object obj) {
            if (obj == null) {
                return;
            }
            _supplierAdapter.ItemClicked().ShipmentVouchers = (List<ShipmentVoucher>)obj;
            
        }

        private void ErrorShipmentVoucher(object obj, string msg) {
            MainActivity.CallDialogOkCancel(Resources.GetString(Resource.String.titleError), $"{Resources.GetString(Resource.String.readShipmentVoucher)}  \n \n {msg}", Resources.GetString(Resource.String.btnRetry), Resources.GetString(Resource.String.btnCancel), PositiveShipment, Negative);
        }

        #endregion

        #region Callback DialogAlert Erreur Get shpimentVoucher

        private void PositiveShipment() {
            LoadShipmentVoucher();
        }

        #endregion

        private void SetupRecyclerView(Context context) {
            var layoutManager = new LinearLayoutManager(context);
            _recyclerView.SetLayoutManager(layoutManager);
            _recyclerView.SetItemAnimator(new DefaultItemAnimator());
            _recyclerView.SetAdapter(_supplierAdapter);
        }

        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater) {
            base.OnCreateOptionsMenu(menu, inflater);
            menu.FindItem(Resource.Id.action_search).SetVisible(true);
            var item = menu.FindItem(Resource.Id.action_search);
            SetSearch(item);
        }

        private void SetSearch(IMenuItem item) {
            var searchView = MenuItemCompat.GetActionView(item);
            _searchView = searchView.JavaCast<SearchView>();

            _searchView.QueryTextChange += (s, e) => _supplierAdapter.Filter.InvokeFilter(e.NewText);
            _searchView.QueryTextSubmit += (s, e) => {
                _searchView.ClearFocus();
                e.Handled = true;
            };
        }

    }


}

