﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using Volcan.NanoApp.Entrepot.Models;
using Volcan.NanoApp.Entrepot.Models.Adapter;
using Android.Content;
using Android.Graphics;
using Android.Support.V4.Content;
using Android.Support.V4.View;
using Android.Views.Animations;
using Android.Widget;
using Java.Interop;
using Volcan.NanoApp.Entrepot.Helpers;
using SearchView = Android.Support.V7.Widget.SearchView;

namespace Volcan.NanoApp.Entrepot.Fragments {
    public class FragmentCustomer : BaseFragment {

        private RecyclerView _recyclerView;
        private CustomerAdapter _customerAdapter;
        private List<Customer> _customers;
        private SearchView _searchView;  

        private Customer _selectedCustomer;
        private Button _assignButton;
        private Task _apiCall;
        private string _customerId;

        private int _pos;
       // private Button _btnDeleteFiles;

        public static FragmentCustomer NewInstance(string title) {
          
            var frag = new FragmentCustomer { Arguments = new Bundle(), Title = title };
            return frag;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            var view = inflater.Inflate(Resource.Layout.fragmentCustomer, container, false);
            _assignButton = view.FindViewById<Button>(Resource.Id.btnAssignment);

            //_btnDeleteFiles = view.FindViewById<Button>(Resource.Id.btnDeleteFile);
            //_btnDeleteFiles.Click += delegate {
            //    foreach (var customer in _customers) {
            //        StorageHelper.DeleteFile($"CustomerId_{customer.Id}_SlipId_");
            //    }
            //};

            if (_customers == null) {
                _customers = new List<Customer>();
                _apiCall = CallApi.Get<CustomerIn>(MainActivity, $"InventoryPackaging/GetCustomers?seasonId={CallApi.CurrentSeason.Id}&cmdType={CallApi.CurrentCustomersTypes.Id}", SuccessCustomer, ErrorCustomer, UpdateCustomer);
            } else {
                FindFreeCustomer();
            }

            _customerAdapter = new CustomerAdapter(_customers, Context, MainActivity.SupportFragmentManager, MainActivity);
            _customerAdapter.ItemClick += OnItemClick;

            // Get our RecyclerView layout:
            _recyclerView = view.FindViewById<RecyclerView>(Resource.Id.recyclerViewCustomer);

            SetupRecyclerView(Context);


            return view;
        }


        #region LoadingCustomer
        private void SuccessCustomer(object obj) {
            if (obj == null) {
                return;
            }
            var newobj = (CustomerIn)obj;
            foreach (var customer in newobj.Customers) {
                customer.Deliveries = new List<Delivery>();
                _customers.Add(customer);
            }
           
        }

        private void ErrorCustomer(object obj, string msg) {
            var errorMessage = msg;
            if (obj != null) {
                var newObj = (CustomerIn)obj;
                var newmsg = Helper.FindGoodErrorMessage(newObj.Error.Code);

                errorMessage = !string.IsNullOrEmpty(newmsg) ? newmsg : msg;
            }
            
            MainActivity.CallDialogOkCancel(Resources.GetString(Resource.String.titleError), $"{Resources.GetString(Resource.String.readCustomers)} \n \n {errorMessage}", Resources.GetString(Resource.String.btnRetry), Resources.GetString(Resource.String.btnCancel), Positive, Negative);
        }

        private void Positive() {
            _apiCall = CallApi.Get<CustomerIn>(MainActivity, $"InventoryPackaging/GetCustomers?seasonId={CallApi.CurrentSeason.Id}&cmdType={CallApi.CurrentCustomersTypes.Id}", SuccessCustomer, ErrorCustomer, UpdateCustomer);
        }
        private void Negative() {
            MainActivity.HideLoader();
        }

        private void UpdateCustomer() {
            _customerAdapter.ResetItems();
            _customerAdapter.NotifyDataSetChanged();
            FindFreeCustomer();
        }

        private void GetPackingSlips() {
            _apiCall = CallApi.Get<PackingSlipsIn>(MainActivity, $"InventoryPackaging/GetCustomerPackingSlips?seasonId={CallApi.CurrentSeason.Id}&CustomerId={_customerId}&cmdType={CallApi.CurrentCustomersTypes.Id}", SuccessPackingSlips, ErrorPackingSlips, UpdatePackingSlips);
        }

        private void SuccessPackingSlips(object obj) {
            if (obj == null) {
                return;
            }

            var newobj = (PackingSlipsIn)obj;
           
            foreach (var newobjPackingSlip in newobj.PackingSlips) {
                newobjPackingSlip.CustomerId = _customerAdapter.ItemClicked().Id;
                _customerAdapter.ItemClicked().Deliveries.Add(newobjPackingSlip);
            }  
        }

        private void ErrorPackingSlips(object obj, string msg) {
            var errorMessage = msg;
            if (obj != null) {
                var newObj = (CustomerIn)obj;
                var newmsg = Helper.FindGoodErrorMessage(newObj.Error.Code);

                errorMessage = !string.IsNullOrEmpty(newmsg) ? newmsg : msg;
            }

            MainActivity.CallDialogOkCancel(Resources.GetString(Resource.String.titleError), $"{Resources.GetString(Resource.String.readDelivery)} \n \n {errorMessage}", Resources.GetString(Resource.String.btnRetry), Resources.GetString(Resource.String.btnCancel), PositivePackingSlip, Negative);
        }

        private void UpdatePackingSlips() {
            _customerAdapter.NotifyItemChanged(_pos);
        }

        private void PositivePackingSlip() {
            _apiCall = CallApi.Get<PackingSlipsIn>(MainActivity, $"InventoryPackaging/GetCustomerPackingSlips?seasonId={CallApi.CurrentSeason.Id}&CustomerId={_customerId}&cmdType={CallApi.CurrentCustomersTypes.Id}", SuccessPackingSlips, ErrorPackingSlips, UpdatePackingSlips);
        }


        private void FindFreeCustomer() {

            if (_customers.Any(it => it.EmployeIds.Count > 0)) {
                _assignButton.Click += AutomaticAssigment;
            } else {
                _assignButton.SetBackgroundColor(new Color(ContextCompat.GetColor(Context, Resource.Color.grisNanoTransparent)));
            }
        }
        #endregion


        #region GoScanDelivery

        private async void CheckEmployeId() {
            await CallApi.Get<EmployeIn>(MainActivity, $"/InventoryPackaging/GetUserForShipment?customerId={_selectedCustomer.Id}&seasonId={CallApi.CurrentSeason.Id}&cmdType={CallApi.CurrentCustomersTypes.Id}", SuccessCheckEmploye, ErrorCheckEmploye, UpdateCheckEmploye);
        }

        private void SuccessCheckEmploye(object obj) {
            if (obj == null) {
                return;
            }
            var newobj = (EmployeIn)obj;
            foreach (var newobjEmployeId in newobj.EmployeIds) {
                if (!_selectedCustomer.EmployeIds.Contains(newobjEmployeId)) {
                    _selectedCustomer.EmployeIds.Add(newobjEmployeId);
                }
            }
        }

        private void ErrorCheckEmploye(object obj, string msg) {
            var errorMessage = msg;
            if (obj != null) {
                var newObj = (EmployeIn)obj;
                var newmsg = Helper.FindGoodErrorMessage(newObj.Error.Code);
                errorMessage = !string.IsNullOrEmpty(newmsg) ? newmsg : msg;
            }
            
            MainActivity.CallDialogOkCancel(Resources.GetString(Resource.String.titleError), $"{Resources.GetString(Resource.String.readEmploye)} \n \n {errorMessage}", Resources.GetString(Resource.String.btnRetry), Resources.GetString(Resource.String.btnCancel), PositiveRetryCheckEmploye, Negative);
        }

        private void PositiveRetryCheckEmploye() {
            CheckEmployeId();
        }

        private void UpdateCheckEmploye() {
            ValidateCurrentEmploye();

        }
        private void AutomaticAssigment(object sender, EventArgs e) {

            _selectedCustomer = _customers.FirstOrDefault(it => it.EmployeIds.Count == 0 && !it.IsIndividualDelivery);
            if (_selectedCustomer == null) { return; }
            CheckEmployeId();
        }

        private void OnItemClick(object sender, int position) {
            _pos = position;
            _selectedCustomer = _customerAdapter.ItemClicked();

            if (_selectedCustomer.IsIndividualDelivery) {
                _customerId = _selectedCustomer.Id;

                if (_selectedCustomer.Deliveries.Count > 0) {
                    _selectedCustomer.Deliveries = new List<Delivery>();
                    _customerAdapter.NotifyItemChanged(position);
                    return;
                }

                GetPackingSlips();
                return;
            }

            CheckEmployeId();
        }

        private void ValidateCurrentEmploye() {
            if (_selectedCustomer.EmployeIds.Count > 0 && !_selectedCustomer.EmployeIds.Contains(CallApi.CurrentUser.OasisId)) {
                MainActivity.CallDialogPermission(Resources.GetString(Resource.String.employeAlreadyThereNeedApproval), PositiveCurrentEmploye, null);

                return;
            }
            if (!_selectedCustomer.EmployeIds.Contains(CallApi.CurrentUser.OasisId)) {
                ChangeCurrentEmploye();
                return;
            }
            ChangeFragment(_selectedCustomer);

        }

        private async void ChangeCurrentEmploye() {
            var userOut = new UserShipmentOut { CustomerId = _selectedCustomer.Id,  EmployeId = CallApi.CurrentUser.OasisId, SeasonId = CallApi.CurrentSeason.Id, CmdType = CallApi.CurrentCustomersTypes.Id };

            await CallApi.Put<UserShipmentOut, UserShipmentOut>(MainActivity, $"InventoryPackaging/SaveUserForShipment", userOut, SuccessChangeEmploye, ErrorChangeEmploye, UpdateChangeEmploye);
        }

        private void SuccessChangeEmploye(object obj) {
            if (obj == null) {
                return;
            }
            var newobj = (UserShipmentOut)obj;

            _selectedCustomer.EmployeIds.Add(newobj.EmployeId);
           
        }

        private void ErrorChangeEmploye(object obj, string msg) {

            MainActivity.CallDialogOkCancel(Resources.GetString(Resource.String.titleError), $"{Resources.GetString(Resource.String.updateEmploye)} \n \n {msg}", Resources.GetString(Resource.String.btnRetry), Resources.GetString(Resource.String.btnCancel), PositiveChangeEmploye, Negative);
        }

        private void PositiveChangeEmploye() {
            ChangeCurrentEmploye();
        }
        private void UpdateChangeEmploye() {
            //_customerAdapter.NotifyDataSetChanged();
            ChangeFragment(_selectedCustomer);
        }
        private void PositiveCurrentEmploye() {
            ChangeCurrentEmploye();  
        }

        private void ChangeFragment(Customer customer) {
            Android.Support.V4.App.Fragment fragment = FragmentScanDelivery.NewInstance($"{customer.Name} | {Resources.GetString(Resource.String.customers)}", customer.Id, null);

            MainActivity.ReplaceFragment(fragment, "ScanDelivery");
        }

        #endregion

        private void SetupRecyclerView(Context context) {
            var layoutManager = new LinearLayoutManager(context);
            _recyclerView.SetLayoutManager(layoutManager);
            _recyclerView.SetItemAnimator(new DefaultItemAnimator());
            _recyclerView.SetAdapter(_customerAdapter);
        }

        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater) {
            base.OnCreateOptionsMenu(menu, inflater);
            menu.FindItem(Resource.Id.action_search).SetVisible(true);
            var item = menu.FindItem(Resource.Id.action_search);
            SetSearch(item);
        }

        private void SetSearch(IMenuItem item) {
            var searchView = MenuItemCompat.GetActionView(item);
            _searchView = searchView.JavaCast<SearchView>();

            _searchView.QueryTextChange += (s, e) => _customerAdapter.Filter.InvokeFilter(e.NewText);
            _searchView.QueryTextSubmit += (s, e) => {
                _searchView.ClearFocus();
                e.Handled = true;
            };
        }

    }
}