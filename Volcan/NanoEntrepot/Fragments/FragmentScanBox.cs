﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Android.Content;
using Android.OS;
using Android.Support.V4.View;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Clans.Fab;
using Java.Interop;
using Newtonsoft.Json;
using Volcan.NanoApp.Entrepot.Helpers;
using Volcan.NanoApp.Entrepot.Models;
using Volcan.NanoApp.Entrepot.Models.Adapter;
using SearchView = Android.Support.V7.Widget.SearchView;

namespace Volcan.NanoApp.Entrepot.Fragments {
    public class FragmentScanBox : BaseFragment, View.IOnClickListener {
        private RecyclerView _recyclerView;
        private ScannedBoxAdapter _scannedBoxAdapter;
        private List<ScannedDelivery> _scannedStyles;
        private Box _currentBox;
        private ScannedDelivery _selectedStyle;
        private SearchView _searchView;
        private FloatingActionMenu _fabDel;
        private EditText _scanUpc;
        private ImageView _imgView;
        private TextView _noStyle;
        private TextView _size;
        private TextView _color;

        private StoragePackaging _storagePackaging;
        private long _timestampBox;
        private string _customerId;
        private string _slipId;


        private Task _apiCall;
        private Task _apiCallSendEmail;

        private List<ScannedDelivery> _lstItemNeedPermission;

        public static FragmentScanBox NewInstance(string title, StoragePackaging storagePackaging, long timestampBox, string customerId, string slipId) {
            var frag = new FragmentScanBox { Arguments = new Bundle(), Title = title };
            frag.Arguments.PutString("StoragePackaging", JsonConvert.SerializeObject(storagePackaging));
            frag.Arguments.PutLong("TimestampBox", timestampBox);
            frag.Arguments.PutString("CustomerId", customerId);
            frag.Arguments.PutString("SlipId", slipId);
            return frag;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            var view = inflater.Inflate(Resource.Layout.fragmentScanBox, container, false);

            if (_storagePackaging == null) {
                _storagePackaging = JsonConvert.DeserializeObject<StoragePackaging>(Arguments.GetString("StoragePackaging"));
            }



            _timestampBox = Arguments.GetLong("TimestampBox");

            _customerId = Arguments.GetString("CustomerId");
            _slipId = Arguments.GetString("SlipId");

            _imgView = view.FindViewById<ImageView>(Resource.Id.imgStyleBox);
            _noStyle = view.FindViewById<TextView>(Resource.Id.titleNoStyleBox);
            _size = view.FindViewById<TextView>(Resource.Id.titleSizeBox);
            _color = view.FindViewById<TextView>(Resource.Id.titleColorBox);
            _fabDel = view.FindViewById<FloatingActionMenu>(Resource.Id.deleteStyleBox);
            _scanUpc = view.FindViewById<EditText>(Resource.Id.scanUPCBox);
            _fabDel.Visibility = ViewStates.Invisible;
            _fabDel.IconAnimated = false;
            _fabDel.SetOnMenuButtonClickListener(this);
            view.SetOnClickListener(this);

            _currentBox = _storagePackaging.InternalBoxes.Find(it => it.Timestamp == _timestampBox);

            if (_currentBox.IsClosed && _currentBox.ScannedStyles.Count == 0) {
                _apiCall = CallApi.Get<BoxIn>(MainActivity, $"InventoryPackaging/GetPackingBoxStyles?boxId={_currentBox.Id}&cmdType={CallApi.CurrentCustomersTypes.Id}", SuccessBoxStyles, ErrorBoxStyles, UpdateBoxStyles);
            }

            _scannedStyles = _storagePackaging.InternalBoxes.Find(it => it.Timestamp == _timestampBox).ScannedStyles;
            if (_scannedStyles.Count > 0) {
                _selectedStyle = _scannedStyles[0];
                ShowImageSide();
            }

            _scanUpc.SetCursorVisible(false);
            _scanUpc.RequestFocus();

            _scanUpc.KeyPress += (sender, e) => {
                if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter) {
                    GunClick();
                    _scanUpc.Text = "";
                } else {
                    e.Handled = false;
                }
            };


            _scannedBoxAdapter = new ScannedBoxAdapter(Context, _scannedStyles);
            _scannedBoxAdapter.ItemClick += OnItemClick;

            _recyclerView = view.FindViewById<RecyclerView>(Resource.Id.recyclerViewScannedShipmentVoucher);

            SetupRecyclerView(Context);
            return view;
        }

        private void SuccessBoxStyles(object obj) {
            if (obj == null) { return; }

            var newobj = (BoxIn)obj;
            if (newobj.Box.Styles == null || newobj.Box.Styles.Count == 0) {

                return;
            }
            foreach (var style in newobj.Box.Styles) {

                _currentBox.ScannedStyles.Add(new ScannedDelivery {
                    Style = new StyleDelivery {
                        Color = style.Color,
                        Description = style.Description,
                        ExpectedTotal = style.ExpectedTotal,
                        Total = style.Total,
                        Id = style.Id,
                        ImagePath = style.ImagePath,
                        Name = style.Name,
                        Size = style.Size,
                        Upc = style.Upc
                    }
                });
            }

            WriteStoragePackage();
            _selectedStyle = _currentBox.ScannedStyles[0];
            _selectedStyle.IsSelected = true;
        }

        private void ErrorBoxStyles(object obj, string msg) {
            var errorMessage = msg;
            if (obj != null) {
                var newObj = (BoxIn)obj;
                var newmsg = Helper.FindGoodErrorMessage(newObj.Error.Code);

                errorMessage = !string.IsNullOrEmpty(newmsg) ? newmsg : msg;
            }
           
            MainActivity.CallDialogOkCancel(Resources.GetString(Resource.String.titleError), $"{Resources.GetString(Resource.String.readStylesBox)} \n \n  {errorMessage}", Resources.GetString(Resource.String.btnRetry), Resources.GetString(Resource.String.btnCancel), Positive, Negative);
        }

        private void Positive() {
            _apiCall = CallApi.Get<BoxIn>(MainActivity, $"InventoryPackaging/GetPackingBoxStyles?boxId={_currentBox.Id}&cmdType={CallApi.CurrentCustomersTypes.Id}", SuccessBoxStyles, ErrorBoxStyles, UpdateBoxStyles);
        }

        private void Negative() {
            MainActivity.HideLoader();
        }

        private void UpdateBoxStyles() {

            ChangeSelected();
            ShowImageSide();
            _scannedBoxAdapter.ResetItems();
            _scannedBoxAdapter.NotifyDataSetChanged();
        }

        private void GunClick() {
            _currentBox.IsClosed = false;
            var pos = _storagePackaging.InternalScannedDeliveries.FindIndex(it => it.Style?.Upc == _scanUpc.Text);
            if (pos > -1) {
                var item = _storagePackaging.InternalScannedDeliveries[pos];

                if (item.Style.Total == 0) {
                    MainActivity.CallDialogOkCancel(Resources.GetString(Resource.String.missingItem), $"{Resources.GetString(Resource.String.thisUpcCode)} {_scanUpc.Text} {Resources.GetString(Resource.String.notOnRack)} \n \n {Resources.GetString(Resource.String.pleaseScanAgain)}", Resources.GetString(Resource.String.btnOk), Resources.GetString(Resource.String.btnCancel), null, null);
                    return;
                }
                if (item.Style.IsNeedPermission) {
                    _currentBox.IsNeedPermission = true;
                }
                AddItemInBox(item);
            } else {
                //get upc
                MainActivity.CallDialogOkCancel(Resources.GetString(Resource.String.missingItem), $"{Resources.GetString(Resource.String.thisUpcCode)} {_scanUpc.Text} {Resources.GetString(Resource.String.notOnSlip)} \n \n {Resources.GetString(Resource.String.pleaseScanAgain)}", Resources.GetString(Resource.String.btnOk), Resources.GetString(Resource.String.btnCancel), null, null);
            }
        }

        private void AddItemInBox(ScannedDelivery item) {
            var internalStyle = _scannedStyles.Find(it => it.Style.Upc == item.Style.Upc);
            if (internalStyle == null) {
                _scannedStyles.Insert(0, new ScannedDelivery() {
                    Style = new StyleDelivery {
                        Color = item.Style.Color,
                        Description = item.Style.Description,
                        ExpectedTotal = item.Style.RealTotal,
                        Id = item.Style.Id,
                        ImagePath = item.Style.ImagePath,
                        Name = item.Style.Name,
                        Size = item.Style.Size,
                        Upc = item.Style.Upc,
                        Total = 0,
                        IsNeedPermission = item.Style.IsNeedPermission
                    },
                    IsSelected = true
                });
                internalStyle = _scannedStyles[0];
            }
            _selectedStyle = internalStyle;
            ChangeSelected();
            ShowImageSide();
            internalStyle.Style.Total++;
            item.Style.Total--;
            item.Style.BoxTotal++;
            _scannedBoxAdapter.NotifyDataSetChanged();
            WriteStoragePackage();

            //Put at the end list Scan Delivery
            if (item.Style.RealTotal <= item.Style.BoxTotal) {
                var pos = _storagePackaging.InternalScannedDeliveries.FindIndex(it => it.Style?.Upc == _scanUpc.Text);
                _storagePackaging.InternalScannedDeliveries.RemoveAt(pos);
                item.IsSelected = false;
                _storagePackaging.InternalScannedDeliveries.Add(item);
                WriteStoragePackage();
            }

        }
        private void PositivePermission() {
            _currentBox.IsNeedPermission = false;
            SendEmail();

            //CloseBox
            MainActivity.CallDialogDimension(SucessDimension, _currentBox.Dimension);
        }

        private void SendEmail() {
            var styles = new List<Style>();
            foreach (var scannedDelivery in _lstItemNeedPermission) {
                styles.Add(scannedDelivery.Style);
            }
            var stylesOut = new StylesOut {
                SeasonId = CallApi.CurrentSeason.Id,
                CustomerId = _customerId,
                ShipmentId = _slipId,
                Styles = styles
            };
            _apiCallSendEmail = CallApi.Put<StylesOut, StylesOut>(MainActivity, $"email/PackingBoxWithItemsNotOnBill", stylesOut, SuccessEmail, ErrorEmail, UpdateEmail);
        }

        private void SuccessEmail(object obj) {
            if (obj == null) {
                return;
            }
        }

        private void ErrorEmail(object obj, string msg) {
            var errorMessage = msg;
            if (obj != null) {
                var newObj = (StylesOut)obj;
                var newmsg = Helper.FindGoodErrorMessage(newObj.Error.Code);

                errorMessage = !string.IsNullOrEmpty(newmsg) ? newmsg : msg;
            }
            
            MainActivity.CallDialogOkCancel(Resources.GetString(Resource.String.titleError), $"{Resources.GetString(Resource.String.emailError)} \n \n {errorMessage}", Resources.GetString(Resource.String.btnRetry), Resources.GetString(Resource.String.btnCancel), PositiveRetryEmail, Negative);
        }

        private void PositiveRetryEmail() {
            SendEmail();
        }

        private void UpdateEmail() {
            foreach (var scannedDelivery in _lstItemNeedPermission) {
                scannedDelivery.Style.IsNeedPermission = false;
            }
            WriteStoragePackage();
            _scannedBoxAdapter.NotifyDataSetChanged();
        }

        private void ChangeSelected() {
            if (_scannedStyles.Any(it => it.IsSelected && _selectedStyle.Style.Upc != it.Style.Upc)) {
                var selecteds = _scannedStyles.Where(it => it.IsSelected && _selectedStyle.Style.Upc != it.Style.Upc);

                foreach (var scannedShipmentVoucher in selecteds) {
                    var pos = _scannedStyles.IndexOf(scannedShipmentVoucher);
                    scannedShipmentVoucher.IsSelected = false;
                    _scannedBoxAdapter.NotifyItemChanged(pos);
                }
            }
        }

        private void OnItemClick(object sender, int position) {
            var adapter = (ScannedBoxAdapter)sender;
            _selectedStyle = adapter.GetSelectedItem();
            adapter.NotifyDataSetChanged();

            if (_selectedStyle != null) {
                ShowImageSide();
            } else {
                HideImageSide();
            }
        }

        private void HideImageSide() {
            _imgView.SetImageBitmap(null);
            _noStyle.Text = "";
            _size.Text = "";
            _color.Text = "";
            _fabDel.Visibility = ViewStates.Invisible;
        }

        private void ShowImageSide() {
            HideImageSide();

            if (_selectedStyle.Style != null) {
                if (_selectedStyle.Style.ImagePath != "") {
                    var imageBitmap = Helper.GetImageBitmapFromUrl(_selectedStyle.Style.ImagePath, MainActivity);
                    if (imageBitmap != null) {
                        _imgView.SetImageBitmap(imageBitmap);
                    }
                }

                _noStyle.Text = _selectedStyle.Style.Name;
                _size.Text = _selectedStyle.Style.Size;
                _color.Text = _selectedStyle.Style.Color;
                _fabDel.Visibility = ViewStates.Visible;
            } else {
                _noStyle.Text = $"Code UPC : {_scanUpc.Text}";
            }
            _fabDel.Visibility = ViewStates.Visible;
        }

        private void SetupRecyclerView(Context context) {
            var layoutManager = new LinearLayoutManager(context);
            _recyclerView.SetLayoutManager(layoutManager);
            _recyclerView.SetItemAnimator(new DefaultItemAnimator());
            _recyclerView.SetAdapter(_scannedBoxAdapter);
        }

        public void OnClick(View v) {
            var parent = v.Parent as FloatingActionMenu;
            var menu = parent;
            if (menu?.Id == Resource.Id.deleteStyleBox) {
                MainActivity.CallDialogOkCancel(Resources.GetString(Resource.String.titleDelete), Resources.GetString(Resource.String.ConfirmeDelete), Resources.GetString(Resource.String.yes), Resources.GetString(Resource.String.no), PositiveDeleteCallback, null);
            }
        }

        private void PositiveDeleteCallback() {
            _currentBox.IsClosed = false;
            var pos = _storagePackaging.InternalScannedDeliveries.FindIndex(it => it.Style?.Upc == _selectedStyle.Style.Upc);
            var item = _storagePackaging.InternalScannedDeliveries[pos];
            _selectedStyle.Style.Total--;
            item.Style.Total++;
            item.Style.ExpectedTotal++;
            if (_selectedStyle.Style.Total == 0) {
                _scannedStyles.Remove(_selectedStyle);
                HideImageSide();
            }
            _scannedBoxAdapter.NotifyDataSetChanged();

            WriteStoragePackage();
        }

        private void WriteStoragePackage() {
            var json = StorageHelper.JsonSerialize(_storagePackaging);

            StorageHelper.WriteTextAllAsync($"CustomerId_{_customerId}_SlipId_{_slipId}_CmdType_{CallApi.CurrentCustomersTypes.Id}", json);
        }

        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater) {
            base.OnCreateOptionsMenu(menu, inflater);

            menu.FindItem(Resource.Id.action_search).SetVisible(true);
            menu.FindItem(Resource.Id.menu_done).SetVisible(true);
            var item = menu.FindItem(Resource.Id.action_search);
            SetSearch(item);
        }

        public override bool OnOptionsItemSelected(IMenuItem item) {
            switch (item.ItemId) {
                case Resource.Id.action_search:
                    HideImageSide();
                    return true;
                case Resource.Id.menu_done:
                    if (_currentBox.ScannedStyles.Count > 0) {
                        if (_currentBox.IsNeedPermission) {

                            _lstItemNeedPermission = _currentBox.ScannedStyles.FindAll(it => it.Style.IsNeedPermission);
                            var msgEnd = _lstItemNeedPermission.Count > 1 ? $"{Resources.GetString(Resource.String.manyItems)} \n \n" : $"{Resources.GetString(Resource.String.oneItem)} \n \n";

                            foreach (var scannedDelivery in _lstItemNeedPermission) {
                                msgEnd += $"-  {scannedDelivery.Style.Name}, {scannedDelivery.Style.Size}, {scannedDelivery.Style.Color}  x{scannedDelivery.Style.Total}\n";

                            }
                            MainActivity.CallDialogPermission($"{Resources.GetString(Resource.String.askAdmin)} {msgEnd}", PositivePermission, null);
                        } else {
                            MainActivity.CallDialogDimension(SucessDimension, _currentBox.Dimension);
                        }

                    } else {
                        MainActivity.CallDialogOkCancel(Resources.GetString(Resource.String.titleError), Resources.GetString(Resource.String.boxEmpty), Resources.GetString(Resource.String.btnOk), Resources.GetString(Resource.String.btnCancel), null, null);
                    }
                    return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        private void SucessDimension(object obj) {
            _currentBox.Dimension = obj as Dimension;
            _currentBox.IsClosed = true;
            WriteStoragePackage();

            if (_currentBox.Id > 0) {
                PutBox();
                return;
            }
            PostBox();
        }

        private async void PostBox() {
            var boxOut = new BoxOut(_currentBox) {
                CustomerId = _customerId,
                SeasonId = CallApi.CurrentSeason.Id,
                ShipmentId = _slipId,
                CmdType = CallApi.CurrentCustomersTypes.Id
            };
            await CallApi.Post<BoxIn, BoxOut>(MainActivity, "InventoryPackaging/SaveBoxForShipment", boxOut, SuccessPostBox, ErrorPostBox, UpdatePostBox);
        }

        private void SuccessPostBox(object obj) {
            var newobj = obj as BoxIn;
            if (newobj != null) {
                _currentBox.Id = newobj.Box.Id;
                
            }
        }

        private void ErrorPostBox(object obj, string msg) {
            var errorMessage = msg;
            if (obj != null) {
                var newObj = (BoxIn)obj;
               
                var newmsg = Helper.FindGoodErrorMessage(newObj.Error.Code);

                errorMessage = !string.IsNullOrEmpty(newmsg) ? newmsg : msg;
            }
            
            MainActivity.CallDialogOkCancel(Resources.GetString(Resource.String.titleError), $"{Resources.GetString(Resource.String.writeBox)} \n \n {errorMessage}", Resources.GetString(Resource.String.btnRetry), Resources.GetString(Resource.String.btnCancel), PositiveRestartPost, Negative);
        }

        private void ErrorPutBox(object obj, string msg) {
            var errorMessage = msg;
            if (obj != null) {
                var newObj = (BoxIn)obj;
                if (newObj.Error.Code == 40) {
                    PostBox();
                    return;
                }
                var newmsg = Helper.FindGoodErrorMessage(newObj.Error.Code);

                errorMessage = !string.IsNullOrEmpty(newmsg) ? newmsg : msg;
            }

            MainActivity.CallDialogOkCancel(Resources.GetString(Resource.String.titleError), $"{Resources.GetString(Resource.String.writeBox)} \n \n {errorMessage}", Resources.GetString(Resource.String.btnRetry), Resources.GetString(Resource.String.btnCancel), PositiveRestartPost, Negative);
        }

        private void PositiveRestartPost() {
            PostBox();
        }

        private void UpdatePostBox() {
            WriteStoragePackage();
            GetPrint();
        }



        private async void PutBox() {
            var boxOut = new BoxOut(_currentBox) {
                CustomerId = _customerId,
                SeasonId = CallApi.CurrentSeason.Id,
                ShipmentId = _slipId,
                CmdType = CallApi.CurrentCustomersTypes.Id
            };
            await CallApi.Put<BoxIn, BoxOut>(MainActivity, $"InventoryPackaging/SaveBoxForShipment", boxOut, SuccessPostBox, ErrorPutBox, UpdatePostBox);
        }



        private async void GetPrint() {
            await CallApi.Get<PrintIn>(MainActivity, $"Print/PackingBoxShippingSlips?boxIds={_currentBox.Id}", SuccessPrintBox, ErrorPrintBox, UpdatePrintBox);
        }

        private void SuccessPrintBox(object obj) {
            if (obj == null) {
                return; }


            }

        private void ErrorPrintBox(object obj, string msg) {
            var errorMessage = msg;
            if (obj != null) {
                var newObj = (PrintIn)obj;
                var newmsg = Helper.FindGoodErrorMessage(newObj.Error.Code);

                errorMessage = !string.IsNullOrEmpty(newmsg) ? newmsg : msg;
            }

            MainActivity.CallDialogOkCancel(Resources.GetString(Resource.String.titleError), $"{Resources.GetString(Resource.String.printBoxError)} \n \n {errorMessage}", Resources.GetString(Resource.String.btnRetry), Resources.GetString(Resource.String.btnCancel), PositiveRestartPrint, Negative);
        }

        private void PositiveRestartPrint() {
            GetPrint();
        }

        private void UpdatePrintBox() {
            MainActivity.SupportFragmentManager.PopBackStack();
        }
        private void SetSearch(IMenuItem item) {
            var searchView = MenuItemCompat.GetActionView(item);
            _searchView = searchView.JavaCast<SearchView>();

            _searchView.QueryTextChange += (s, e) => _scannedBoxAdapter.Filter.InvokeFilter(e.NewText);
            _searchView.QueryTextSubmit += (s, e) => {
                _searchView.ClearFocus();
                e.Handled = true;
            };
        }
    }
}