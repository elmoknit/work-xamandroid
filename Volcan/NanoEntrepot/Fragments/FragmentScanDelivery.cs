﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.View;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Volcan.NanoApp.Entrepot.Helpers;
using Volcan.NanoApp.Entrepot.Models;
using Volcan.NanoApp.Entrepot.Models.Adapter;
using SearchView = Android.Support.V7.Widget.SearchView;

namespace Volcan.NanoApp.Entrepot.Fragments {
    public class FragmentScanDelivery : BaseFragment {
        private RecyclerView _recyclerViewScan;
        private RecyclerView _recyclerViewError;
        private ScannedDeliveryAdapter _scannedDeliveryAdapter;
        private ErrorAdapter _errorAdapter;
        private List<ErrorScan> _errorsScan;
        private List<ScannedDelivery> _scannedDeliveriesApi;
        private ScannedDelivery _selectedScannedDelivery;
        private TextView _totalRack;
        private TextView _totalBox;
        private TextView _totalExpected;

        private RelativeLayout _rlError;
        private TextView _errorsCount;
        private Style _errorStyle;
        private TextView _test;

        private EditText _scanUpc;

        private ImageView _imgView;
        private TextView _total;
        private TextView _expectedTotal;
        private View _borderSeparator;

        private SearchView _searchView;
        private Task _apiCallGetStyles;
        private StoragePackaging _storagePackaging;
        private string _customerId;
        private string _slipId;

        private List<string> _scannedUpcList;
        private int _countTask = 0;
        private int _pos;
        private StyleDelivery _findStyleDelivery;

        public static FragmentScanDelivery NewInstance(string title, string customerId, string slipId) {
            var frag = new FragmentScanDelivery() { Arguments = new Bundle(), Title = title };
            frag.Arguments.PutString("CustomerId", customerId);
            frag.Arguments.PutString("SlipId", slipId);
            frag.Arguments.PutString("Title", title);
            return frag;
        }

        private int _counter = 0;
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            var view = inflater.Inflate(Resource.Layout.fragmentScanDelivery, container, false);

            _customerId = Arguments.GetString("CustomerId");
            _slipId = Arguments.GetString("SlipId");


            _totalRack = view.FindViewById<TextView>(Resource.Id.RackTotal);
            _totalBox = view.FindViewById<TextView>(Resource.Id.nbBoxTotal);
            _totalExpected = view.FindViewById<TextView>(Resource.Id.totalTotalScanDelivery);

            _rlError = view.FindViewById<RelativeLayout>(Resource.Id.rlErrorCount);
            _errorsCount = view.FindViewById<TextView>(Resource.Id.nbError);


            _scanUpc = view.FindViewById<EditText>(Resource.Id.scanUPCDelivery);

            _imgView = view.FindViewById<ImageView>(Resource.Id.imgDelivery);
            _total = view.FindViewById<TextView>(Resource.Id.totalDelivery);
            _expectedTotal = view.FindViewById<TextView>(Resource.Id.totalExpectedDelivery);
            _borderSeparator = view.FindViewById<View>(Resource.Id.borderSeparator);


            _apiCallGetStyles = CallApi.Get<StylesIn>(MainActivity, $"InventoryPackaging/GetPackingStyles?seasonId={CallApi.CurrentSeason.Id}&customerId={_customerId}&shipmentId={_slipId}&cmdType={CallApi.CurrentCustomersTypes.Id}", SuccessScannedDelivery, ErrorScannedDelivery, UpdateScannedDelivery);

            GetStorageList();
            _scanUpc.SetCursorVisible(false);
            _scanUpc.RequestFocus();

            _scanUpc.KeyPress += (sender, e) => {
                if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter) {
                    _counter += 1;
                    GunClick();
                    _scanUpc.Text = "";
                } else {
                    e.Handled = false;
                }
            };



            _scannedDeliveryAdapter = new ScannedDeliveryAdapter(Context, _storagePackaging.InternalScannedDeliveries);
            _scannedDeliveryAdapter.ItemClick += OnItemClick;

            _errorsScan = new List<ErrorScan>();
            _errorAdapter = new ErrorAdapter(Context, _errorsScan);
            _errorAdapter.ItemClick += OnErrorClick;

            _recyclerViewScan = view.FindViewById<RecyclerView>(Resource.Id.recyclerViewScannedDelivery);
            _recyclerViewError = view.FindViewById<RecyclerView>(Resource.Id.recyclerViewError);
         
           

            _scannedUpcList = new List<string>();
            SetupRecyclerView(Context);
            _totalRack.Text = FindRackTotal();
            _totalBox.Text = FindBoxTotal();
            _totalExpected.Text = FindExpectedTotal();
            return view;

        }

        #region GetAPI
        private void SuccessScannedDelivery(object obj) {
            if (obj == null) { return; }

            var newobj = (StylesIn)obj;
            _scannedDeliveriesApi = new List<ScannedDelivery>();

            if (newobj.Styles == null || newobj.Styles.Count == 0) {
                return;
            }
            foreach (var style in newobj.Styles) {
                _scannedDeliveriesApi.Add(new ScannedDelivery { Style = style });
            }

            UpdateInternalList();
        }

        private void ErrorScannedDelivery(object obj, string msg) {
            var errorMessage = msg;
            if (obj != null) {
                var newObj = (StylesIn)obj;
                var newmsg = Helper.FindGoodErrorMessage(newObj.Error.Code);

                errorMessage = !string.IsNullOrEmpty(newmsg) ? newmsg : msg;
            }

            MainActivity.CallDialogOkCancel(Resources.GetString(Resource.String.titleError), $"{Resources.GetString(Resource.String.readStylesSlip)} \n \n {errorMessage}", Resources.GetString(Resource.String.btnRetry), Resources.GetString(Resource.String.btnCancel), Positive, Negative);
        }

        private void Positive() {
            _apiCallGetStyles = CallApi.Get<StylesIn>(MainActivity, $"InventoryPackaging/GetPackingStyles?seasonId={CallApi.CurrentSeason.Id}&customerId={_customerId}&shipmentId={_slipId}&cmdType={CallApi.CurrentCustomersTypes.Id}", SuccessScannedDelivery, ErrorScannedDelivery, UpdateScannedDelivery);
        }

        private void Negative() {
            MainActivity.HideLoader();
        }

        private void UpdateScannedDelivery() {

            ShowImageSide();
            _scannedDeliveryAdapter.ResetItems();
            _scannedDeliveryAdapter.NotifyDataSetChanged();
            _totalRack.Text = FindRackTotal();
            _totalBox.Text = FindBoxTotal();
            _totalExpected.Text = FindExpectedTotal();
        }


        #endregion

        #region File
        private void GetStorageList() {

            if (!StorageHelper.IsFileExistAsync($"CustomerId_{_customerId}_SlipId_{_slipId}_CmdType_{CallApi.CurrentCustomersTypes.Id}")) {
                StorageHelper.CreateFile($"CustomerId_{_customerId}_SlipId_{_slipId}_CmdType_{CallApi.CurrentCustomersTypes.Id}");
            }

            var read = StorageHelper.ReadAllTextAsync($"CustomerId_{_customerId}_SlipId_{_slipId}_CmdType_{CallApi.CurrentCustomersTypes.Id}");

            if (read == "" || read == "null") {
                if (_storagePackaging == null) {
                    _storagePackaging = new StoragePackaging { CustomerId = _customerId, DeliveryId = _slipId, CmdType = CallApi.CurrentCustomersTypes.Id };
                }

                if (_storagePackaging.InternalScannedDeliveries == null) {
                    _storagePackaging.InternalScannedDeliveries = new List<ScannedDelivery>();
                }

                if (_storagePackaging.InternalBoxes == null) {
                    _storagePackaging.InternalBoxes = new List<Box>();
                }
                return;
            }

            _storagePackaging = StorageHelper.JsonDesialize(read);
            ShowImageSide();

        }
        private void UpdateInternalList() {
            if (_storagePackaging.InternalScannedDeliveries.Count == 0) {
                CreateInternalScannedDeliveries();
                return;
            }

            //Effacer les styles qui ne sont pas sur les supports où dans les boîtes. 
            var toRemove = new List<ScannedDelivery>();
            foreach (var storagePackagingInternalScannedDelivery in _storagePackaging.InternalScannedDeliveries) {
                var pos = _scannedDeliveriesApi.FindIndex(it => it.Style.Upc == storagePackagingInternalScannedDelivery.Style.Upc);

                if (pos <= -1) {
                    //S'il ne sont pu sur la liste API mais dans mes boîtes ou sur support, mettre expectedTotal à 0
                    if (storagePackagingInternalScannedDelivery.Style.BoxTotal > 0 || storagePackagingInternalScannedDelivery.Style.Total > 0) {
                        storagePackagingInternalScannedDelivery.Style.ExpectedTotal = 0;
                        storagePackagingInternalScannedDelivery.Style.RealTotal = 0;
                    } else {
                        toRemove.Add(storagePackagingInternalScannedDelivery);
                    }

                }
            }
            //Effacer les styles qui ne sont pas dans le call API après vérification . 
            foreach (var scannedDelivery in toRemove) {
                _storagePackaging.InternalScannedDeliveries.Remove(scannedDelivery);
            }

            //Comparer la liste API avec l'interne
            foreach (var scannedDelivery in _scannedDeliveriesApi) {
                var pos = _storagePackaging.InternalScannedDeliveries.FindIndex(it => it.Style.Upc == scannedDelivery.Style.Upc);

                //Ajouter les styles du call API qui ne sont pas sur la liste interne
                if (pos <= -1) {
                    _storagePackaging.InternalScannedDeliveries.Add(scannedDelivery);
                }

                var styleApi = scannedDelivery.Style;
                var styleInternal = _storagePackaging.InternalScannedDeliveries[pos].Style;

                _storagePackaging.InternalScannedDeliveries[pos].IsSelected = false;
                _storagePackaging.InternalScannedDeliveries[pos].IsPending = false;

                //API Expectedtotal Change
                if (styleApi.ExpectedTotal != styleInternal.RealTotal) {
                    //Changer ExpectedTotal
                    //styleInternal.ExpectedTotal = styleInternal.ExpectedTotal + (styleApi.ExpectedTotal - styleInternal.RealTotal);
                    styleInternal.ExpectedTotal = styleApi.ExpectedTotal;
                    //Changer RealTotal
                    styleInternal.RealTotal = styleApi.ExpectedTotal;
                }

                //API TOTAL > 0  == Boîte(s) fermé(s) 
                if (styleApi.Total > 0 /*&& styleInternal.RealTotal - styleInternal.ExpectedTotal != styleApi.Total*/) {
                    styleInternal.BoxTotal = styleApi.Total;
                    //styleInternal.ExpectedTotal = styleInternal.RealTotal - styleInternal.ExpectedTotal;
                }

                //Mettre toutes les infos du style venant de la BD (au cas d'un changement)
                styleInternal.Color = styleApi.Color;
                styleInternal.Description = styleApi.Description;
                styleInternal.ImagePath = styleApi.ImagePath;
                styleInternal.Name = styleApi.Name;
                styleInternal.Size = styleApi.Size;
            }

            //Sélectionner le premier
            _storagePackaging.InternalScannedDeliveries[0].IsSelected = true;
            _selectedScannedDelivery = _storagePackaging.InternalScannedDeliveries[0];
            WriteStorageDeliveries();
        }

        private void CreateInternalScannedDeliveries() {

            var internalScannedDeliveries = _storagePackaging.InternalScannedDeliveries;
            if (_scannedDeliveriesApi == null) {
                return;
            }
            foreach (var scannedDelivery in _scannedDeliveriesApi) {
                scannedDelivery.Style.BoxTotal = scannedDelivery.Style.Total;
                scannedDelivery.Style.Total = 0;
                scannedDelivery.Style.RealTotal = scannedDelivery.Style.ExpectedTotal;
                internalScannedDeliveries.Add(scannedDelivery);
            }

            _storagePackaging.InternalScannedDeliveries[0].IsSelected = true;
            _selectedScannedDelivery = _storagePackaging.InternalScannedDeliveries[0];
            WriteStorageDeliveries();
        }

        private async Task WriteStorageDeliveries() {
            var json = StorageHelper.JsonSerialize(_storagePackaging);

            await StorageHelper.WriteTextAllAsync($"CustomerId_{_customerId}_SlipId_{_slipId}_CmdType_{CallApi.CurrentCustomersTypes.Id}", json);
        }
        #endregion

        #region ScanItem
        private void GunClick() {
            _tasks.Add(ValidationClick());
        }
        private static List<Task> _tasks => new List<Task>();

        private void PositiveAddFind() {
            var styleDelivery = new StyleDelivery {BoxTotal = 0, Color = _errorStyle.Color, Description = _errorStyle.Description, ExpectedTotal = 0, Id = _errorStyle.Id, ImagePath = _errorStyle.ImagePath, IsNeedPermission = true, Name = _errorStyle.Name, RealTotal = 0, SameCount = 0, Total = 1, Size= _errorStyle.Size, Upc = _errorStyle.Upc};
            var newDelivery = new ScannedDelivery {
                Style = styleDelivery,
                IsSelected = false
            };

            var positionFirstGreen = _storagePackaging.InternalScannedDeliveries.FindIndex(it => it.Style.Total == it.Style.ExpectedTotal);

            if (positionFirstGreen > -1) {
                _storagePackaging.InternalScannedDeliveries.Insert(positionFirstGreen, newDelivery);
                _scannedDeliveryAdapter.NotifyItemInserted(positionFirstGreen);
                _findStyleDelivery = null;
                _recyclerViewScan.SmoothScrollToPosition(positionFirstGreen);
                WriteStorageDeliveries();
                return;
            }

            _storagePackaging.InternalScannedDeliveries.Add(newDelivery);
            _scannedDeliveryAdapter.NotifyItemInserted(_storagePackaging.InternalScannedDeliveries.Count - 1);
            _findStyleDelivery = null;
            _recyclerViewScan.SmoothScrollToPosition(_storagePackaging.InternalScannedDeliveries.Count - 1);
            WriteStorageDeliveries();
        }

        private async Task FindStyle() {
            await CallApi.Get<StyleDeliveryIn>(MainActivity, $"InventoryPackaging/GetStyle?upc={_scanUpc.Text}", SuccessFindStyle, ErrorFindStyle, UpdateFindStyle, null, false);
        }

        private void SuccessFindStyle(object obj) {
            if (obj == null) { return; }
            var newobj = obj as StyleDeliveryIn;
            if (newobj != null) {
                _findStyleDelivery = newobj.Style;
                //_findStyleDelivery.Id = 0;
            }
            _findStyleDelivery.IsNeedPermission = true;
            _findStyleDelivery.RealTotal = 0;
            _findStyleDelivery.Total++;
            _findStyleDelivery.ExpectedTotal = 0;
        }

        private void ErrorFindStyle(object obj, string msg) {

            var newobj = obj as StyleDeliveryIn;
            _findStyleDelivery = null;
            if (newobj != null) {
                _errorsScan.Add(new ErrorScan { Name = $"{Resources.GetString(Resource.String.missing)}", TypeError = TypeError.WrongUpc, Style = new Style { Upc = newobj.Upc } });
                _errorAdapter.NotifyItemInserted(_errorsScan.Count-1);
                MainActivity.ErrorSound();
                ErrorCount();
                return;
            }
            _errorsScan.Add(new ErrorScan { Name = $"{msg}", TypeError = TypeError.Server, Style = new Style{ Upc = _scanUpc.Text} });
            _errorAdapter.NotifyItemInserted(_errorsScan.Count - 1);
            MainActivity.ErrorSound();
            ErrorCount();
        }

        private void UpdateFindStyle() {
            _errorsScan.Add(new ErrorScan { Name = Resources.GetString(Resource.String.addNewItem), TypeError = TypeError.NotOnPackingSlip, Style = _findStyleDelivery });
            _errorAdapter.NotifyItemInserted(_errorsScan.Count - 1);
            MainActivity.ErrorSound();
            ErrorCount();
        }
        private async Task AddItem(int position) {
            var item = _storagePackaging.InternalScannedDeliveries[position];
            item.Style.Total += 1;

            item.IsSelected = true;
            _selectedScannedDelivery = item;

            _recyclerViewScan.ScrollToPosition(position);

            _scannedDeliveryAdapter.NotifyDataSetChanged();

            ShowImageSide();
            ChangeSelected();
            _totalRack.Text = FindRackTotal();
            _totalBox.Text = FindBoxTotal();


            await WriteStorageDeliveries();
        }

        private async Task ValidationClick() {
            _pos = _storagePackaging.InternalScannedDeliveries.FindIndex(it => it.Style?.Upc == _scanUpc.Text);
            if (_pos > -1) {
                var item = _storagePackaging.InternalScannedDeliveries[_pos];

                if (item.Style.ExpectedTotal < item.Style.Total + item.Style.BoxTotal + 1) {
                    _errorsScan.Add(new ErrorScan { Name = Resources.GetString(Resource.String.addItemSup), TypeError = TypeError.AddItem, Style = item.Style});
                    MainActivity.ErrorSound();
                    ErrorCount();
                    return;
                }

                await AddItem(_pos);
            } else {
                await FindStyle();
            }

        }

        private void PositivePlus() {
            AddItem(_pos);
            
        }

        #endregion


        private void OnItemClick(object sender, int position) {
            var adapter = (ScannedDeliveryAdapter)sender;
            _selectedScannedDelivery = adapter.GetSelectedItem();
            adapter.NotifyDataSetChanged();

            if (_selectedScannedDelivery != null) {
                ShowImageSide();
            } else {
                HideImageSide();
            }
        }

        private void OnErrorClick(object sender, int position) {
            _errorStyle = _errorsScan[position].Style;
            switch (_errorsScan[position].TypeError) {
                case TypeError.AddItem:
                    MainActivity.CallDialogOkCancel(Resources.GetString(Resource.String.titleAddItem), $"{_errorStyle.Name}, {_errorStyle.Size},  {_errorStyle.Color} {Resources.GetString(Resource.String.questionAddItem)}", Resources.GetString(Resource.String.yes), Resources.GetString(Resource.String.no), PositivePlus, null, false);
                    break;
                case TypeError.NotOnPackingSlip:
                    MainActivity.CallDialogOkCancel(Resources.GetString(Resource.String.missingItem), $"{_errorStyle.Name}, {_errorStyle.Size},  {_errorStyle.Color} {Resources.GetString(Resource.String.notOnSlip)} \n \n {Resources.GetString(Resource.String.questionAdd)} \n \n {Resources.GetString(Resource.String.needAutorize)}", Resources.GetString(Resource.String.yes), Resources.GetString(Resource.String.no), PositiveAddFind, Negative, false);
                    break;
                default: //WRONG UPC OU SERVER
                    break;
            }
            _errorsScan.RemoveAt(position);
            _errorAdapter.NotifyItemRemoved(position);
            ErrorCount();
        }

        private void ErrorCount() {
            if (_errorsScan.Count == 0) {
                _rlError.Visibility = ViewStates.Gone;
                return;
            }
            _rlError.Visibility = ViewStates.Visible;
            _errorsCount.Text = _errorsScan.Count.ToString();
            
        }

        private void ChangeSelected() {
            if (!_storagePackaging.InternalScannedDeliveries.Any(it => it.IsSelected)) {
                return;
            }

            var selecteds = _storagePackaging.InternalScannedDeliveries.Where(it => it.IsSelected && _selectedScannedDelivery.Style.Upc != it.Style.Upc);

            foreach (var scannedDelivery in selecteds) {
                var pos = _storagePackaging.InternalScannedDeliveries.IndexOf(scannedDelivery);
                scannedDelivery.IsSelected = false;
                _scannedDeliveryAdapter.NotifyItemChanged(pos);
            }
        }


        private void HideImageSide() {
            _imgView.SetImageBitmap(null);
            _total.Text = "";
            _expectedTotal.Text = "";
            _borderSeparator.Visibility = ViewStates.Invisible;
        }

        private void ShowImageSide() {
            HideImageSide();

            if (_selectedScannedDelivery == null) { return; }
            if (_selectedScannedDelivery.Style.ImagePath != "") {
                var imageBitmap = Helper.GetImageBitmapFromUrl(_selectedScannedDelivery.Style.ImagePath, MainActivity);
                if (imageBitmap != null) {
                    _imgView.SetImageBitmap(imageBitmap);
                }
            }

            _total.Text = _selectedScannedDelivery.Style.Total.ToString();
            _expectedTotal.Text = _selectedScannedDelivery.Style.ExpectedTotal.ToString();
            _borderSeparator.Visibility = ViewStates.Visible;
        }

        private string FindRackTotal() {
            var countScanned = _storagePackaging.InternalScannedDeliveries.Where(scannedDelivery => scannedDelivery.Style != null).Sum(scannedDelivery => scannedDelivery.Style.Total);
            return $"{countScanned}";
        }
        private string FindBoxTotal() {
            var countQt = _storagePackaging.InternalScannedDeliveries.Where(scannedDelivery => scannedDelivery.Style != null).Sum(scannedDelivery => scannedDelivery.Style.BoxTotal);
            return $"{countQt}";
        }

        private string FindExpectedTotal() {
            var countQt = _storagePackaging.InternalScannedDeliveries.Where(scannedDelivery => scannedDelivery.Style != null).Sum(scannedDelivery => scannedDelivery.Style.RealTotal);

            return $"{countQt}";
        }
        private void SetupRecyclerView(Context context) {

            _recyclerViewScan.SetLayoutManager(new LinearLayoutManager(context));
            //_recyclerView.AddItemDecoration(new SimpleDividerItemDecoration); Set Border
            _recyclerViewScan.SetItemAnimator(new DefaultItemAnimator());
            _recyclerViewScan.SetAdapter(_scannedDeliveryAdapter);

            _recyclerViewError.SetLayoutManager(new LinearLayoutManager(context));
            _recyclerViewError.SetItemAnimator(new DefaultItemAnimator());
            _recyclerViewError.SetAdapter(_errorAdapter);
            _recyclerViewError.StopScroll();
        }

        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater) {
            base.OnCreateOptionsMenu(menu, inflater);
            menu.FindItem(Resource.Id.menu_done).SetVisible(true);
            menu.FindItem(Resource.Id.menu_box).SetVisible(true);
            menu.FindItem(Resource.Id.action_search).SetVisible(true);

            var item = menu.FindItem(Resource.Id.action_search);
            SetSearch(item);
        }

        public override bool OnOptionsItemSelected(IMenuItem item) {
            switch (item.ItemId) {
                case Resource.Id.menu_box:
                    if (_storagePackaging == null) {
                        //MainActivity.CallDialogOkCancel("Vide", "                      ");
                    }
                    Android.Support.V4.App.Fragment fragment = FragmentBoxes.NewInstance($"{Arguments.GetString("Title")} | {Resources.GetString(Resource.String.titleCreateBox)}", _customerId, _slipId);
                    MainActivity.ReplaceFragment(fragment, "Boxes");
                    return true;
                case Resource.Id.menu_done:
                    MainActivity.CallDialogOkCancel(Resources.GetString(Resource.String.closeDelivery), Resources.GetString(Resource.String.msgCloseDelivery), Resources.GetString(Resource.String.btnClose), Resources.GetString(Resource.String.btnCancel), PositiveClosingDelivery, null);
                    return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        private async void PositiveClosingDelivery() {
            var deliveryOut = new DeliveryOut { CustomerId = _customerId, ShipmentId = _slipId ?? "", SeasonId = CallApi.CurrentSeason.Id, CmdType = CallApi.CurrentCustomersTypes.Id };
            await CallApi.Put<DeliveryIn, DeliveryOut>(MainActivity, $"InventoryPackaging/ClosePackingSlips", deliveryOut, SuccessClosingDelivery, ErrorClosingDelivery, UpdateClosingDelivery);
        }

        private void SuccessClosingDelivery(object obj) {
            if (obj == null) { return; }
            var newobj = obj as DeliveryIn;
            if (newobj != null) {

            }

        }

        private void ErrorClosingDelivery(object obj, string msg) {
            var errorMessage = msg;
            if (obj != null) {
                var newObj = (DeliveryIn)obj;
                var newmsg = Helper.FindGoodErrorMessage(newObj.Error.Code);

                errorMessage = !string.IsNullOrEmpty(newmsg) ? newmsg : msg;
            }

            MainActivity.CallDialogOkCancel(Resources.GetString(Resource.String.titleError), $"{Resources.GetString(Resource.String.errorCloseDelivery)} \n \n {errorMessage}", Resources.GetString(Resource.String.btnRetry), Resources.GetString(Resource.String.btnCancel), PositiveClosingDelivery, Negative);
        }

        private void UpdateClosingDelivery() {
            MainActivity.OnBackPressed();
        }

        private void SetSearch(IMenuItem item) {
            var searchView = MenuItemCompat.GetActionView(item);
            _searchView = searchView.JavaCast<SearchView>();

            _searchView.QueryTextChange += (s, e) => _scannedDeliveryAdapter.Filter.InvokeFilter(e.NewText);
            _searchView.QueryTextSubmit += (s, e) => {
                _searchView.ClearFocus();
                e.Handled = true;
            };
        }
    }
}