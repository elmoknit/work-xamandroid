﻿using Android.OS;
using Volcan.NanoApp.Entrepot.Helpers;
using Fragment = Android.Support.V4.App.Fragment;

namespace Volcan.NanoApp.Entrepot.Fragments {
    public class BaseFragment : Fragment {

        public MainActivity MainActivity;
        public string Title { get; set; }
        public string SubTitle { get; set; }

        public override void OnCreate(Bundle savedInstanceState) {
            base.OnCreate(savedInstanceState);
            MainActivity = (MainActivity)Activity;
            HasOptionsMenu = true;
            // Create your fragment here
        }

        public override void OnResume() {
            base.OnResume();

            if (MainActivity == null) {
                MainActivity = (MainActivity)Activity;

            }

            MainActivity.ChangeToolbarTitle(Title);
            MainActivity.SupportActionBar.Show();

            
            if (CallApi.CurrentUser == null) {
                MainActivity.CallDialogLogin(SuccessLogin);
            }
            if (!Helper.CheckConnectivity(MainActivity)) {
                MainActivity.CallDialogOkCancel("Erreur", "Aucune connexion Internet", Resources.GetString(Resource.String.btnOk), Resources.GetString(Resource.String.btnCancel), null, null);
            }

        }

        public virtual void SuccessLogin() {
      
        }
 

        

    }
}