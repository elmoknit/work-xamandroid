﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Android.Content;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.View;
using Android.Support.V7.Widget;
using Android.Views;
using Volcan.NanoApp.Entrepot.Helpers;
using Volcan.NanoApp.Entrepot.Models;
using Volcan.NanoApp.Entrepot.Models.Adapter;

namespace Volcan.NanoApp.Entrepot.Fragments {
    public class FragmentBoxes : BaseFragment {
        private RecyclerView _recyclerView;
        private BoxAdapter _boxAdapter;

        private Task _apiCall;
        private List<Box> _boxesApi;

        private string _customerId;
        private string _slipId;

        private SearchView _searchView;

        private StoragePackaging _storagePackaging;

        private bool _isCheckBoxPrintActived = false;
        private string _boxesId = "";

        public static FragmentBoxes NewInstance(string title, string customerId, string slipId) {
            var frag = new FragmentBoxes { Arguments = new Bundle(), Title = title };
            frag.Arguments.PutString("CustomerId", customerId);
            frag.Arguments.PutString("SlipId", slipId);
            frag.Arguments.PutString("Title", title);

            //frag.Arguments.PutString("StoragePackaging", JsonConvert.SerializeObject(storagePackaging));
            return frag;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            var view = inflater.Inflate(Resource.Layout.fragmentBoxes, container, false);
            _customerId = Arguments.GetString("CustomerId");
            _slipId = Arguments.GetString("SlipId");

            GetStoragePackaging();


            _apiCall = CallApi.Get<BoxesIn>(MainActivity, $"InventoryPackaging/GetPackingBoxes?seasonId={CallApi.CurrentSeason.Id}&customerId={Arguments.GetString("CustomerId")}&shipmentId={_slipId}&cmdType={CallApi.CurrentCustomersTypes.Id}", SuccessBoxes, ErrorBoxes, UpdateBoxes);


            _boxAdapter = new BoxAdapter(Context, _storagePackaging.InternalBoxes);
            _boxAdapter.ItemClick += OnItemClick;
            _recyclerView = view.FindViewById<RecyclerView>(Resource.Id.recyclerViewBoxes);
            SetupRecyclerView(Context);
            return view;
        }


        private void SuccessBoxes(object obj) {
            if (obj == null) {
                return;
            }

            var newobj = (BoxesIn)obj;
            _boxesApi = new List<Box>();

            foreach (var box in newobj.Boxes) {
                box.ScannedStyles = new List<ScannedDelivery>();
                _boxesApi.Add(box);
            }
        }

        private void ErrorBoxes(object obj, string msg) {
            var errorMessage = msg;

            if (obj != null) {
                var newObj = (BoxesIn)obj;
                var newmsg = Helper.FindGoodErrorMessage(newObj.Error.Code);

                errorMessage = !string.IsNullOrEmpty(newmsg) ? newmsg : msg;
            }

            MainActivity.CallDialogOkCancel(Resources.GetString(Resource.String.titleError), $"{Resources.GetString(Resource.String.readBoxes)} \n \n {errorMessage}", Resources.GetString(Resource.String.btnRetry), Resources.GetString(Resource.String.btnCancel), Positive, Negative);
        }

        private void Positive() {
            _apiCall = CallApi.Get<BoxesIn>(MainActivity, $"InventoryPackaging/GetPackingBoxes?seasonId={CallApi.CurrentSeason.Id}&customerId={_customerId}&shipmentId={_slipId}&cmdType={CallApi.CurrentCustomersTypes.Id}", SuccessBoxes, ErrorBoxes, UpdateBoxes);
        }

        private void Negative() {
            MainActivity.HideLoader();
        }

        private void UpdateBoxes() {
            UpdateBoxesList();
            _boxAdapter.ResetItems();
            _boxAdapter.NotifyDataSetChanged();
        }

        private void UpdateBoxesList() {
            var lstToRemove = new List<Box>();
            var lstApiNotInternal = new List<Box>();
            //REÇU API
            foreach (var boxApi in _boxesApi) {
                var pos = _storagePackaging.InternalBoxes.FindIndex(it => it.Timestamp == boxApi.Timestamp);

                //Si box API a une correspondance dans le fichier interne
                if (pos > -1) {
                    //Si la boîte est fermée --> Supprimé la boîte (On va l'ajouter plus tard)
                    if (_storagePackaging.InternalBoxes[pos].IsClosed) {
                        //Changer les totaux des styles (enlever des boîtes et ajouter à la liste "à scanner")

                        if (_storagePackaging.InternalBoxes[pos].ScannedStyles == null || _storagePackaging.InternalBoxes[pos].ScannedStyles.Count == 0) {
                            foreach (var scannedDelivery in _storagePackaging.InternalBoxes[pos].Styles) {
                                var item = _storagePackaging.InternalScannedDeliveries.Find(it => it.Style.Upc == scannedDelivery.Upc);


                                item.Style.Total++;
                                item.Style.BoxTotal--;
                            }
                           
                        } else {
                            foreach (var scannedDelivery in _storagePackaging.InternalBoxes[pos].ScannedStyles) {
                                var item = _storagePackaging.InternalScannedDeliveries.Find(it => it.Style.Upc == scannedDelivery.Style.Upc);


                                item.Style.Total++;
                                item.Style.BoxTotal--;
                            }
                        }
                        
                        //Supprimer de la liste de boîte
                        _storagePackaging.InternalBoxes.RemoveAt(pos);
                        _boxAdapter.NotifyItemRemoved(pos);


                    } //Si la boîte est ouverte je ne veux pas la remplacer
                    else {
                        lstToRemove.Add(boxApi);

                    }


                } //Si box API a pas de correspondance
                else {
                    lstApiNotInternal.Add(boxApi);
                }
            }

            //Delete les boxAPI qui ne sont pas à ajouter
            foreach (var box in lstToRemove) {
                _boxesApi.Remove(box);
            }

            var lstInternalRemove = new List<Box>();
            //Fichier Enregistrer fermer --> À supprimer
            //Pour chaque boîte restante dans le fichier interne
            foreach (var storagePackagingInternalBox in _storagePackaging.InternalBoxes) {
                //Si la boîte est fermée on veut la supprimer
                if (storagePackagingInternalBox.IsClosed) {
                    var pos = _storagePackaging.InternalBoxes.FindIndex(it => it.Timestamp == storagePackagingInternalBox.Timestamp && it.IsClosed);
                    //Changer les totaux des styles (enlever des boîtes et ajouter à la liste "à scanner")
                    foreach (var scannedDelivery in _storagePackaging.InternalBoxes[pos].ScannedStyles) {
                        var item = _storagePackaging.InternalScannedDeliveries.Find(it => it.Style.Upc == scannedDelivery.Style.Upc);
                        item.Style.Total++;
                        item.Style.BoxTotal--;
                    }
                    lstInternalRemove.Add(storagePackagingInternalBox);
                    //_storagePackaging.InternalBoxes.Remove(storagePackagingInternalBox);
                    //_boxAdapter.NotifyItemRemoved(pos);
                }
            }
            //Delete les boxInterne qui sont à supprimer
            foreach (var box in lstInternalRemove) {
                _storagePackaging.InternalBoxes.Remove(box);

            }
            //Ajouter les box qui sont sur l'API mais pas à l'interne
            foreach (var box in _boxesApi) {
                //Changer les totaux des styles (ajouter des boîtes et enlever à la liste "à scanner")
                if (lstApiNotInternal.Contains(box)) {
                    foreach (var scannedDelivery in box.Styles) {
                        var item = _storagePackaging.InternalScannedDeliveries.Find(it => it.Style.Upc == scannedDelivery.Upc);

                        //Si l'item existe pas sur le bon (il faut l'ajouter)
                        if (item == null) {
                            _storagePackaging.InternalScannedDeliveries.Add(new ScannedDelivery {IsPending = false, IsSelected = false, Style = new StyleDelivery {BoxTotal = scannedDelivery.Total, Color = scannedDelivery.Color, Description = scannedDelivery.Description, ExpectedTotal = 0, Id = scannedDelivery.Id, ImagePath = scannedDelivery.ImagePath, IsNeedPermission = false, Name = scannedDelivery.Name, RealTotal = 0, Upc = scannedDelivery.Upc}});
                        } else {
                            item.Style.BoxTotal++;
                        }
                        
                    }
                } else {
                    foreach (var scannedDelivery in box.Styles) {
                        var item = _storagePackaging.InternalScannedDeliveries.Find(it => it.Style.Upc == scannedDelivery.Upc);
                        item.Style.Total--;
                        item.Style.BoxTotal++;
                    }
                }

                _storagePackaging.InternalBoxes.Add(box);
            }
            _boxAdapter.NotifyDataSetChanged();
            WriteStorageDeliveries();
        }

        private void GetStoragePackaging() {
            var read = StorageHelper.ReadAllTextAsync($"CustomerId_{_customerId}_SlipId_{_slipId}_CmdType_{CallApi.CurrentCustomersTypes.Id}");

            _storagePackaging = StorageHelper.JsonDesialize(read);
        }

        private void OnItemClick(object sender, int position) {
            var selectedBox = _boxAdapter.ItemClicked();

            if (selectedBox == null) {
                return;
            }

            Android.Support.V4.App.Fragment fragment = FragmentScanBox.NewInstance($"{Arguments.GetString("Title")} | {Resources.GetString(Resource.String.titleBox)}{selectedBox.BoxNumber}", _storagePackaging, selectedBox.Timestamp, _customerId, _slipId);
            MainActivity.ReplaceFragment(fragment, "ScanBox");
        }

        private void SetupRecyclerView(Context context) {
            var layoutManager = new LinearLayoutManager(context);
            _recyclerView.SetLayoutManager(layoutManager);
            _recyclerView.SetItemAnimator(new DefaultItemAnimator());
            _recyclerView.SetAdapter(_boxAdapter);
        }

        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater) {
            base.OnCreateOptionsMenu(menu, inflater);
            if (_isCheckBoxPrintActived) {
                MainActivity.SetBackOnNavigation();
                MainActivity.ChangeToolbarTitle(Resources.GetString(Resource.String.printBoxes), "");
                menu.FindItem(Resource.Id.menu_print).SetVisible(true);
            } else {
                MainActivity.SetHomeOnNavigation();
                MainActivity.ChangeToolbarTitle(Arguments.GetString("Title"));
                menu.FindItem(Resource.Id.action_search).SetVisible(true);
                menu.FindItem(Resource.Id.menu_add).SetVisible(true);
                menu.FindItem(Resource.Id.menu_print).SetVisible(true);
                var item = menu.FindItem(Resource.Id.action_search);
                SetSearch(item);
            }
        }

        public override bool OnOptionsItemSelected(IMenuItem item) {
            switch (item.ItemId) {
                case Android.Resource.Id.Home:
                    _isCheckBoxPrintActived = false;
                    MainActivity.InvalidateOptionsMenu();
                    HideCheckbox();
                    break;
                case Resource.Id.menu_add:
                    var newbox = new Box { BoxNumber = _storagePackaging.InternalBoxes.Count + 1, Timestamp = Helper.GetTimestamp(), ScannedStyles = new List<ScannedDelivery>() };
                    _storagePackaging.InternalBoxes.Add(newbox);

                    _boxAdapter.ResetItems();
                    _boxAdapter.NotifyItemInserted(_storagePackaging.InternalBoxes.Count - 1);

                    //_recyclerView.ScrollToPosition(_storagePackaging.InternalBoxes.Count -1);

                    WriteStorageDeliveries();

                    Android.Support.V4.App.Fragment fragment = FragmentScanBox.NewInstance($"{Arguments.GetString("Title")} | {Resources.GetString(Resource.String.titleBox)}{newbox.BoxNumber}", _storagePackaging, newbox.Timestamp, _customerId, _slipId);
                    MainActivity.ReplaceFragment(fragment, "ScanBox");
                    return true;
                case Resource.Id.menu_print:
                    //Deuxième clic

                    if (_isCheckBoxPrintActived) {
                        var boxToPrint = _storagePackaging.InternalBoxes.FindAll(it => it.IsSelectedForPrint);
                        if (boxToPrint.Count > 0) {
                            _boxesId = $"boxIds={string.Join("&boxIds=", boxToPrint.Select(it => it.Id))}";
                            boxToPrint.ForEach(it => it.IsSelectedForPrint = false);

                            _isCheckBoxPrintActived = false;
                            MainActivity.InvalidateOptionsMenu();
                            GetPrints();
                            HideCheckbox();
                        } else {
                            MainActivity.CallDialogOkCancel(Resources.GetString(Resource.String.titleError), Resources.GetString(Resource.String.msgPrintSelectBox), Resources.GetString(Resource.String.btnOk), Resources.GetString(Resource.String.btnCancel), null, null);
                        }


                        return true;
                    }

                    //premier clic
                    if (_storagePackaging.InternalBoxes.Any(it => it.IsClosed)) {
                        ShowCheckbox();
                        _isCheckBoxPrintActived = true;
                        MainActivity.InvalidateOptionsMenu();
                    } else {
                        MainActivity.CallDialogOkCancel(Resources.GetString(Resource.String.titleError), Resources.GetString(Resource.String.msgPrintCloseBox), Resources.GetString(Resource.String.btnOk), Resources.GetString(Resource.String.btnCancel), null, null);
                    }

                    return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        private async void GetPrints() {
            await CallApi.Get<PrintIn>(MainActivity, $"Print/PackingBoxSlippingSlips?{_boxesId}", SuccessPrintBoxes, ErrorPrintBoxes, UpdatePrintBoxes);
        }
        private void SuccessPrintBoxes(object obj) {
            if (obj == null) {
                return;
            }
        }

        private void ErrorPrintBoxes(object obj, string msg) {
            var errorMessage = msg;
            if (obj != null) {
                var newObj = (PrintIn)obj;
                var newmsg = Helper.FindGoodErrorMessage(newObj.Error.Code);

                errorMessage = !string.IsNullOrEmpty(newmsg) ? newmsg : msg;

                if (newObj.BoxesNotFound.Count > 0) {
                    var boxIds = string.Join(", ", newObj.BoxesNotFound);
                    errorMessage += $"\n \n {Resources.GetString(Resource.String.ErrorBoxNotFound)} \n \n {boxIds}";
                }
            }

            MainActivity.CallDialogOkCancel(Resources.GetString(Resource.String.titleError), $"{Resources.GetString(Resource.String.printBoxError)} \n \n {errorMessage}", Resources.GetString(Resource.String.btnRetry), Resources.GetString(Resource.String.btnCancel), PositiveRestartPrint, null);
        }

        private void PositiveRestartPrint() {
            GetPrints();
        }

        private void UpdatePrintBoxes() {
            _boxesId = "";
            HideCheckbox();
            MainActivity.CallDialogOkCancel(Resources.GetString(Resource.String.titlePrint), Resources.GetString(Resource.String.msgPrintGood), Resources.GetString(Resource.String.btnOk), Resources.GetString(Resource.String.btnCancel), null, null);

        }

        private void ShowCheckbox() {
            foreach (var box in _storagePackaging.InternalBoxes) {
                if (box.IsClosed) {
                    box.IsPrintable = true;
                }
            }
            _boxAdapter.NotifyDataSetChanged();
        }

        private void HideCheckbox() {
            //_storagePackaging.InternalBoxes
            //    .Where(it => !it.IsClosed)
            //    .ToList()
            //    .ForEach(it=> it.IsPrintable = false);
            foreach (var box in _storagePackaging.InternalBoxes) {
                if (box.IsClosed) {
                    box.IsPrintable = false;
                }
            }
            _boxAdapter.NotifyDataSetChanged();
        }

        private void SetSearch(IMenuItem item) {
            var searchView = MenuItemCompat.GetActionView(item);
            _searchView = searchView.JavaCast<SearchView>();

            _searchView.QueryTextChange += (s, e) => _boxAdapter.Filter.InvokeFilter(e.NewText);
            _searchView.QueryTextSubmit += (s, e) => {
                _searchView.ClearFocus();
                e.Handled = true;
            };
        }

        private void WriteStorageDeliveries() {
            var json = StorageHelper.JsonSerialize(_storagePackaging);

            StorageHelper.WriteTextAllAsync($"CustomerId_{_customerId}_SlipId_{_slipId}_CmdType_{CallApi.CurrentCustomersTypes.Id}", json);
        }
    }
}