﻿using System;
using System.Collections.Generic;
using Android.OS;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using Volcan.NanoApp.Entrepot.Helpers;
using Volcan.NanoApp.Entrepot.Models;
using Volcan.NanoApp.Entrepot.Models.Adapter;

namespace Volcan.NanoApp.Entrepot.Fragments {
    public class FragmentHome : BaseFragment {
        //private string TAG = "HOME";

        private Button _btnSupplier;
        private Button _btnCustomer;

        private List<Season> _seasons;
        private TextView _helloUser;
        private TextView _logout;
        private Spinner _spinnerSeason;
        private Spinner _spinnerCustomersType;
        private SeasonArrayAdapter _seasonAdapter;
        private CustomersTypeArrayAdapter _customersTypeAdapter;
        private TextView _notAuthorize;
        private TextView _lblSpinner;
        private TextView _versionName;

        private List<CustomersType> _customersTypes;

        public override void OnResume() {
            base.OnResume();
            ChangeHelloUser();
            HandlerClick();
            MainActivity.ChangeNavigationSelected(Resource.Id.nav_home);
            MainActivity.SupportActionBar.Hide();
        }

        public static FragmentHome NewInstance() {
            var frag = new FragmentHome {
                Arguments = new Bundle(),
                RetainInstance = true
            };

            return frag;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
           
            var view = inflater.Inflate(Resource.Layout.fragmentHome, container, false);
            MainActivity.LockDrawerLayout();

            //Home
            _btnSupplier = view.FindViewById<Button>(Resource.Id.btnSupplier);

            _spinnerCustomersType = view.FindViewById<Spinner>(Resource.Id.spinnerCustomersType);
            _btnCustomer = view.FindViewById<Button>(Resource.Id.btnCustomer);
           
            _helloUser = view.FindViewById<TextView>(Resource.Id.userName);
            _logout = view.FindViewById<TextView>(Resource.Id.logout);
            _spinnerSeason = view.FindViewById<Spinner>(Resource.Id.spinnerSeason);

            _notAuthorize = view.FindViewById<TextView>(Resource.Id.NotAuthorize);
            _lblSpinner = view.FindViewById<TextView>(Resource.Id.lblSpinnerSeason);
            _versionName = view.FindViewById<TextView>(Resource.Id.version);

            _versionName.Text = Context.PackageManager.GetPackageInfo(Context.PackageName, 0).VersionName;

            _logout.Click += delegate {
                _helloUser.Text = "";
                CallApi.CurrentUser = null;
                CallApi.CurrentToken = null;
                CallApi.CurrentSeason = null;
                MainActivity.CallDialogLogin(SuccessLogin);
            };


            if (Arguments.GetString("seasons") != null) {
                ClearBackStack();
                _seasons = JsonConvert.DeserializeObject<List<Season>>(Arguments.GetString("seasons"));
                FillSpinnerSeasons(_spinnerSeason);
                _notAuthorize.Text = "";

            } else {
                _seasons = new List<Season>();
            }

            if (Arguments.GetString("customersType") != null) {
              
                _customersTypes = JsonConvert.DeserializeObject<List<CustomersType>>(Arguments.GetString("customersType"));
                FillCustomerTypeSpinner(_spinnerCustomersType);
                _notAuthorize.Text = "";

            } else {
                _customersTypes = new List<CustomersType>();
                FillCustomersType();
                FillCustomerTypeSpinner(_spinnerCustomersType);
            }

            ShowBtns();

            return view;
        }

        private void ClearBackStack() {
            var fm = MainActivity.SupportFragmentManager;
            for (var i = 0; i < fm.BackStackEntryCount-1; ++i) {
                fm.PopBackStack();
            }
        }
        private async void LoadSeasons() {

            await CallApi.Get<List<Season>>(MainActivity, "inventoryreception/GetSeasons?inEnglish=false", SuccessFillSeasons, ErrorDelegateSeason, UpdateSeason);
        }

        public override void SuccessLogin() {
            MainActivity.ChangeCurrentUser();

            LoadSeasons();
            ShowBtns();
        }

        public void ShowBtns() {
            if (CallApi.CurrentUser == null) { return; }

            if (CallApi.CurrentUser.Roles.Contains("Admin")) {
                _notAuthorize.Visibility = ViewStates.Gone;
                _btnSupplier.Visibility = ViewStates.Visible;
                _btnCustomer.Visibility = ViewStates.Visible;
                
                _lblSpinner.Visibility = ViewStates.Visible;
                _spinnerSeason.Visibility = ViewStates.Visible;
            } else if (CallApi.CurrentUser.Roles.Contains("Employe") || CallApi.CurrentUser.Roles.Contains("TrustedEmploye")) {
                _notAuthorize.Visibility = ViewStates.Gone;
                _btnSupplier.Visibility = ViewStates.Invisible;
                _btnCustomer.Visibility = ViewStates.Visible;
  
                _lblSpinner.Visibility = ViewStates.Visible;
                _spinnerSeason.Visibility = ViewStates.Visible;
            } else {
                _notAuthorize.Text = Resources.GetString(Resource.String.notAutorize);
                _notAuthorize.Visibility = ViewStates.Visible;
                _btnSupplier.Visibility = ViewStates.Invisible;
                _btnCustomer.Visibility = ViewStates.Invisible;
                _spinnerSeason.Visibility = ViewStates.Invisible;
                _lblSpinner.Visibility = ViewStates.Invisible;
            }
        }
        private void HandlerClick() {
            if (_btnSupplier == null || _btnCustomer == null ) {
                return;
            }
            _btnSupplier.Click += btn_click;
            _btnCustomer.Click += btn_click;
        }

        private void SuccessFillSeasons(object obj) {
            if (obj == null) {
                return;
            }
            if (_seasons.Count > 0) {
                return;
            }
            foreach (var season in (List<Season>)obj) {
                _seasons.Add(season);

            }
        }

        private void ErrorDelegateSeason(object obj, string msg) {
            MainActivity.CallDialogOkCancel(Resources.GetString(Resource.String.titleError), $"{Resources.GetString(Resource.String.readSeason)} \n \n {msg}", Resources.GetString(Resource.String.btnRetry), Resources.GetString(Resource.String.btnCancel), Positive, Negative);
        }

        private void UpdateSeason() {
            FillSpinnerSeasons(_spinnerSeason);

        }

        private void Positive() {
            SuccessLogin();
        }
        private void Negative() {
            MainActivity.HideLoader();
        }


        private void FillSpinnerSeasons(AdapterView spinner) {
            spinner.ItemSelected += spinnerSeason_ItemSelected;
            _seasonAdapter = new SeasonArrayAdapter(MainActivity, _seasons);
            _spinnerSeason.Adapter = _seasonAdapter;
        }

        private void FillCustomersType() {
            _customersTypes = new List<CustomersType> {
                new CustomersType{Id = 0, Name = "Placement"},
                new CustomersType{Id = 1, Name = "Consigne"},
                new CustomersType{Id = 2, Name = "Échange\\Retour"},
                new CustomersType{Id = 3, Name = "Liquidation"},
                new CustomersType{Id = 4, Name = "Placement 30%"},
                new CustomersType{Id = 5, Name = "Reprise"}
            };
        }
        private void FillCustomerTypeSpinner(AdapterView spinner) {
            spinner.ItemSelected += spinnerCustomersType_ItemSelected;
            _customersTypeAdapter = new CustomersTypeArrayAdapter(MainActivity, _customersTypes);
            _spinnerCustomersType.Adapter = _customersTypeAdapter;
        }


        private void spinnerSeason_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e) {
            var spinner = (Spinner)sender;
            CallApi.CurrentSeason = _seasons.Find(s => s.Name == spinner.GetItemAtPosition(e.Position).ToString());
        }

        private void spinnerCustomersType_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e) {
            var spinner = (Spinner)sender;
            CallApi.CurrentCustomersTypes = _customersTypes.Find(s => s.Name == spinner.GetItemAtPosition(e.Position).ToString());
            _btnCustomer.Text = $"{Resources.GetString(Resource.String.customers)} ({CallApi.CurrentCustomersTypes.Name})";
        }

        private void btn_click(object sender, EventArgs e) {
            var id = ((Button)sender).Id;

            switch (id) {
                case Resource.Id.btnSupplier:
                    MainActivity.ListItemClicked(2, Resource.Id.nav_supplier);
                    break;
                case Resource.Id.btnCustomer:
                    MainActivity.ListItemClicked(1, Resource.Id.nav_customer);
                    break;
               
            }

        }

        public void ChangeHelloUser() {
            if (CallApi.CurrentUser != null) {
                _helloUser.Text =$"{Resources.GetString(Resource.String.hi)} {CallApi.CurrentUser.Name} | ";
            }
        }

        public override void OnPause() {
            base.OnPause();
            _btnSupplier.Click -= btn_click;
            _btnCustomer.Click -= btn_click;
            Arguments.PutString("seasons", JsonConvert.SerializeObject(_seasons));
            Arguments.PutString("customersType", JsonConvert.SerializeObject(_customersTypes));

        }

        public override void OnDetach() {
            base.OnDetach();
            _btnSupplier.Click -= btn_click;
            _btnCustomer.Click -= btn_click;
        }


    }
}