﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.View;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Volcan.NanoApp.Entrepot.Helpers;
using Volcan.NanoApp.Entrepot.Models;
using Volcan.NanoApp.Entrepot.Models.Adapter;
using SearchView = Android.Support.V7.Widget.SearchView;

namespace Volcan.NanoApp.Entrepot.Fragments {
    public class FragmentShipmentVoucherDetail : BaseFragment {

        private RecyclerView _recyclerView;
        private ShipmentVoucherDetailAdapter _shipmentVoucherDetailAdapter;

        private ShipmentVoucherDetail _shipmentVoucherDetail;
        private List<Style> _styles;
        private Button _btnFinish;

        private SearchView _searchView;
        private Task _apiCall;
        public static FragmentShipmentVoucherDetail NewInstance(string title, string shipmentVoucherId) {
            var frag = new FragmentShipmentVoucherDetail() { Arguments = new Bundle(), Title = title };
            frag.Arguments.PutString("ShipmentVoucherId", shipmentVoucherId);
            frag.Arguments.PutString("SupplierName", title);
            return frag;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            // Use this to return your custom view for this Fragment
            var view = inflater.Inflate(Resource.Layout.fragmentShipmentVoucherDetail, container, false);
            _btnFinish = view.FindViewById<Button>(Resource.Id.btnOrderCompleted);

            _btnFinish.Click += delegate {
                MainActivity.CallDialogOkCancel(Resources.GetString(Resource.String.titleValidate), Resources.GetString(Resource.String.msgValidate), Resources.GetString(Resource.String.btnOk), Resources.GetString(Resource.String.btnCancel), PositiveFirst, Negative);

            };

            //if (_shipmentVoucherDetail == null) {
                _shipmentVoucherDetail = new ShipmentVoucherDetail {Styles = new List<Style>()};
                _styles = _shipmentVoucherDetail.Styles;
                _apiCall = CallApi.Get<ShipmentVoucherDetail>(MainActivity, $"inventoryreception/GetShipmentVoucherDetails?voucherId={Arguments.GetString("ShipmentVoucherId")}", SuccessStyles, ErrorStyles, UpdateStyles);
            //}
            _shipmentVoucherDetailAdapter = new ShipmentVoucherDetailAdapter(_styles, Context);

            // Get our RecyclerView layout:
            _recyclerView = view.FindViewById<RecyclerView>(Resource.Id.recyclerViewShipmentVoucherDetail);

            SetupRecyclerView(Context);
            MainActivity.InvalidateOptionsMenu();

            return view;
        }

      
        private void SuccessStyles(object obj) {
            if (obj == null) {return;}

            _shipmentVoucherDetail = (ShipmentVoucherDetail) obj;
            foreach (var style in _shipmentVoucherDetail.Styles) {
                _styles.Add(style);
            }
        }

        private void ErrorStyles(object obj, string msg) {

            MainActivity.CallDialogOkCancel(Resources.GetString(Resource.String.titleError), $"{Resources.GetString(Resource.String.readStyleShipmentVoucherDetail)} \n \n {msg}", Resources.GetString(Resource.String.btnRetry), Resources.GetString(Resource.String.btnCancel), Positive, Negative);
        }
        private void Positive() {
            _apiCall = CallApi.Get<ShipmentVoucherDetail>(MainActivity, $"inventoryreception/GetShipmentVoucherDetails?voucherId={Arguments.GetString("ShipmentVoucherId")}", SuccessStyles, ErrorStyles, UpdateStyles);
        }

        private void Negative() {
            MainActivity.HideLoader();
        }
        private void UpdateStyles() {
            _shipmentVoucherDetailAdapter.ResetItems();
            _shipmentVoucherDetailAdapter.NotifyDataSetChanged();
        }
        private void PositiveFirst() {

            MainActivity.CallDialogValidate(Resources.GetString(Resource.String.titleDoubleValidation), Resources.GetString(Resource.String.msgDoubleValidation), Resources.GetString(Resource.String.keyDoubleValidation), PositiveFirst, Negative);
        }


        private void SetupRecyclerView(Context context) {
            var layoutManager = new LinearLayoutManager(context);
            _recyclerView.SetLayoutManager(layoutManager);
            _recyclerView.SetItemAnimator(new DefaultItemAnimator());
            _recyclerView.SetAdapter(_shipmentVoucherDetailAdapter);
        }

        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater) {
            base.OnCreateOptionsMenu(menu, inflater);
            menu.FindItem(Resource.Id.menu_scan).SetVisible(true);

            menu.FindItem(Resource.Id.action_search).SetVisible(true);
            var item = menu.FindItem(Resource.Id.action_search);
            SetSearch(item);
        }
        public override bool OnOptionsItemSelected(IMenuItem item) {
            switch (item.ItemId) {
                case Resource.Id.menu_scan:
                    Android.Support.V4.App.Fragment fragment = FragmentScanShipmentVoucher.NewInstance($"Scanner {Arguments.GetString("SupplierName")}",Arguments.GetString("ShipmentVoucherId"));
                    MainActivity.ReplaceFragment(fragment, "ScanSupplierShipmentVoucher");
                    return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        private void SetSearch(IMenuItem item) {
            var searchView = MenuItemCompat.GetActionView(item);
            _searchView = searchView.JavaCast<SearchView>();

            _searchView.QueryTextChange += (s, e) => _shipmentVoucherDetailAdapter.Filter.InvokeFilter(e.NewText);
            _searchView.QueryTextSubmit += (s, e) => {
                _searchView.ClearFocus();
                e.Handled = true;
            };
        }
    }
}