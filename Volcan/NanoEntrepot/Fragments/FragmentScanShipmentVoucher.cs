﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Clans.Fab;
using Volcan.NanoApp.Entrepot.Helpers;
using Volcan.NanoApp.Entrepot.Models;
using Volcan.NanoApp.Entrepot.Models.Adapter;
using Keycode = Android.Views.Keycode;

namespace Volcan.NanoApp.Entrepot.Fragments {
    public class FragmentScanShipmentVoucher : BaseFragment, View.IOnClickListener {
        private RecyclerView _recyclerView;
        private ScannedShipmentVoucherAdapter _scannedShipmentVoucherAdapter;
        private List<ScannedShipmentVoucher> _scannedShipmentVouchers;
        private FloatingActionMenu _fabDel;
        private FloatingActionMenu _fabEdit;
        private ScannedShipmentVoucher _selectedShipmentVoucher;

        private int _addQte;
        private EditText _scanUpc;
        private ImageView _imgView;
        private TextView _noStyle;
        private TextView _size;
        private TextView _color;

        private ScannedShipmentVoucher _addScannedShipmentVoucher;

        public static FragmentScanShipmentVoucher NewInstance(string title, string shipmentVoucherId) {
            var frag = new FragmentScanShipmentVoucher() { Arguments = new Bundle(), Title = title };
            frag.Arguments.PutString("ShipmentVoucherId", shipmentVoucherId);
            return frag;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


            var view = inflater.Inflate(Resource.Layout.fragmentScanShipmentVoucher, container, false);
            _imgView = view.FindViewById<ImageView>(Resource.Id.imgShipmentVoucher);
            _noStyle = view.FindViewById<TextView>(Resource.Id.titleNoStyle);
            _size = view.FindViewById<TextView>(Resource.Id.titleSize);
            _color = view.FindViewById<TextView>(Resource.Id.titleColor);
            _fabDel = view.FindViewById<FloatingActionMenu>(Resource.Id.delete);
            _fabEdit = view.FindViewById<FloatingActionMenu>(Resource.Id.menu_edit);
            _scanUpc = view.FindViewById<EditText>(Resource.Id.scanUPC);

            _scannedShipmentVouchers = new List<ScannedShipmentVoucher>();
            _scanUpc.SetCursorVisible(false);
            _scanUpc.RequestFocus();



            _scanUpc.KeyPress += (sender, e) => {
                if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter) {
                    GunClick();
                    _scanUpc.Text = "";
                } else {
                    e.Handled = false;
                }
            };

            HideImageSide();

            _fabDel.Visibility = ViewStates.Invisible;
            _fabDel.IconAnimated = false;
            _fabDel.SetOnMenuButtonClickListener(this);
            view.SetOnClickListener(this);

            #region fabClick
            var fabP1 = view.FindViewById<FloatingActionButton>(Resource.Id.p1);
            fabP1.Click += ProgramFab1_Click;
            var fabP5 = view.FindViewById<FloatingActionButton>(Resource.Id.p5);
            fabP5.Click += ProgramFab1_Click;
            var fabP10 = view.FindViewById<FloatingActionButton>(Resource.Id.p10);
            fabP10.Click += ProgramFab1_Click;
            var fabP12 = view.FindViewById<FloatingActionButton>(Resource.Id.p12);
            fabP12.Click += ProgramFab1_Click;

            var fabM1 = view.FindViewById<FloatingActionButton>(Resource.Id.m1);
            fabM1.Click += ProgramFab1_Click;
            var fabM5 = view.FindViewById<FloatingActionButton>(Resource.Id.m5);
            fabM5.Click += ProgramFab1_Click;
            var fabM10 = view.FindViewById<FloatingActionButton>(Resource.Id.m10);
            fabM10.Click += ProgramFab1_Click;
            var fabM12 = view.FindViewById<FloatingActionButton>(Resource.Id.m12);
            fabM12.Click += ProgramFab1_Click;
            #endregion

            _scannedShipmentVoucherAdapter = new ScannedShipmentVoucherAdapter(Context, _scannedShipmentVouchers);
            _scannedShipmentVoucherAdapter.ItemClick += OnItemClick;

            _recyclerView = view.FindViewById<RecyclerView>(Resource.Id.recyclerViewScannedShipmentVoucher);

            SetupRecyclerView(Context);


            return view;
        }

        private void ChangeSelected() {
            if (_scannedShipmentVouchers.Any(it => it.IsSelected && _selectedShipmentVoucher.Timestamp != it.Timestamp)) {

                var selecteds = _scannedShipmentVouchers.Where(it => it.IsSelected && _selectedShipmentVoucher.Timestamp != it.Timestamp);

                foreach (var scannedShipmentVoucher in selecteds) {
                    var pos = _scannedShipmentVouchers.IndexOf(scannedShipmentVoucher);
                    scannedShipmentVoucher.IsSelected = false;
                    _scannedShipmentVoucherAdapter.NotifyItemChanged(pos);
                }
            }
        }

        private int GetPositionSelectedShipmentVoucher() {
            return _scannedShipmentVouchers.IndexOf(_selectedShipmentVoucher);
        }

        private void SetupRecyclerView(Context context) {
            var layoutManager = new LinearLayoutManager(context);
            _recyclerView.SetLayoutManager(layoutManager);
            _recyclerView.SetItemAnimator(new DefaultItemAnimator());
            _recyclerView.SetAdapter(_scannedShipmentVoucherAdapter);
        }

        private void OnItemClick(object sender, int position) {
            if (_fabEdit.IsOpened) {
                _fabEdit.Close(true);
            }

            var adapter = (ScannedShipmentVoucherAdapter)sender;
            _selectedShipmentVoucher = adapter.GetSelectedItem();
            adapter.NotifyDataSetChanged();

            if (_selectedShipmentVoucher != null) {
                ShowImageSide();
            } else {
                HideImageSide();
            }
        }

        private void HideImageSide() {
            _imgView.SetImageBitmap(null);
            _noStyle.Text = "";
            _size.Text = "";
            _color.Text = "";
            _fabDel.Visibility = ViewStates.Invisible;
            _fabEdit.Visibility = ViewStates.Invisible;
        }

        private void ShowImageSide() {
            HideImageSide();

            if (_selectedShipmentVoucher.Style != null) {
                if (_selectedShipmentVoucher.Style.ImagePath != "") {
                    var imageBitmap = Helper.GetImageBitmapFromUrl(_selectedShipmentVoucher.Style.ImagePath, MainActivity);
                    if (imageBitmap != null) {
                        _imgView.SetImageBitmap(imageBitmap);
                    }
                }

                _noStyle.Text = _selectedShipmentVoucher.Style.Name;
                _size.Text = _selectedShipmentVoucher.Style.Size;
                _color.Text = _selectedShipmentVoucher.Style.Color;
                _fabEdit.Visibility = ViewStates.Visible;
            } else {
                _noStyle.Text = $"{Resources.GetString(Resource.String.upc)} {_selectedShipmentVoucher.Upc}";
            }
            _fabDel.Visibility = ViewStates.Visible;

        }


        private void ChangeInfo(int position) {
            _scannedShipmentVoucherAdapter.NotifyItemChanged(position);

            ChangeSelected();

            ShowImageSide();

        }



        #region ADD ITEM

        private void GunClick() {
            _selectedShipmentVoucher = new ScannedShipmentVoucher { Upc = _scanUpc.Text, ShipmentVoucherId = Arguments.GetString("ShipmentVoucherId"), Timestamp = Helper.GetTimestamp() };

            _scannedShipmentVouchers.Insert(0, _selectedShipmentVoucher);
            _scannedShipmentVoucherAdapter.NotifyItemInserted(0);
            _recyclerView.ScrollToPosition(0);
            LoadAddItem();

        }

        private async void LoadAddItem() {
            await CallApi.Post<ScannedShipmentVoucher, ScannedShipmentVoucher>(MainActivity, "inventoryreception/ScanItem", _selectedShipmentVoucher, SuccessScan, ErrorScan, UpdateScan);
        }

        private void SuccessScan(object obj) {
            if (obj == null) { return; }

            var newObj = (ScannedShipmentVoucher)obj;
            _addScannedShipmentVoucher = new ScannedShipmentVoucher {
                Timestamp = newObj.Timestamp,
                ShipmentVoucherId = newObj.ShipmentVoucherId,
                Total = newObj.Total,
                Upc = newObj.Upc,
                Style = newObj.Style,
                IsSelected = true,
                IsPending = false,
                Error = newObj.Error,
                ServerTotal = newObj.Total
            };
        }

        private void ErrorScan(object obj, string msg) {
            if (obj == null) {  
                return;
            }
            var newObj = (ScannedShipmentVoucher)obj;


            _addScannedShipmentVoucher = new ScannedShipmentVoucher {
                Timestamp = newObj.Timestamp,
                ShipmentVoucherId = newObj.ShipmentVoucherId,
                Total = newObj.Total,
                Upc = newObj.Upc,
                Style = newObj.Style,
                IsSelected = true,
                IsPending = false,
                Error = newObj.Error,
                ServerTotal = newObj.Total
            };

            if (newObj.Error == null) {
                _addScannedShipmentVoucher.Error = new Error {Message = msg};
            } else {
                var newmsg = Helper.FindGoodErrorMessage(newObj.Error.Code);
                if (!string.IsNullOrEmpty(newmsg)) {
                    _addScannedShipmentVoucher.Error.Message = newmsg;
                }
            }
           
            
            ChangeInfo(PutAddItemIntoListReturnPosition());
            MainActivity.ErrorSound();
        }

        private void UpdateScan() {

            ChangeInfo(PutAddItemIntoListReturnPosition());
        }

        private int PutAddItemIntoListReturnPosition() {
            _selectedShipmentVoucher = _addScannedShipmentVoucher;

            var position = _scannedShipmentVouchers.FindIndex(it => it.Timestamp == _addScannedShipmentVoucher.Timestamp);
            _scannedShipmentVouchers[position] = _addScannedShipmentVoucher;
            return position;
        }

        #endregion

        #region EDIT ITEM
        private async void LoadEditItem() {
            _selectedShipmentVoucher.IsPending = true;
            _scannedShipmentVoucherAdapter.NotifyDataSetChanged();
            await CallApi.Put<ScannedShipmentVoucher, ScannedShipmentVoucher>(MainActivity, "inventoryreception/ScanItem", _selectedShipmentVoucher, SuccessEdit, ErrorEdit, UpdateEdit);
        }

        private void SuccessEdit(object obj) {
            if (obj == null) { return; }

            var newObj = (ScannedShipmentVoucher)obj;
            var position = _scannedShipmentVouchers.FindIndex(it => it.Timestamp == newObj.Timestamp);
            _scannedShipmentVouchers[position].Total = newObj.Total;
            _scannedShipmentVouchers[position].Style.Total = newObj.Total;
            _scannedShipmentVouchers[position].ServerTotal = newObj.Total;
            _scannedShipmentVouchers[position].Error = newObj.Error;
            ChangeInfo(position);


        }

        private void ErrorEdit(object obj, string msg) {
            if (obj == null) { return; }

            var newObj = (ScannedShipmentVoucher)obj;
            var position = _scannedShipmentVouchers.FindIndex(it => it.Timestamp == newObj.Timestamp);
            _scannedShipmentVouchers[position].Total = newObj.ServerTotal;
            _scannedShipmentVouchers[position].Style.Total = newObj.ServerTotal;
            _scannedShipmentVouchers[position].Error = newObj.Error;

            var newmsg = Helper.FindGoodErrorMessage(newObj.Error.Code);
            if (!string.IsNullOrEmpty(newmsg)) {
                _scannedShipmentVouchers[position].Error.Message = newmsg;
            }
            if (newObj.Error == null) {
                _scannedShipmentVouchers[position].Error = new Error { Message = msg };
            }
            ChangeInfo(position);
        }

        private void UpdateEdit() {
            _selectedShipmentVoucher.IsPending = false;
            _scannedShipmentVoucherAdapter.NotifyDataSetChanged();
        }

        private void ProgramFab1_Click(object sender, EventArgs e) {
            var fab = sender as FloatingActionButton;
            var position = GetPositionSelectedShipmentVoucher();
            var qte = 0;
            if (fab == null) { return; }
            switch (fab.Id) {
                case Resource.Id.p1:
                    qte = 1;
                    break;
                case Resource.Id.p5:
                    qte = _selectedShipmentVoucher.Style.Total == 1 ? 4 : 5;
                    break;
                case Resource.Id.p10:
                    qte = _selectedShipmentVoucher.Style.Total == 1 ? 9 : 10;
                    break;
                case Resource.Id.p12:
                    qte = _selectedShipmentVoucher.Style.Total == 1 ? 11 : 12;
                    break;
                case Resource.Id.m1:
                    qte = -1;
                    break;
                case Resource.Id.m5:
                    qte = -5;
                    break;
                case Resource.Id.m10:
                    qte = -10;
                    break;
                case Resource.Id.m12:
                    qte = -12;
                    break;
            }
            SpeedEditScanned(qte);
            _scannedShipmentVoucherAdapter.NotifyItemChanged(position);
            LoadEditItem();
        }

        private void SpeedEditScanned(int qte) {
            if (qte < 0 && _selectedShipmentVoucher.Style.Total + qte <= 1) {
                _selectedShipmentVoucher.Style.Total = 1;
            } else {
                _addQte = qte;
                PositiveAddCallback();
            }
            _selectedShipmentVoucher.Total = _selectedShipmentVoucher.Style.Total;

        }

        private void PositiveAddCallback() {
            _selectedShipmentVoucher.Style.Total += _addQte;
            var position = GetPositionSelectedShipmentVoucher();
            _scannedShipmentVoucherAdapter.NotifyItemChanged(position);
        }
        #endregion

        #region DELETE ITEM

        private async void ApiDelItem() {
            _selectedShipmentVoucher.IsPending = true;
            _scannedShipmentVoucherAdapter.NotifyDataSetChanged();
            await CallApi.Delete<ScannedShipmentVoucher>(MainActivity, $"inventoryreception/ScanItem/{_selectedShipmentVoucher.Timestamp}", _selectedShipmentVoucher, SuccessDel, ErrorDel, UpdateDel);
        }

        private void SuccessDel(object obj) {
            if (obj == null) { return; }

            var newObj = (ScannedShipmentVoucher)obj;
            var position = _scannedShipmentVouchers.FindIndex(it => it.Timestamp == newObj.Timestamp);
            _scannedShipmentVouchers[position].Error = newObj.Error;
            _selectedShipmentVoucher = _scannedShipmentVouchers[position];
        }

        private void ErrorDel(object obj, string msg) {
            if (obj == null) { return; }

            var newObj = (ScannedShipmentVoucher)obj;

            var position = _scannedShipmentVouchers.FindIndex(it => it.Timestamp == newObj.Timestamp);
            if (newObj.Style == null) {
                _selectedShipmentVoucher = _scannedShipmentVouchers[position];
                DeleteItem(position);
                return;
            }
            _scannedShipmentVouchers[position].Error = newObj.Error;
            var newmsg = Helper.FindGoodErrorMessage(newObj.Error.Code);
            if (!string.IsNullOrEmpty(newmsg)) {
                _scannedShipmentVouchers[position].Error.Message = newmsg;
            }
            if (newObj.Error == null) {
                _scannedShipmentVouchers[position].Error = new Error { Message = msg };
            }
            ChangeInfo(position);
        }

        private void UpdateDel() {
            var position = GetPositionSelectedShipmentVoucher();
            DeleteItem(position);


        }

        private void DeleteItem(int position) {
            _scannedShipmentVouchers.Remove(_selectedShipmentVoucher);
            _scannedShipmentVoucherAdapter.NotifyItemRemoved(position);
            _scannedShipmentVoucherAdapter.UnselectItem();
            HideImageSide();
        }
        public void OnClick(View v) {
            if (_fabEdit.IsOpened) {
                _fabEdit.Close(true);
            }
            var parent = v.Parent as FloatingActionMenu;
            var menu = parent;
            if (menu?.Id == Resource.Id.delete) {
                MainActivity.CallDialogOkCancel(Resources.GetString(Resource.String.titleDelete), Resources.GetString(Resource.String.ConfirmeDelete), Resources.GetString(Resource.String.yes), Resources.GetString(Resource.String.no), PositiveDeleteCallback, null);
            }
        }

        private void PositiveDeleteCallback() {
            ApiDelItem();
        }
        #endregion

    }
}