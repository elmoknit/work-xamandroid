﻿using System;
using System.Globalization;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using Newtonsoft.Json;
using Volcan.NanoApp.Entrepot.Helpers;
using Volcan.NanoApp.Entrepot.Models;

namespace Volcan.NanoApp.Entrepot.Fragments.Dialog {
    public class DialogFragmentDimensions : DialogFragment {
        private EditText _editTextWidth;
        private EditText _editTextHeight;
        private EditText _editTextDepth;
        private EditText _editTextWeigth;
        private Button _btnClose;
        private TextView _txtError;

        private readonly ApiCallbackLogin.SuccessWithResultDelegate _success;

        public DialogFragmentDimensions(ApiCallbackLogin.SuccessWithResultDelegate success) {
            _success = success;
        }

        public static DialogFragmentDimensions NewInstance(Bundle bundle, ApiCallbackLogin.SuccessWithResultDelegate success, Dimension dimension) {
            var fragment = new DialogFragmentDimensions(success);
            var args = new Bundle();
            if (dimension != null) {
                args.PutString("dimension", JsonConvert.SerializeObject(dimension));
            }
            fragment.Arguments = args;
            return fragment;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            var view = inflater.Inflate(Resource.Layout.DialogDimensions, container, false);

           
            _editTextWidth = view.FindViewById<EditText>(Resource.Id.etWidth);
            _editTextHeight = view.FindViewById<EditText>(Resource.Id.etHeight);
            _editTextDepth = view.FindViewById<EditText>(Resource.Id.etDepth);
            _editTextWeigth = view.FindViewById<EditText>(Resource.Id.etWeight);

            if (Arguments.ContainsKey("dimension")) {
                var dim = JsonConvert.DeserializeObject<Dimension>(Arguments.GetString("dimension"));
                if (dim != null) {
                    _editTextWidth.Text = dim.Width.ToString(CultureInfo.InvariantCulture);
                    _editTextHeight.Text = dim.Heigth.ToString(CultureInfo.InvariantCulture);
                    _editTextDepth.Text = dim.Depth.ToString(CultureInfo.InvariantCulture);
                    _editTextWeigth.Text = dim.Weight.ToString(CultureInfo.InvariantCulture);
                }
            }

            _editTextWeigth.EditorAction += delegate (object sender, TextView.EditorActionEventArgs args) {
                if (args.ActionId == ImeAction.Done) {
                    DoneDimension();
                }
            };

            _btnClose = view.FindViewById<Button>(Resource.Id.btnCloseBox);
            _txtError = view.FindViewById<TextView>(Resource.Id.lblErrorDimensions);
            _btnClose.Click += Ok_Click;

            return view;
        }

        private void Ok_Click(object sender, EventArgs e) {
            DoneDimension();
        }

        private void DoneDimension() {
            _txtError.Text = "";

            if (string.IsNullOrEmpty(_editTextWidth.Text) || string.IsNullOrEmpty(_editTextHeight.Text) || string.IsNullOrEmpty(_editTextDepth.Text) || string.IsNullOrEmpty(_editTextWeigth.Text)) {
                _txtError.Text = Resources.GetString(Resource.String.emptyField);
                return;
            }
            var dept = _editTextDepth.Text.Replace('.', ',');
            var height = _editTextHeight.Text.Replace('.', ',');
            var width = _editTextWidth.Text.Replace('.', ',');
            var weight = _editTextWeigth.Text.Replace('.', ',');
            var dimensions = new Dimension { Depth = Convert.ToDouble(dept), Heigth = Convert.ToDouble(height), Weight = Convert.ToDouble(weight), Width = Convert.ToDouble(width) };


            _success?.Invoke(dimensions);
            Dismiss();
        }
    }
}