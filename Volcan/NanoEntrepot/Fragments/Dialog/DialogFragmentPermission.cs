﻿using System;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using Volcan.NanoApp.Entrepot.Helpers;
using Volcan.NanoApp.Entrepot.Models;

namespace Volcan.NanoApp.Entrepot.Fragments.Dialog {
    public class DialogFragmentPermission : DialogFragment {
        private EditText _editTextUser;
        private EditText _editTextPwd;
        private TextView _txtErrorDialog;
        private TextView _txtMsg;
        private Button _btnPermission;
        private Button _btnCancelPermission;
        private readonly DialogCallbackButton.PositiveDelegate _positive;
        private readonly DialogCallbackButton.NegativeDelegate _negative;

        private UserIn _userPermission;

        public DialogFragmentPermission(DialogCallbackButton.PositiveDelegate positive, DialogCallbackButton.NegativeDelegate negative) {
            _positive = positive;
            _negative = negative;
        }

        public static DialogFragmentPermission NewInstance(string msg, DialogCallbackButton.PositiveDelegate positive, DialogCallbackButton.NegativeDelegate negative) {
            var fragment = new DialogFragmentPermission(positive, negative);
            var args = new Bundle();

            args.PutString("msg", msg);
            fragment.Arguments = args;
            return fragment;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            var view = inflater.Inflate(Resource.Layout.DialogPermission, container, false);
            _editTextUser = view.FindViewById<EditText>(Resource.Id.etUserPermission);
            _editTextPwd = view.FindViewById<EditText>(Resource.Id.etPwdPermission);
            _txtErrorDialog = view.FindViewById<TextView>(Resource.Id.lblErrorPermission);
            _txtMsg = view.FindViewById<TextView>(Resource.Id.lblMsgPermisison);

            _btnPermission = view.FindViewById<Button>(Resource.Id.btnPermission);
            _btnCancelPermission = view.FindViewById<Button>(Resource.Id.btnCancelPermission);



            _txtMsg.Text = Arguments.GetString("msg");
            _editTextPwd.EditorAction += delegate (object sender, TextView.EditorActionEventArgs args) {
                if (args.ActionId == ImeAction.Done) {
                    Authorize();
                }
            };

            _btnPermission.KeyPress += (sender, e) => {
                if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter) {
                    e.Handled = true;
                } else {
                    e.Handled = false;
                }
            };
            _btnCancelPermission.KeyPress += (sender, e) => {
                if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter) {
                    e.Handled = true;
                } else {
                    e.Handled = false;
                }
            };

            _btnCancelPermission.Click += delegate {
                _negative?.Invoke();
                Dismiss();
            };

            _btnPermission.Click += Ok_Click;

            return view;
        }

        private void Ok_Click(object sender, EventArgs e) {

            Authorize();

        }

        private void Authorize() {
            _txtErrorDialog.Text = "";

            if (string.IsNullOrEmpty(_editTextUser.Text) || string.IsNullOrEmpty(_editTextPwd.Text)) {
                _txtErrorDialog.Text = Resources.GetString(Resource.String.emptyField);
                return;
            }

            GetUserPermission();
        }

        private async void GetUserPermission() {
            await CallApi.GetUserRole((MainActivity)Activity, $"{_editTextUser.Text}{Resources.GetString(Resource.String.endEmail)}", _editTextPwd.Text, SuccessPermission, ErrorPermission, UpdatePermission);
        }

        private void SuccessPermission(object obj) {
            if (obj == null) {return;}
            _userPermission = (UserIn) obj;

        }

        private void ErrorPermission(object obj, string msg) {
            if (msg == Resources.GetString(Resource.String.badRequest)) {
                _txtErrorDialog.Text = Resources.GetString(Resource.String.errorEmployeNotFound);
            } else {
                var errorMessage = msg;
                if (obj != null) {
                    var newObj = (UserIn)obj;
                    var newmsg = Helper.FindGoodErrorMessage(newObj.Error.Code);

                    errorMessage = !string.IsNullOrEmpty(newmsg) ? newmsg : msg;
                }
                _txtErrorDialog.Text = errorMessage;
            }
        }

        private void UpdatePermission() {
            if (_userPermission.Roles.Contains("Admin")) {
                Dismiss();
                _positive?.Invoke();
                
            } else {
                _txtErrorDialog.Text = Resources.GetString(Resource.String.adminRequired);
            }
        }
    }
}