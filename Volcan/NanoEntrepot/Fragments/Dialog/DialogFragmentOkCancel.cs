﻿using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Volcan.NanoApp.Entrepot.Helpers;

namespace Volcan.NanoApp.Entrepot.Fragments.Dialog {
    public class DialogFragmentOkCancel : DialogFragment {

        public DialogFragmentOkCancel(DialogCallbackButton.PositiveDelegate positive, DialogCallbackButton.NegativeDelegate negative) {
            _positive = positive;
            _negative = negative;
        }


        private TextView _txtTitle;
        private TextView _txtMsg;
        private Button _btnOk;
        private Button _btnCancel;
        private readonly DialogCallbackButton.PositiveDelegate _positive;
        private readonly DialogCallbackButton.NegativeDelegate _negative;

        public static DialogFragmentOkCancel NewInstance(string title, string msg, string btnOk, string btnCancel, DialogCallbackButton.PositiveDelegate positive, DialogCallbackButton.NegativeDelegate negative) {
            var fragment = new DialogFragmentOkCancel(positive, negative);
            var args = new Bundle();

            args.PutString("title", title);
            args.PutString("msg", msg);
            args.PutString("Ok", btnOk);
            args.PutString("Cancel", btnCancel);
            fragment.Arguments = args;
            return fragment;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            var view = inflater.Inflate(Resource.Layout.DialogOKCancel, container, false);
            _txtTitle = view.FindViewById<TextView>(Resource.Id.lblTitleAlert);
            _txtMsg = view.FindViewById<TextView>(Resource.Id.msgAlert);
            _btnCancel = view.FindViewById<Button>(Resource.Id.btnCancelAlert);
            _btnOk = view.FindViewById<Button>(Resource.Id.btnOkAlert);
            if (Arguments.ContainsKey("title") && Arguments.ContainsKey("msg") && Arguments.ContainsKey("Ok") && Arguments.ContainsKey("Cancel")) {
                _txtTitle.Text = Arguments.GetString("title");
                _txtMsg.Text = Arguments.GetString("msg");
                _btnOk.Text = Arguments.GetString("Ok");
                _btnCancel.Text = Arguments.GetString("Cancel");

            }

            _btnCancel.KeyPress += (sender, e) => {
                if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter) {
                    e.Handled = true;
                } else {
                    e.Handled = false;
                }
            };
            _btnOk.KeyPress += (sender, e) => {
                if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter) {
                    e.Handled = true;
                } else {
                    e.Handled = false;
                }
            };

            _btnCancel.Click += delegate {
                Dismiss();
                _negative?.Invoke();
            };

            _btnOk.Click += delegate {
                Dismiss();
                _positive?.Invoke();
            };

            return view;
        }
    }
}