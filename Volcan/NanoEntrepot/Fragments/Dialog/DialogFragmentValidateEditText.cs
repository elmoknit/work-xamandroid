﻿using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Volcan.NanoApp.Entrepot.Helpers;

namespace Volcan.NanoApp.Entrepot.Fragments.Dialog {
    public class DialogFragmentValidateEditText : DialogFragment {

        public DialogFragmentValidateEditText(DialogCallbackButton.PositiveDelegate positive, DialogCallbackButton.NegativeDelegate negative) {
            _positive = positive;
            _negative = negative;
        }

        private TextView _txtTitle;
        private TextView _txtMsg;
        private TextView _error;
        private EditText _etValidate;
        private Button _btnOk;
        private Button _btnCancel;
        private readonly DialogCallbackButton.PositiveDelegate _positive;
        private readonly DialogCallbackButton.NegativeDelegate _negative;

        public static DialogFragmentValidateEditText NewInstance(string title, string msg, string key, DialogCallbackButton.PositiveDelegate positive, DialogCallbackButton.NegativeDelegate negative) {
            var fragment = new DialogFragmentValidateEditText(positive, negative);
            var args = new Bundle();

            args.PutString("title", title);
            args.PutString("msg", msg);
            args.PutString("key", key);
            fragment.Arguments = args;
            return fragment;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            var view = inflater.Inflate(Resource.Layout.DialogValidateEditText, container, false);

            _txtTitle = view.FindViewById<TextView>(Resource.Id.lblTitleAlert);
            _txtMsg = view.FindViewById<TextView>(Resource.Id.msgAlert);

            _etValidate = view.FindViewById<EditText>(Resource.Id.etValidate);
            _error = view.FindViewById<TextView>(Resource.Id.lblErrorValidate);
            if (Arguments.ContainsKey("title") && Arguments.ContainsKey("msg")) {
                _txtTitle.Text = Arguments.GetString("title");
                _txtMsg.Text = Arguments.GetString("msg");
            }

            _btnCancel = view.FindViewById<Button>(Resource.Id.btnCancelAlert);
            _btnOk = view.FindViewById<Button>(Resource.Id.btnOkAlert);

            _btnCancel.Click += delegate {
                _negative?.Invoke();
                Dismiss();
            };

            _btnOk.Click += delegate {
                if (_etValidate.Text == Arguments.GetString("key")) {
                    _positive?.Invoke();
                    Dismiss();
                } else {
                    _error.Text = Resources.GetString(Resource.String.wrongKey);
                }
                
            };
            return view;
        }
    }
}