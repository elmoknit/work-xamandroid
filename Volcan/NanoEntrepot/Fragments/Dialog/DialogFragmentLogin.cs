﻿using System;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using Volcan.NanoApp.Entrepot.Helpers;
using Volcan.NanoApp.Entrepot.Models;

namespace Volcan.NanoApp.Entrepot.Fragments.Dialog {
    public class DialogFragmentLogin : DialogFragment {
        private EditText _editTextUser;
        private EditText _editTextPwd;
        private TextView _txtErrorDialog;
        private Button _btnLogin;
        private readonly ApiCallbackLogin.SuccessDelegate _success;
        private View _view;
        public DialogFragmentLogin(ApiCallbackLogin.SuccessDelegate success) {
            _success = success;

        }

        public static DialogFragmentLogin NewInstance(Bundle bundle, ApiCallbackLogin.SuccessDelegate success) {
            var fragment = new DialogFragmentLogin(success) { Arguments = bundle };

            return fragment;
        }


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            _view = inflater.Inflate(Resource.Layout.DialogLogin, container, false);

            _editTextUser = _view.FindViewById<EditText>(Resource.Id.etUser);
            _editTextPwd = _view.FindViewById<EditText>(Resource.Id.etPwd);
            _txtErrorDialog = _view.FindViewById<TextView>(Resource.Id.lblErrorLogin);
            _btnLogin = _view.FindViewById<Button>(Resource.Id.btnLogin);
            _btnLogin.Click += Ok_Click;

            _editTextUser.RequestFocus();




            _editTextPwd.EditorAction += delegate (object sender, TextView.EditorActionEventArgs args) {
                if (args.ActionId == ImeAction.Done) {


                    Login();
                }

            };

            return _view;
        }



        private void Ok_Click(object sender, EventArgs e) {
            Login();
        }

        private void Login() {

            _txtErrorDialog.Text = "";

            if (string.IsNullOrEmpty(_editTextUser.Text) || string.IsNullOrEmpty(_editTextPwd.Text)) {
                _txtErrorDialog.Text = Resources.GetString(Resource.String.emptyField);
                return;
            }

            var ma = (MainActivity)Activity;
            ma.HideKeyboard((InputMethodManager)_editTextPwd.Context.GetSystemService("input_method"));

            Token();

        }

        private async void Token() {
            await CallApi.GetToken((MainActivity)Activity, $"{_editTextUser.Text}{Resources.GetString(Resource.String.endEmail)}", _editTextPwd.Text, SuccessToken, ErrorToken, UpdateToken);
        }

        private void SuccessToken(object obj) {
            if (obj != null) {
                var token = (Token)obj;
                CallApi.CurrentToken = token.Value;
            }

        }
        private void ErrorToken(object obj, string msg) {
            var ma = (MainActivity)Activity;
            if (msg == Resources.GetString(Resource.String.badRequest)) {
                _txtErrorDialog.Text = Resources.GetString(Resource.String.errorEmployeNotFound);
            } else {
                ma.CallDialogOkCancel(Resources.GetString(Resource.String.titleError), $"{Resources.GetString(Resource.String.errorAuth)} \n \n {msg}", Resources.GetString(Resource.String.btnRetry), Resources.GetString(Resource.String.btnCancel), Positive, Negative);
            }

        }

        private async void Positive() {
            await CallApi.GetToken((MainActivity)Activity, $"{_editTextUser.Text}{Resources.GetString(Resource.String.endEmail)}", _editTextPwd.Text, SuccessToken, ErrorToken, UpdateToken);
        }

        private void Negative() {
            var ma = (MainActivity) Activity;
            ma.HideLoader();
        }

        private async void UpdateToken() {
            await CallApi.Get<User>((MainActivity)Activity, "identity/GetUserInfo", SuccessGetUser, ErrorGetUser, UpdateGetUser);
            Dismiss();
        }
        private void SuccessGetUser(object obj) {
            if (obj != null) {
                CallApi.CurrentUser = (User)obj;

            }
        }
        private void ErrorGetUser(object obj, string msg) {
            var errorMessage = msg;
            if (obj != null) {
                var newObj = (StylesOut)obj;
                var newmsg = Helper.FindGoodErrorMessage(newObj.Error.Code);

                errorMessage = !string.IsNullOrEmpty(newmsg) ? newmsg : msg;
            }



            _txtErrorDialog.Text = errorMessage;

        }

        private void UpdateGetUser() {
            var ma = (MainActivity)Activity;
            ma.ChangerNavigationMenuLoggin();
            _success?.Invoke();

        }

    }
}