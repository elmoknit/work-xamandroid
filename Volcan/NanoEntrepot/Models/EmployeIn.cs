﻿using System.Collections.Generic;

namespace Volcan.NanoApp.Entrepot.Models {
    public class EmployeIn : In {
        public List<string> EmployeIds { get; set; }
    }
}