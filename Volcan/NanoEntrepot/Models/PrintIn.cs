﻿using System.Collections.Generic;

namespace Volcan.NanoApp.Entrepot.Models {
    public class PrintIn: In {
        public List<int> BoxIds { get; set; }
        public List<int> BoxesNotFound { get; set; }

    }
}