﻿namespace Volcan.NanoApp.Entrepot.Models {
    public class StyleDeliveryIn : In{
        public StyleDelivery Style { get; set; }
        public string Upc { get; set; }
    }
}