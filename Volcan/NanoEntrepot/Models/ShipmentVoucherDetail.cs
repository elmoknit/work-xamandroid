﻿using System.Collections.Generic;

namespace Volcan.NanoApp.Entrepot.Models {
    class ShipmentVoucherDetail {

        public string Id { get; set; }
        public List<Style> Styles { get; set; }

    }
}