﻿namespace Volcan.NanoApp.Entrepot.Models {
    public class Error {
        public int Code { get; set; }
        public string Message { get; set; }
    }

    public enum CodeError {
        DbErrorPrimaryKeyViolation = 10,
        DbErrorSaveError = 20,
        DbErrorOnDelete = 30,
        DbErrorForeignKeyViolation = 40,
        DbErrorUnknown = 90,
        RequiredFieldsMissing = 100,
        ItemNotFound = 110,
        UserPasswordNotChanged = 200,
        ScanUpcUnknown = 500,
        ScanUpcNotPartOfVoucher = 510,
        SeasonNotFound = 700
}

}

