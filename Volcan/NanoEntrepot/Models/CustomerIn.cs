﻿using System.Collections.Generic;

namespace Volcan.NanoApp.Entrepot.Models {
    public class CustomerIn:In {
      
        public List<Customer> Customers { get; set; }  
    }
}