﻿namespace Volcan.NanoApp.Entrepot.Models {
    public class RecyclerViewItem {

        public bool IsSelected { get; set; }
        public bool IsPending { get; set; }
        public Error Error { get; set; }

        public RecyclerViewItem() {
            IsPending = true;
            IsSelected = false;
            Error = null;
        }
    }
}