﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Android.Support.V4.Content;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Java.Lang;
using Volcan.NanoApp.Entrepot.Fragments;
using Volcan.NanoApp.Entrepot.Helpers;

namespace Volcan.NanoApp.Entrepot.Models.Adapter {
    public class SupplierAdapter : RecyclerView.Adapter, IFilterable {

        protected List<Supplier> Suppliers;
        protected readonly List<Supplier> OriginalSuppliers;
        private ShipmentVoucherAdapter _shipmentVoucherAdapter;
        private readonly Android.Support.V4.App.FragmentManager _fragmentManager;
        private readonly Context _context;
        private Supplier _itemClicked;

        public event EventHandler<int> ItemClick;
        public Filter Filter { get; }

        public SupplierAdapter(List<Supplier> suppliers, Context context, Android.Support.V4.App.FragmentManager fragmentManager) {
            OriginalSuppliers = suppliers;
            Suppliers = suppliers;
            _context = context;
            _fragmentManager = fragmentManager;
            Filter = new SupplierFilter(this);
        }

        public void ResetItems() {
            Suppliers = OriginalSuppliers;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            var supplierHolder = holder as SupplierView;

            if (supplierHolder == null) { return; }
            var item = Suppliers[position];

            var backgroundRes = item.ShipmentVouchers.Any() ? Resource.Drawable.background_border_supplier : Resource.Drawable.background_border;

            supplierHolder.SupplierName.Text = $"{item.Id} - {item.Name}";
            supplierHolder.SupplierName.Background = ContextCompat.GetDrawable(_context, backgroundRes);
            
            _shipmentVoucherAdapter = new ShipmentVoucherAdapter(item.ShipmentVouchers, item, _fragmentManager, _context);

            _shipmentVoucherAdapter.ItemClick += OnItemClick;
            supplierHolder.ShipmentVoucherRecyclerView.SetLayoutManager(new LinearLayoutManager(_context));
            supplierHolder.ShipmentVoucherRecyclerView.SetAdapter(_shipmentVoucherAdapter);
        }

        private void OnItemClick(object sender, int position) {

            var adapter = (ShipmentVoucherAdapter)sender;

            var supplier = adapter.CurrentSupplier;

            Android.Support.V4.App.Fragment fragment = FragmentShipmentVoucherDetail.NewInstance($"{supplier.Id} - {supplier.Name} | {supplier.ShipmentVouchers[position].Name}", supplier.ShipmentVouchers[position].Id);

            _fragmentManager.BeginTransaction()
                .Replace(Resource.Id.content_frame, fragment).AddToBackStack(typeof(FragmentShipmentVoucherDetail).ToString())
                .Commit();
        }

        private void OnClick(int position) {
            _itemClicked = Suppliers.ElementAt(position);
            ItemClick?.Invoke(this, position);
        }

        public Supplier ItemClicked() {
            return _itemClicked;
        }

        public void UpdateList(List<Supplier> suppliers) {
            Suppliers.Clear();
            Suppliers.AddRange(suppliers);
            NotifyDataSetChanged();
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
            var itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.cardSupplier, parent, false);

            // Create a ViewHolder to hold view references inside the CardView:
            var supplierHolder = new SupplierView(itemView, OnClick);
            return supplierHolder;
        }

        public override int ItemCount => Suppliers.Count;

        public class SupplierView : RecyclerView.ViewHolder {
            public View MainView { get; set; }
            public TextView SupplierName { get; set; }
            public RecyclerView ShipmentVoucherRecyclerView { get; set; }


            public SupplierView(View itemView, Action<int> listener) : base(itemView) {
                MainView = itemView;
                // Locate and cache view references:
                SupplierName = itemView.FindViewById<TextView>(Resource.Id.supplierName);
                ShipmentVoucherRecyclerView = (RecyclerView)itemView.FindViewById(Resource.Id.recyclerViewShipmentVoucher);
                itemView.Click += (sender, e) => listener(Position);
            }

        }

        /// <summary>
        /// Permet de filtrer les données via une recherche
        /// </summary>
        private class SupplierFilter : Filter {
            private readonly SupplierAdapter _adapter;
            private readonly List<Supplier> _filteredList;

            public SupplierFilter(SupplierAdapter adapter) {
                _adapter = adapter;
                _filteredList = new List<Supplier>();
            }

            protected override FilterResults PerformFiltering(ICharSequence constraint) {
                _filteredList.Clear();

                var results = new FilterResults();

                if (constraint.Length() == 0) {
                    _filteredList.AddRange(_adapter.OriginalSuppliers);
                } else {
                    var filterPattern = constraint.ToString().ToLower().Trim();
                    _filteredList.AddRange(_adapter.OriginalSuppliers.FindAll(it => it.Name.ToLower().Contains(filterPattern) || it.Id.ToLower().Contains(filterPattern)));
                }

                results.Values = FromArray(_filteredList.Select(r => r.ToJavaObject()).ToArray());
                results.Count = _filteredList.Count;

                return results;
            }

            protected override void PublishResults(ICharSequence constraint, FilterResults results) {
                using (var values = results.Values) {
                    _adapter.Suppliers = values.ToArray<Java.Lang.Object>()
                         .Select(r => r.ToNetObject<Supplier>()).ToList();
                }

                _adapter.NotifyDataSetChanged();
            }
        }

    }
}