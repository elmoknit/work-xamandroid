﻿using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using Object = Java.Lang.Object;

namespace Volcan.NanoApp.Entrepot.Models.Adapter {
    class CustomersTypeArrayAdapter : BaseAdapter {
        private readonly List<CustomersType> _customersTypes;
        private readonly Activity _context;

        public CustomersTypeArrayAdapter(Activity activity, List<CustomersType> customersTypes) {
            _context = activity;
            _customersTypes = customersTypes;
        }


        public override Object GetItem(int position) {
            return _customersTypes[position].Name;
        }

        public override long GetItemId(int position) {
            return _customersTypes[position].Id;
        }

        public override View GetView(int position, View convertView, ViewGroup parent) {
            var item = _customersTypes[position];
            var view = (convertView ?? _context.LayoutInflater.Inflate(Android.Resource.Layout.SimpleSpinnerDropDownItem,
                            parent,
                            false));
            var name = view.FindViewById<TextView>(Android.Resource.Id.Text1);
            name.Text = item.Name;
            return view;
        }

        public override int Count => _customersTypes.Count;
    }
}