﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Android.Graphics;
using Android.Support.V4.Content;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Webkit;
using Android.Widget;


namespace Volcan.NanoApp.Entrepot.Models.Adapter {
    class ScannedShipmentVoucherAdapter : RecyclerView.Adapter {
        private readonly List<ScannedShipmentVoucher> _scannedShipmentVouchers;
        private ScannedShipmentVoucher _selected;
        public event EventHandler<int> ItemClick;
        private readonly Context _context;

        public ScannedShipmentVoucherAdapter(Context context, List<ScannedShipmentVoucher> scannedShipmentVouchers) {
            _scannedShipmentVouchers = scannedShipmentVouchers;
            _context = context;

        }
        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            var scannedShipmentVoucherHolder = holder as ScannedShipmentVoucherView;
            if (scannedShipmentVoucherHolder == null) { return; }

            //Trouver l'objet dans la liste
            var item = _scannedShipmentVouchers[position];

            var backgroundRes = item.IsSelected ? Resource.Drawable.background_border_selected : Resource.Drawable.background_border; 

            //State par default
            scannedShipmentVoucherHolder.ScanStyleName.Text = "";
            scannedShipmentVoucherHolder.ScanStyleTotal.Text = "";
            scannedShipmentVoucherHolder.Loading.Visibility = ViewStates.Gone;
            scannedShipmentVoucherHolder.ScanStyleName.Visibility = ViewStates.Visible;
            scannedShipmentVoucherHolder.ScanStyleSize.Visibility = (item.Style == null) ? ViewStates.Gone : ViewStates.Visible;
            scannedShipmentVoucherHolder.ScanStyleTotal.Visibility = (item.Style == null) ? ViewStates.Gone : ViewStates.Visible;

            //Edit / Delete Statement
            if (item.Style != null) {
                //Application des variables
                scannedShipmentVoucherHolder.ScanStyleName.Text = item.Style.Name;
                scannedShipmentVoucherHolder.ScanStyleSize.Text = item.Style.Size;

                if (item.Style.Total > 1) {
                    var qt = $"x{item.Style.Total}";
                    scannedShipmentVoucherHolder.ScanStyleTotal.Text = qt;
                }
            }

            //Si nous avons un message d'erreur
            if (item.Error != null) { 
                var viewMessage = (item.Style == null) ? scannedShipmentVoucherHolder.ScanStyleName : scannedShipmentVoucherHolder.ScanStyleSize;

                backgroundRes = item.IsSelected ? Resource.Drawable.background_border_error_selected : Resource.Drawable.background_border_error;

                viewMessage.Text = item.Error.Message;
            }

            //Gestion de la bordure et fond
            scannedShipmentVoucherHolder.RelativeLayout.Background = ContextCompat.GetDrawable(_context, backgroundRes);

            //Afficher le loding si nécessaire
            if (item.IsPending && item.Error == null) {

                ShowLoading(scannedShipmentVoucherHolder);
            }


        }

        private void ShowLoading(ScannedShipmentVoucherView holder) {
            holder.Loading.Visibility = ViewStates.Visible;

            //scannedShipmentVoucherHolder.Wait.from
            holder.Loading.LoadUrl("file:///android_asset/Spinner.gif");
            // this makes it transparent so you can load it over a background
            //holder.Loading.SetBackgroundColor(Color.White);
            holder.Loading.SetLayerType(LayerType.Software, null);
        }

        private void OnClick(int position) {
            if (ItemClick != null) {
                var item = _scannedShipmentVouchers[position];

                _selected = null;
                var isSelected = !item.IsSelected;
                if (_scannedShipmentVouchers.Any(it => it.IsSelected)) {

                    foreach (var scannedShipmentVoucher in _scannedShipmentVouchers.Where(it => it.IsSelected)) {
                        var pos = _scannedShipmentVouchers.IndexOf(scannedShipmentVoucher);
                        scannedShipmentVoucher.IsSelected = false;
                        NotifyItemChanged(pos);
                        if (pos == position) {
                            isSelected = false;
                        }
                    }
                }

                item.IsSelected = isSelected;
                if (isSelected) {
                    _selected = item;
                }

                NotifyItemChanged(position);

                ItemClick?.Invoke(this, position);
            }
        }


        public ScannedShipmentVoucher GetSelectedItem() {
            return _selected;
        }
        public void UnselectItem() {
            _selected = null;
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
            var itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.cardScanShipmentVoucher, parent, false);

            // Create a ViewHolder to hold view references inside the CardView:
            var scannedShipmentVoucherHolder = new ScannedShipmentVoucherView(itemView, OnClick);
            return scannedShipmentVoucherHolder;
        }

        public override int ItemCount => _scannedShipmentVouchers.Count;

        public class ScannedShipmentVoucherView : RecyclerView.ViewHolder {
            public View MainView { get; set; }
            public TextView ScanStyleName { get; set; }
            public TextView ScanStyleTotal { get; set; }
            public TextView ScanStyleSize { get; set; }
            public RelativeLayout RelativeLayout { get; set; }

            public WebView Loading { get; set; }


            public ScannedShipmentVoucherView(View itemView, Action<int> listener) : base(itemView) {
                MainView = itemView;

                // Locate and cache view references:
                ScanStyleName = itemView.FindViewById<TextView>(Resource.Id.ScanStyleName);
                ScanStyleTotal = itemView.FindViewById<TextView>(Resource.Id.ScanStyleTotal);
                ScanStyleSize = itemView.FindViewById<TextView>(Resource.Id.scanStyleSize);
                RelativeLayout = itemView.FindViewById<RelativeLayout>(Resource.Id.scannedShipmentVoucherView);
                Loading = itemView.FindViewById<WebView>(Resource.Id.loadingScanShipmentVoucher);
                itemView.Click += (sender, e) => listener(Position);
            }

        }
    }
}