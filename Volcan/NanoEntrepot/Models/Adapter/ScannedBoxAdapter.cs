﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Android.Support.V4.Content;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Webkit;
using Android.Widget;
using Java.Lang;
using Volcan.NanoApp.Entrepot.Helpers;

namespace Volcan.NanoApp.Entrepot.Models.Adapter {
    class ScannedBoxAdapter : RecyclerView.Adapter, IFilterable {

        protected List<ScannedDelivery> ScannedBoxStyles;
        protected List<ScannedDelivery> OriginalScannedBoxStyles;
        private readonly Context _context;
        public event EventHandler<int> ItemClick;
        private ScannedDelivery _selected;
    

        public Filter Filter { get; }
        public ScannedBoxAdapter(Context context, List<ScannedDelivery> scannedBoxStyles) {
            ScannedBoxStyles = scannedBoxStyles;
            OriginalScannedBoxStyles = scannedBoxStyles;
            _context = context;

            Filter = new ScannedBoxFilter(this);
        }

        public void ResetItems() {
            ScannedBoxStyles = OriginalScannedBoxStyles;
        }
        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            var scannedBoxHolder = holder as ScannedBoxView;
            if (scannedBoxHolder == null) { return; }
            scannedBoxHolder.Loading.Visibility = ViewStates.Gone;
            //Trouver l'objet dans la liste
            var item = ScannedBoxStyles[position];

            var backgroundRes = item.IsSelected ? Resource.Drawable.background_border_selected : Resource.Drawable.background_border;

            //State par default
            scannedBoxHolder.ScanStyleName.Text = "";
            scannedBoxHolder.ScanStyleTotal.Text = "";
            scannedBoxHolder.ScanStyleName.Visibility = ViewStates.Visible;
            scannedBoxHolder.ScanStyleSize.Visibility = (item.Style == null) ? ViewStates.Gone : ViewStates.Visible;
            scannedBoxHolder.ScanStyleTotal.Visibility = (item.Style == null) ? ViewStates.Gone : ViewStates.Visible;

            //Edit / Delete Statement
            if (item.Style != null) {
                //Application des variables
                scannedBoxHolder.ScanStyleName.Text = item.Style.Name;
                scannedBoxHolder.ScanStyleSize.Text = item.Style.Size;

                if (item.Style.Total > 1) {
                    var qt = $"x{item.Style.Total}";
                    scannedBoxHolder.ScanStyleTotal.Text = qt;
                }
                if (item.Style.IsNeedPermission) {
                    backgroundRes = item.IsSelected ? Resource.Drawable.background_border_warning_selected : Resource.Drawable.background_border_warning;
                }

            }

            //Si nous avons un message d'erreur
            if (item.Error != null) {
                var viewMessage = (item.Style == null) ? scannedBoxHolder.ScanStyleName : scannedBoxHolder.ScanStyleSize;

                backgroundRes = item.IsSelected ? Resource.Drawable.background_border_error_selected : Resource.Drawable.background_border_error;

                viewMessage.Text = item.Error.Message;
            }

            
           

            //Gestion de la bordure et fond
            scannedBoxHolder.RelativeLayout.Background = ContextCompat.GetDrawable(_context, backgroundRes);

           

        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
            var itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.cardScanShipmentVoucher, parent, false);

            // Create a ViewHolder to hold view references inside the CardView:
            var scannedBoxHolder = new ScannedBoxView(itemView, OnClick);
            return scannedBoxHolder;
        }

        public override int ItemCount => ScannedBoxStyles.Count;

        public ScannedDelivery GetSelectedItem() {
            return _selected;
        }
        public void UnselectItem() {
            _selected = null;
        }

        private void OnClick(int position) {
            if (ItemClick != null) {
                var item = ScannedBoxStyles[position];

                _selected = null;
                var isSelected = !item.IsSelected;
                if (ScannedBoxStyles.Any(it => it.IsSelected)) {

                    foreach (var scannedBoxStyle in ScannedBoxStyles.Where(it => it.IsSelected)) {
                        var pos = ScannedBoxStyles.IndexOf(scannedBoxStyle);
                        scannedBoxStyle.IsSelected = false;
                        NotifyItemChanged(pos);
                        if (pos == position) {
                            isSelected = false;
                        }
                    }
                }

                item.IsSelected = isSelected;
                if (isSelected) {
                    _selected = item;
                }

                NotifyItemChanged(position);

                ItemClick?.Invoke(this, position);
            }
        }


        public class ScannedBoxView : RecyclerView.ViewHolder {
            public View MainView { get; set; }
            public TextView ScanStyleName { get; set; }
            public TextView ScanStyleTotal { get; set; }
            public TextView ScanStyleSize { get; set; }
            public RelativeLayout RelativeLayout { get; set; }
            public WebView Loading { get; set; }

            public ScannedBoxView(View itemView, Action<int> listener) : base(itemView) {
                MainView = itemView;

                // Locate and cache view references:
                ScanStyleName = itemView.FindViewById<TextView>(Resource.Id.ScanStyleName);
                ScanStyleTotal = itemView.FindViewById<TextView>(Resource.Id.ScanStyleTotal);
                ScanStyleSize = itemView.FindViewById<TextView>(Resource.Id.scanStyleSize);
                RelativeLayout = itemView.FindViewById<RelativeLayout>(Resource.Id.scannedShipmentVoucherView);
                Loading = itemView.FindViewById<WebView>(Resource.Id.loadingScanShipmentVoucher);
                itemView.Click += (sender, e) => listener(Position);
            }

        }

        /// <summary>
        /// Permet de filtrer les données via une recherche
        /// </summary>
        private class ScannedBoxFilter : Filter {
            private readonly ScannedBoxAdapter _adapter;
            private readonly List<ScannedDelivery> _filteredList;

            public ScannedBoxFilter(ScannedBoxAdapter adapter) {
                _adapter = adapter;
                _filteredList = new List<ScannedDelivery>();
            }

            protected override FilterResults PerformFiltering(ICharSequence constraint) {
                _filteredList.Clear();

                var results = new FilterResults();

                if (constraint.Length() == 0) {
                    _filteredList.AddRange(_adapter.OriginalScannedBoxStyles);
                } else {
                    var filterPattern = constraint.ToString().ToLower().Trim();
                    _filteredList.AddRange(_adapter.OriginalScannedBoxStyles.FindAll(it => it.Style.Name.ToLower().Contains(filterPattern)));
                }

                results.Values = FromArray(_filteredList.Select(r => r.ToJavaObject()).ToArray());
                results.Count = _filteredList.Count;

                return results;
            }

            protected override void PublishResults(ICharSequence constraint, FilterResults results) {
                using (var values = results.Values) {
                    _adapter.ScannedBoxStyles = values.ToArray<Java.Lang.Object>()
                        .Select(r => r.ToNetObject<ScannedDelivery>()).ToList();
                }

                _adapter.NotifyDataSetChanged();
            }
        }
    }
}