﻿using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using Object = Java.Lang.Object;

namespace Volcan.NanoApp.Entrepot.Models.Adapter {
    class SeasonArrayAdapter : BaseAdapter {
        private readonly List<Season> _seasons;
        private readonly Activity _context;

        public SeasonArrayAdapter(Activity activity, List<Season> seasons) {
            _context = activity;
            _seasons = seasons;
        }


        public override Object GetItem(int position) {
            return _seasons[position].Name;
        }

        public override long GetItemId(int position) {
            return _seasons[position].Id;
        }

        public override View GetView(int position, View convertView, ViewGroup parent) {
            var item = _seasons[position];
            var view = (convertView ?? _context.LayoutInflater.Inflate(Android.Resource.Layout.SimpleSpinnerDropDownItem,
                            parent,
                            false));
            var name = view.FindViewById<TextView>(Android.Resource.Id.Text1);
            name.Text = item.Name;
            return view;
        }

        public override int Count => _seasons.Count;
    }
}