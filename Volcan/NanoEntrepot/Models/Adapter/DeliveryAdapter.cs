﻿using System;
using System.Collections.Generic;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Volcan.NanoApp.Entrepot.Fragments;

namespace Volcan.NanoApp.Entrepot.Models.Adapter {
    public class DeliveryAdapter: RecyclerView.Adapter {

        private readonly List<Delivery> _deliveries;
        public Customer CurrentCustomer;
        private readonly Android.Support.V4.App.FragmentManager _fragmentManager;
        private int _pos = 0;
        public event EventHandler<int> ItemClick;

        public DeliveryAdapter(List<Delivery> deliveries, Customer currentCustomer, Android.Support.V4.App.FragmentManager fragmentManager) {
            _deliveries = deliveries;
            CurrentCustomer = currentCustomer;
            _fragmentManager = fragmentManager;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            var deliveryHolder = holder as DeliveryView;

            if (deliveryHolder == null) {return;}

            var item = _deliveries[position];
            deliveryHolder.DeliveryName.Text = item.Name;
            deliveryHolder.DeliveryEmployeName.Text = "";

            var employes = "";
            if (item.EmployeIds != null && item.EmployeIds.Count > 0) {
                for (int i = 0; i < item.EmployeIds.Count; i++) {
                    employes += $"{item.EmployeIds[i]}";
                    if (i != item.EmployeIds.Count - 1) {
                        employes += " - ";
                    }

                }
                deliveryHolder.DeliveryEmployeName.Text = $"{employes}";
            }

            var qt = $"{item.Total}/{item.ExpectedTotal}";
            deliveryHolder.DeliveryTotal.Text = qt;
        }

        public void DeliveryName_Click(object sender, EventArgs e) {

            Android.Support.V4.App.Fragment fragment = FragmentScanDelivery.NewInstance($"{CurrentCustomer.Name} | {_deliveries[_pos].Name}", CurrentCustomer.Id, _deliveries[_pos].Id);

            _fragmentManager.BeginTransaction()
                .Replace(Resource.Id.content_frame, fragment)
                .Commit();
        }

        public void OnClick(int position) {
            ItemClick?.Invoke(this, position);
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
            var itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.cardDelivery, parent, false);

            var deliveryHolder = new DeliveryView(itemView, CurrentCustomer, OnClick);
            return deliveryHolder;
        }

        public override int ItemCount => _deliveries.Count;

        public class DeliveryView : RecyclerView.ViewHolder {
            public View MainView { get; set; }
            public TextView DeliveryName { get; set; }
            public TextView DeliveryEmployeName { get; set; }
            public TextView DeliveryTotal { get; set; }
            public Customer Customer { get; set; }

            public DeliveryView(View itemView, Customer customer, Action<int> listener) : base(itemView) {
                MainView = itemView;
                Customer = customer;
                DeliveryName = itemView.FindViewById<TextView>(Resource.Id.deliveryName);
                DeliveryEmployeName = itemView.FindViewById<TextView>(Resource.Id.deliveryEmployeName);
                DeliveryTotal = itemView.FindViewById<TextView>(Resource.Id.deliveryTotal);

                itemView.Click += (sender, e) => listener(Position);
            }
        }
    }
}