﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.Content;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;

namespace Volcan.NanoApp.Entrepot.Models.Adapter {
    class ErrorAdapter: RecyclerView.Adapter {
        protected List<ErrorScan> ScanErrors;
        private readonly Context _context;
        public event EventHandler<int> ItemClick;

        public ErrorAdapter(Context context, List<ErrorScan> scanErrors) {
            ScanErrors = scanErrors;
            _context = context;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            var errorHolder = holder as ErrorView;
            if (errorHolder == null) { return; }
            
            //Trouver l'objet dans la liste
            var item = ScanErrors[position];

            var backgroundRes = Resource.Drawable.background_border_error;

           
            errorHolder.ErrorUPCSTYLE.Text = $"{item.Style.Upc}";
            errorHolder.ErrorName.Text = $"{item.Name}";
           
            if (item.Style.Name != null) {
                errorHolder.ErrorUPCSTYLE.Text = $"L'item {item.Style.Name}, {item.Style.Color}, {item.Style.Size}";
                }

          
            
            

            //Gestion de la bordure et fond
            errorHolder.RelativeLayout.Background = ContextCompat.GetDrawable(_context, backgroundRes);
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
            var itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.cardError, parent, false);

            // Create a ViewHolder to hold view references inside the CardView:
            var errorHolder = new ErrorView(itemView, OnClick);
            return errorHolder;
        }

        public override int ItemCount => ScanErrors.Count;

        private void OnClick(int position) {
            
            ItemClick?.Invoke(this, position);
        }
    }

    public class ErrorView : RecyclerView.ViewHolder {
        public View MainView { get; set; }
        public TextView ErrorUPCSTYLE { get; set; }
        public TextView ErrorName { get; set; }
        public RelativeLayout RelativeLayout { get; set; }

        public ErrorView(View itemView, Action<int> listener) : base(itemView) {
            MainView = itemView;

            // Locate and cache view references:
            ErrorName = itemView.FindViewById<TextView>(Resource.Id.ErrorName);
            ErrorUPCSTYLE = itemView.FindViewById<TextView>(Resource.Id.UPCStyle);
            RelativeLayout = itemView.FindViewById<RelativeLayout>(Resource.Id.rlErrorView);
            
            
            itemView.Click += (sender, e) => listener(Position);
        }

    }
}