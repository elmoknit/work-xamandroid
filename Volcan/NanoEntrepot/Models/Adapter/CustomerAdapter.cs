﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Android.Support.V7.Widget;
using Android.Support.V4.Content;
using Android.Views;
using Android.Widget;
using Java.Lang;
using Volcan.NanoApp.Entrepot.Fragments;
using Volcan.NanoApp.Entrepot.Helpers;

namespace Volcan.NanoApp.Entrepot.Models.Adapter {
    public class CustomerAdapter : RecyclerView.Adapter , IFilterable{

        protected List<Customer> Customers;
        protected readonly List<Customer> OriginalCustomers;
        private DeliveryAdapter _deliveryAdapter;
        private readonly Context _context;
        private readonly Android.Support.V4.App.FragmentManager _fragmentManager;
        public event EventHandler<int> ItemClick;
        private Customer _itemClicked;
        private MainActivity _ma;
        private Customer _currentCustomer;
        private Delivery _currentDelivery;
        public Filter Filter { get; }

        public CustomerAdapter(List<Customer> customers, Context context, Android.Support.V4.App.FragmentManager fragmentManager, MainActivity ma) {
            OriginalCustomers = customers;
            Customers = customers;
            _context = context;
            _fragmentManager = fragmentManager;
            _ma = ma;
            Filter = new CustomerFilter(this);
        }

        public void ResetItems() {
            Customers = OriginalCustomers;
        }

        public void ResetItemsAndSearch(string constaint) {
            ResetItems();
            this.Filter.InvokeFilter(constaint);
        }
        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            var customerHolder = holder as CustomerView;
            if (customerHolder == null) {return;}

            var item = Customers[position];
            customerHolder.CustomerEmployeName.Visibility = (item.IsIndividualDelivery) ? ViewStates.Invisible : ViewStates.Visible;
            //customerHolder.DeliveryRecyclerView.Visibility = (item.IsIndividualDelivery) ? ViewStates.Visible : ViewStates.Gone;
            customerHolder.DeliveryRelativeLayout.Visibility = (item.IsIndividualDelivery) ? ViewStates.Visible : ViewStates.Gone;

            customerHolder.CustomerName.Text = item.Name;
            customerHolder.CustomerEmployeName.Text = "";

            var employes = "";
            if (item.EmployeIds != null && item.EmployeIds.Count > 0) {
                for (int i = 0; i < item.EmployeIds.Count; i++) {
                    employes += $"{item.EmployeIds[i]}";
                    if (i != item.EmployeIds.Count - 1) {
                        employes += " - ";
                    }
                    
                }
               customerHolder.CustomerEmployeName.Text = $"{employes}";
            }
            

            var qt = $"{item.Total}/{item.ExpectedTotal}";

            var background = ContextCompat.GetDrawable(_context, Resource.Drawable.background_border);
            if (item.Total == item.ExpectedTotal) {
                background = ContextCompat.GetDrawable(_context, Resource.Drawable.background_border_good);
            }

            if (item.Total > item.ExpectedTotal) {

                background = ContextCompat.GetDrawable(_context, Resource.Drawable.background_border_warning);
            }

            if (item.IsIndividualDelivery) {
                if (item.Deliveries.Count > 0) {
                    background = ContextCompat.GetDrawable(_context, Resource.Drawable.background_border_supplier);
                }

               _deliveryAdapter = new DeliveryAdapter(item.Deliveries, item, _fragmentManager);
                _deliveryAdapter.ItemClick += OnItemClick;

                customerHolder.DeliveryRecyclerView.SetLayoutManager(new LinearLayoutManager(_context));
                customerHolder.DeliveryRecyclerView.SetAdapter(_deliveryAdapter);
            }

            customerHolder.CustomerTotal.Text = qt;

            customerHolder.RelativeLayout.Background = background;

        }
        private void OnItemClick(object sender, int position) {

            var adapter = (DeliveryAdapter)sender;
            _currentCustomer = adapter.CurrentCustomer;
            _currentDelivery = _currentCustomer.Deliveries[position];

            CheckEmployeId();
        }

        //À SORTIR EN HELPER UN JOUR  --> MÊME CODE OU PREQUE QUE FRAGMENT CUSTOMER
        //TODO PARTOUT
        private async void CheckEmployeId() {
            await CallApi.Get<EmployeIn>(_ma, $"/InventoryPackaging/GetUserForShipment?customerId={_currentCustomer.Id}&shipementId={_currentDelivery.Id}&seasonId={CallApi.CurrentSeason.Id}&cmdType={CallApi.CurrentCustomersTypes.Id}", SuccessCheckEmploye, ErrorCheckEmploye, UpdateCheckEmploye);
        }

        private void SuccessCheckEmploye(object obj) {
            if (obj == null) {
                return;
            }
            var newobj = (EmployeIn)obj;
            foreach (var newobjEmployeId in newobj.EmployeIds) {
                if (!_currentDelivery.EmployeIds.Contains(newobjEmployeId)) {
                    _currentDelivery.EmployeIds.Add(newobjEmployeId);
                }
            }
        }

        private void ErrorCheckEmploye(object obj, string msg) {
            var errorMessage = msg;
            if (obj != null) {
                var newObj = (EmployeIn)obj;
                var newmsg = Helper.FindGoodErrorMessage(newObj.Error.Code);
                errorMessage = !string.IsNullOrEmpty(newmsg) ? newmsg : msg;
            }

            _ma.CallDialogOkCancel(_ma.Resources.GetString(Resource.String.titleError), $"{_ma.Resources.GetString(Resource.String.readEmploye)} \n \n {errorMessage}", _ma.Resources.GetString(Resource.String.btnRetry), _ma.Resources.GetString(Resource.String.btnCancel), PositiveRetryCheckEmploye, Negative);
        }

        private void PositiveRetryCheckEmploye() {
            CheckEmployeId();
        }

        private void UpdateCheckEmploye() {
            ValidateCurrentEmploye();

        }

        private void ValidateCurrentEmploye() {
            if (_currentDelivery.EmployeIds.Count > 0 && !_currentDelivery.EmployeIds.Contains(CallApi.CurrentUser.OasisId)) {
                _ma.CallDialogPermission(_ma.Resources.GetString(Resource.String.employeAlreadyThereNeedApproval), PositiveCurrentEmploye, null);

                return;
            }
            if (!_currentDelivery.EmployeIds.Contains(CallApi.CurrentUser.OasisId)) {
                ChangeCurrentEmploye();
                return;
            }
            ChangeFragment();

        }

        private async void ChangeCurrentEmploye() {
            var userOut = new UserShipmentOut { CustomerId = _currentDelivery.CustomerId, ShipmentId = _currentDelivery.Id, EmployeId = CallApi.CurrentUser.OasisId, SeasonId = CallApi.CurrentSeason.Id, CmdType = CallApi.CurrentCustomersTypes.Id };

            await CallApi.Put<UserShipmentOut, UserShipmentOut>(_ma, $"InventoryPackaging/SaveUserForShipment", userOut, SuccessChangeEmploye, ErrorChangeEmploye, UpdateChangeEmploye);
        }

        private void SuccessChangeEmploye(object obj) {
            if (obj == null) {
                return;
            }
            var newobj = (UserShipmentOut)obj;

            _currentDelivery.EmployeIds.Add(newobj.EmployeId);
           
        }

        private void ErrorChangeEmploye(object obj, string msg) {

            _ma.CallDialogOkCancel(_ma.Resources.GetString(Resource.String.titleError), $"{_ma.Resources.GetString(Resource.String.updateEmploye)} \n \n {msg}", _ma.Resources.GetString(Resource.String.btnRetry), _ma.Resources.GetString(Resource.String.btnCancel), PositiveChangeEmploye, Negative);
        }

        private void PositiveChangeEmploye() {
            ChangeCurrentEmploye();
        }
        private void UpdateChangeEmploye() {
            //_customerAdapter.NotifyDataSetChanged();
            ChangeFragment();
        }
        private void PositiveCurrentEmploye() {
            ChangeCurrentEmploye();  
        }

        private void ChangeFragment() {

            Android.Support.V4.App.Fragment fragment = FragmentScanDelivery.NewInstance($"{_currentCustomer.Name} | {_currentDelivery.Name}", _currentCustomer.Id, _currentDelivery.Id);

            _fragmentManager.BeginTransaction()
                .Replace(Resource.Id.content_frame, fragment).AddToBackStack(typeof(FragmentScanDelivery).ToString())
                .Commit();

        }

        private void Negative() {
            _ma.HideLoader();
        }

        //JUSQUE ICI !!!!!

        private void OnClick(int position) {
            _itemClicked = Customers.ElementAt(position);
            ItemClick?.Invoke(this, position);
        }

        public Customer ItemClicked() {
            return _itemClicked;
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
            var itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.cardCustomer, parent, false);

            // Create a ViewHolder to hold view references inside the CardView:
            var customerHolder = new CustomerView(itemView, OnClick);
            return customerHolder;
        }

        public override int ItemCount => Customers.Count;

        public class CustomerView : RecyclerView.ViewHolder {
            public View MainView { get; set; }
            public TextView CustomerName { get; set; }
            public TextView CustomerEmployeName { get; set; }
            public TextView CustomerTotal { get; set; }

            public RecyclerView DeliveryRecyclerView { get; set; }
            public RelativeLayout DeliveryRelativeLayout { get; set; }
            public Customer CustomerCurrent { get; set; }

            public RelativeLayout RelativeLayout { get; set; }
            public CustomerView(View itemView, Action<int> listener) : base(itemView) {
                MainView = itemView;

                // Locate and cache view references:
                CustomerName = itemView.FindViewById<TextView>(Resource.Id.customerName);
                CustomerEmployeName = itemView.FindViewById<TextView>(Resource.Id.customerEmployeName);
                CustomerTotal = itemView.FindViewById<TextView>(Resource.Id.customerTotal);
                DeliveryRecyclerView = itemView.FindViewById<RecyclerView>(Resource.Id.recyclerViewDelivery);
                DeliveryRelativeLayout = itemView.FindViewById<RelativeLayout>(Resource.Id.rlDelivery);
                RelativeLayout = itemView.FindViewById<RelativeLayout>(Resource.Id.rlCustomers);

                itemView.Click += (sender, e) => listener(Position);
            }
        }
        /// <summary>
        /// Permet de filtrer les données via une recherche
        /// </summary>
        private class CustomerFilter : Filter {
            private readonly CustomerAdapter _adapter;
            private readonly List<Customer> _filteredList;

            public CustomerFilter(CustomerAdapter adapter) {
                _adapter = adapter;
                _filteredList = new List<Customer>();
            }

            protected override FilterResults PerformFiltering(ICharSequence constraint) {
                _filteredList.Clear();

                var results = new FilterResults();

                if (constraint.Length() == 0) {
                    _filteredList.AddRange(_adapter.OriginalCustomers);
                } else {
                    var filterPattern = constraint.ToString().ToLower().Trim();
                    _filteredList.AddRange(_adapter.OriginalCustomers.FindAll(it => it.Name.ToLower().Contains(filterPattern)));
                }

                results.Values = FromArray(_filteredList.Select(r => r.ToJavaObject()).ToArray());
                results.Count = _filteredList.Count;

                return results;
            }

            protected override void PublishResults(ICharSequence constraint, FilterResults results) {
                using (var values = results.Values) {
                    _adapter.Customers = values.ToArray<Java.Lang.Object>()
                        .Select(r => r.ToNetObject<Customer>()).ToList();
                }

                _adapter.NotifyDataSetChanged();
            }
        }
    }
}