﻿using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Android.Support.V4.Content;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Java.Lang;
using Volcan.NanoApp.Entrepot.Helpers;

namespace Volcan.NanoApp.Entrepot.Models.Adapter {
    class ShipmentVoucherDetailAdapter: RecyclerView.Adapter, IFilterable {
        protected List<Style> Styles;
        protected readonly List<Style> OriginalStyles;
        private readonly Context _context;
        public Filter Filter { get; }
        public ShipmentVoucherDetailAdapter(List<Style> styles, Context context) {
            OriginalStyles = styles;
            Styles = styles;
            _context = context;
            Filter = new ShipmentVoucherDetailFilter(this);
        }
        public void ResetItems() {
            Styles = OriginalStyles;
        }
        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            var shipmentVoucherDetailHolder = holder as ShipmentVoucherDetailView;

            if (shipmentVoucherDetailHolder == null) {return;}
            var item = Styles[position];
            shipmentVoucherDetailHolder.ShipmentVoucherStyleName.Text = item.Name;
            shipmentVoucherDetailHolder.ShipmentVoucherStyleSize.Text = item.Size;


            var qt = $"{item.Total}/{item.ExpectedTotal}";
            shipmentVoucherDetailHolder.ShipmentVoucherStyleTotal.Text = qt;

            var borderInt = ContextCompat.GetDrawable(_context, Resource.Drawable.background_border);

               
            if (item.Total == item.ExpectedTotal) {
                borderInt = ContextCompat.GetDrawable(_context, Resource.Drawable.background_border_good);
            }

            if (item.Total > item.ExpectedTotal) {

                borderInt = ContextCompat.GetDrawable(_context, Resource.Drawable.background_border_warning);
            }

            shipmentVoucherDetailHolder.RelativeLayout.Background = borderInt;
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
            var itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.cardShipmentVoucherDetail, parent, false);

            // Create a ViewHolder to hold view references inside the CardView:
            var shipmentVoucherDetailHolder = new ShipmentVoucherDetailView(itemView);
            return shipmentVoucherDetailHolder;
        }

        public override int ItemCount => Styles.Count;

        public class ShipmentVoucherDetailView : RecyclerView.ViewHolder {
            public View MainView { get; set; }
            public TextView ShipmentVoucherStyleName { get; set; }
            public TextView ShipmentVoucherStyleSize { get; set; }
            public TextView ShipmentVoucherStyleTotal { get; set; }
            public RelativeLayout RelativeLayout { get; set; }


            public ShipmentVoucherDetailView(View itemView) : base(itemView) {
                MainView = itemView;

                // Locate and cache view references:
                ShipmentVoucherStyleName = itemView.FindViewById<TextView>(Resource.Id.ShipmentVoucherStyleName);
                ShipmentVoucherStyleSize = itemView.FindViewById<TextView>(Resource.Id.shipmentVoucherStyleSize);
                ShipmentVoucherStyleTotal = itemView.FindViewById<TextView>(Resource.Id.shipmentVoucherStyleTotal);
                RelativeLayout = itemView.FindViewById<RelativeLayout>(Resource.Id.rlShipmentVoucherDetail);
            }

        }

        /// <summary>
        /// Permet de filtrer les données via une recherche
        /// </summary>
        private class ShipmentVoucherDetailFilter : Filter {
            private readonly ShipmentVoucherDetailAdapter _adapter;
            private readonly List<Style> _filteredList;

            public ShipmentVoucherDetailFilter(ShipmentVoucherDetailAdapter adapter) {
                _adapter = adapter;
                _filteredList = new List<Style>();
            }

            protected override FilterResults PerformFiltering(ICharSequence constraint) {
                _filteredList.Clear();

                var results = new FilterResults();

                if (constraint.Length() == 0) {
                    _filteredList.AddRange(_adapter.OriginalStyles);
                } else {
                    var filterPattern = constraint.ToString().ToLower().Trim();
                    _filteredList.AddRange(_adapter.OriginalStyles.FindAll(it => it.Name.ToLower().Contains(filterPattern)));
                }

                results.Values = FromArray(_filteredList.Select(r => r.ToJavaObject()).ToArray());
                results.Count = _filteredList.Count;

                return results;
            }

            protected override void PublishResults(ICharSequence constraint, FilterResults results) {
                using (var values = results.Values) {
                    _adapter.Styles = values.ToArray<Object>()
                        .Select(r => r.ToNetObject<Style>()).ToList();
                }

                _adapter.NotifyDataSetChanged();
            }
        }


    }
}