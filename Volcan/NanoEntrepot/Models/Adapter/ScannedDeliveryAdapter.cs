﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Android.Support.V4.Content;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Java.Lang;
using Volcan.NanoApp.Entrepot.Helpers;
using Object = Java.Lang.Object;

namespace Volcan.NanoApp.Entrepot.Models.Adapter {
    class ScannedDeliveryAdapter : RecyclerView.Adapter, IFilterable {
        protected List<ScannedDelivery> ScannedDeliveries;
        protected readonly List<ScannedDelivery> OriginalScannedDeliveries;
        private ScannedDelivery _selected;
        public event EventHandler<int> ItemClick;
        private readonly Context _context;

        public Filter Filter { get; }
        public ScannedDeliveryAdapter(Context context, List<ScannedDelivery> scannedDeliveries) {
            OriginalScannedDeliveries = scannedDeliveries;
            ScannedDeliveries = scannedDeliveries;
            _context = context;
            Filter = new ScannedDeliveryFilter(this);

        }
        public void ResetItems() {
            ScannedDeliveries = OriginalScannedDeliveries;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            var scannedDeliveryHolder = holder as ScannedDeliveryView;

            if (scannedDeliveryHolder == null) { return; }
            var item = ScannedDeliveries[position];

            scannedDeliveryHolder.ScanStyleName.Visibility = item.IsSelected ? ViewStates.Gone : ViewStates.Visible;
            scannedDeliveryHolder.ScanStyleSize.Visibility = item.IsSelected ? ViewStates.Gone : ViewStates.Visible;

            scannedDeliveryHolder.RackImage.Visibility = item.IsSelected ? ViewStates.Gone : ViewStates.Visible;
            scannedDeliveryHolder.RlRack.Visibility = item.IsSelected ? ViewStates.Gone : ViewStates.Visible;
            scannedDeliveryHolder.BoxImage.Visibility = item.IsSelected ? ViewStates.Gone : ViewStates.Visible;
            scannedDeliveryHolder.RlBox.Visibility = item.IsSelected ? ViewStates.Gone : ViewStates.Visible;
            scannedDeliveryHolder.ExpectedTotalImage.Visibility = item.IsSelected ? ViewStates.Gone : ViewStates.Visible;
            scannedDeliveryHolder.RlExpectedTotal.Visibility = item.IsSelected ? ViewStates.Gone : ViewStates.Visible;

            scannedDeliveryHolder.SelectedStyleName.Visibility = item.IsSelected ? ViewStates.Visible : ViewStates.Gone;
            scannedDeliveryHolder.SelectedStyleColor.Visibility = item.IsSelected ? ViewStates.Visible : ViewStates.Gone;
            scannedDeliveryHolder.SelectedStyleSize.Visibility = item.IsSelected ? ViewStates.Visible : ViewStates.Gone;
            
            scannedDeliveryHolder.SelectedRackImage.Visibility = item.IsSelected ? ViewStates.Visible : ViewStates.Gone;
            scannedDeliveryHolder.SelectedRlRack.Visibility = item.IsSelected ? ViewStates.Visible : ViewStates.Gone;
            scannedDeliveryHolder.SelectedBoxImage.Visibility = item.IsSelected ? ViewStates.Visible : ViewStates.Gone;
            scannedDeliveryHolder.SelectedRlBox.Visibility = item.IsSelected ? ViewStates.Visible : ViewStates.Gone;
            scannedDeliveryHolder.SelectedExpectedTotalImage.Visibility = item.IsSelected ? ViewStates.Visible : ViewStates.Gone;
            scannedDeliveryHolder.SelectedRlExpectedTotal.Visibility = item.IsSelected ? ViewStates.Visible : ViewStates.Gone;



            scannedDeliveryHolder.ScanStyleName.Text = item.Style.Name;
            scannedDeliveryHolder.ScanStyleSize.Text = item.Style.Size;
            scannedDeliveryHolder.RackTotal.Text = item.Style.Total.ToString();
            scannedDeliveryHolder.ExpectedTotalTotal.Text = item.Style.RealTotal.ToString();
            scannedDeliveryHolder.BoxTotal.Text = item.Style.BoxTotal.ToString();

            scannedDeliveryHolder.SelectedStyleName.Text = item.Style.Name;
            scannedDeliveryHolder.SelectedStyleColor.Text = item.Style.Color;
            scannedDeliveryHolder.SelectedStyleSize.Text = item.Style.Size;
            scannedDeliveryHolder.SelectedRackTotal.Text = item.Style.Total.ToString();
            scannedDeliveryHolder.SelectedExpectedTotalTotal.Text = item.Style.RealTotal.ToString();
            scannedDeliveryHolder.SelectedBoxTotal.Text = item.Style.BoxTotal.ToString();

            var backgroundRes = item.IsSelected ? Resource.Drawable.background_border_selected : Resource.Drawable.background_border;

            if (item.Style.Total + item.Style.BoxTotal == item.Style.ExpectedTotal) {
                backgroundRes = item.IsSelected ? Resource.Drawable.background_border_good_selected : Resource.Drawable.background_border_good;
            } else if (item.Style.Total + item.Style.BoxTotal > item.Style.ExpectedTotal) {
                backgroundRes = item.IsSelected ? Resource.Drawable.background_border_warning_selected : Resource.Drawable.background_border_warning;
            }

           
            scannedDeliveryHolder.RelativeLayout.Background = ContextCompat.GetDrawable(_context, backgroundRes);
        }

        public void PutBottomObject(ScannedDelivery obj) {
            var currentPost = ScannedDeliveries.FindIndex(it => it.Style.Upc == obj.Style.Upc);
            ScannedDeliveries.Remove(obj);
            ScannedDeliveries.Add(obj);
            NotifyItemMoved(currentPost, ScannedDeliveries.Count - 1);
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
            var itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.cardScanDelivery, parent, false);

            // Create a ViewHolder to hold view references inside the CardView:
            var scannedDeliveryHolder = new ScannedDeliveryView(itemView, OnClick);
            return scannedDeliveryHolder;
        }

        private void OnClick(int position) {
            if (ItemClick != null) {
                var item = ScannedDeliveries[position];

                _selected = null;
                var isSelected = !item.IsSelected;
                if (ScannedDeliveries.Any(it => it.IsSelected)) {

                    foreach (var scannedDelivery in ScannedDeliveries.Where(it => it.IsSelected)) {
                        var pos = ScannedDeliveries.IndexOf(scannedDelivery);
                        scannedDelivery.IsSelected = false;
                        NotifyItemChanged(pos);
                        if (pos == position) {
                            isSelected = false;
                        }
                    }
                }

                item.IsSelected = isSelected;
                if (isSelected) {
                    _selected = item;
                }

                NotifyItemChanged(position);

                ItemClick?.Invoke(this, position);
            }
        }

        public ScannedDelivery GetSelectedItem() {
            return _selected;
        }

        public override int ItemCount => ScannedDeliveries.Count;

        public class ScannedDeliveryView : RecyclerView.ViewHolder {
            public View MainView { get; set; }
            public TextView ScanStyleName { get; set; }
            public TextView ScanStyleSize { get; set; }

            public RelativeLayout RlRack { get; set; }
            public TextView RackTotal { get; set; }
            public ImageView RackImage { get; set; }

            public RelativeLayout RlBox { get; set; }
            public ImageView BoxImage { get; set; }
            public TextView BoxTotal { get; set; }

            public RelativeLayout RlExpectedTotal { get; set; }
            public ImageView ExpectedTotalImage { get; set; }
            public TextView ExpectedTotalTotal { get; set; }



            public TextView SelectedStyleName { get; set; }
            public TextView SelectedStyleColor { get; set; }
            public TextView SelectedStyleSize { get; set; }

            public RelativeLayout SelectedRlRack { get; set; }
            public ImageView SelectedRackImage { get; set; }
            public TextView SelectedRackTotal { get; set; }

            public RelativeLayout SelectedRlBox { get; set; }
            public ImageView SelectedBoxImage { get; set; }
            public TextView SelectedBoxTotal { get; set; }

            public RelativeLayout SelectedRlExpectedTotal { get; set; }
            public ImageView SelectedExpectedTotalImage { get; set; }
            public TextView SelectedExpectedTotalTotal { get; set; }


            public RelativeLayout RelativeLayout { get; set; }
            public ScannedDeliveryView(View itemView, Action<int> listener) : base(itemView) {
                MainView = itemView;

                // Locate and cache view references:
                ScanStyleName = itemView.FindViewById<TextView>(Resource.Id.ScanStyleNameDelivery);
                ScanStyleSize = itemView.FindViewById<TextView>(Resource.Id.scanStyleSizeDelivery);

                RlRack = itemView.FindViewById<RelativeLayout>(Resource.Id.rlRackNotSelected);
                RackTotal = itemView.FindViewById<TextView>(Resource.Id.RackTotalNotSelected);
                RackImage = itemView.FindViewById<ImageView>(Resource.Id.imgRackNotSelected);

                RlBox = itemView.FindViewById<RelativeLayout>(Resource.Id.rlBoxNotSelected);
                BoxImage = itemView.FindViewById<ImageView>(Resource.Id.imgBoxBlueNotSelected);
                BoxTotal = itemView.FindViewById<TextView>(Resource.Id.nbBoxNotSelected);

                RlExpectedTotal = itemView.FindViewById<RelativeLayout>(Resource.Id.rlExpectedTotalNotSelected);
                ExpectedTotalImage = itemView.FindViewById<ImageView>(Resource.Id.imgExpectedTotalNotSelected);
                ExpectedTotalTotal = itemView.FindViewById<TextView>(Resource.Id.ScanStyleTotalDeliveryNotSelected);

                SelectedStyleName = itemView.FindViewById<TextView>(Resource.Id.ScanStyleNameDeliverySelected);
                SelectedStyleColor = itemView.FindViewById<TextView>(Resource.Id.ScanStyleColorDeliverySelected);
                SelectedStyleSize = itemView.FindViewById<TextView>(Resource.Id.scanStyleSizeDeliverySelected);

                SelectedRlRack = itemView.FindViewById<RelativeLayout>(Resource.Id.rlRackSelected);
                SelectedRackImage = itemView.FindViewById<ImageView>(Resource.Id.imgRackSelected);
                SelectedRackTotal = itemView.FindViewById<TextView>(Resource.Id.RackTotalSelected);

                SelectedRlExpectedTotal = itemView.FindViewById<RelativeLayout>(Resource.Id.rlExpectedTotalSelected);
                SelectedExpectedTotalImage = itemView.FindViewById<ImageView>(Resource.Id.imgExpectedTotalSelected);
                SelectedExpectedTotalTotal = itemView.FindViewById<TextView>(Resource.Id.ScanStyleTotalDeliverySelected);

                SelectedRlBox = itemView.FindViewById<RelativeLayout>(Resource.Id.rlBoxSelected);
                SelectedBoxImage = itemView.FindViewById<ImageView>(Resource.Id.imgBoxBlueSelected);
                SelectedBoxTotal = itemView.FindViewById<TextView>(Resource.Id.nbBoxSelected);

                RelativeLayout = itemView.FindViewById<RelativeLayout>(Resource.Id.scannedDeliveryRL);

                itemView.Click += (sender, e) => listener(Position);
            }

        }

        /// <summary>
        /// Permet de filtrer les données via une recherche
        /// </summary>
        private class ScannedDeliveryFilter : Filter {
            private readonly ScannedDeliveryAdapter _adapter;
            private readonly List<ScannedDelivery> _filteredList;

            public ScannedDeliveryFilter(ScannedDeliveryAdapter adapter) {
                _adapter = adapter;
                _filteredList = new List<ScannedDelivery>();
            }

            protected override FilterResults PerformFiltering(ICharSequence constraint) {
                _filteredList.Clear();

                var results = new FilterResults();

                if (constraint.Length() == 0) {
                    _filteredList.AddRange(_adapter.OriginalScannedDeliveries);
                } else {
                    var filterPattern = constraint.ToString().ToLower().Trim();
                    _filteredList.AddRange(_adapter.OriginalScannedDeliveries.FindAll(it => it.Style.Name.ToLower().Contains(filterPattern)));
                }

                results.Values = FromArray(_filteredList.Select(r => r.ToJavaObject()).ToArray());
                results.Count = _filteredList.Count;

                return results;
            }

            protected override void PublishResults(ICharSequence constraint, FilterResults results) {
                using (var values = results.Values) {
                    _adapter.ScannedDeliveries = values.ToArray<Object>()
                        .Select(r => r.ToNetObject<ScannedDelivery>()).ToList();
                }

                _adapter.NotifyDataSetChanged();
            }
        }
    }
}