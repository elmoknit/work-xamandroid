﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Android.Support.V7.Widget;
using Android.Support.V4.Content;
using Android.Views;
using Android.Widget;
using Java.Lang;
using Volcan.NanoApp.Entrepot.Fragments;
using Volcan.NanoApp.Entrepot.Helpers;

namespace Volcan.NanoApp.Entrepot.Models.Adapter {
    public class CustomerPackingSlipAdapter : RecyclerView.Adapter , IFilterable{
        protected List<CustomerPackingSlip> CustomersPackingSlips;
        protected readonly List<CustomerPackingSlip> OriginalCustomersPackingSlips;
        private readonly Context _context;
        private readonly Android.Support.V4.App.FragmentManager _fragmentManager;
        public event EventHandler<int> ItemClick;
        private CustomerPackingSlip _itemClicked;
        
        public Filter Filter { get; }

        public CustomerPackingSlipAdapter(List<CustomerPackingSlip> customersPackingSlips, Context context, Android.Support.V4.App.FragmentManager fragmentManager) {
            OriginalCustomersPackingSlips = customersPackingSlips;
            CustomersPackingSlips = customersPackingSlips;
            _context = context;
            _fragmentManager = fragmentManager;
            Filter = new CustomerPackingSlipFilter(this);
        }

        public void ResetItems() {
            CustomersPackingSlips = OriginalCustomersPackingSlips;
        }
        
        public void ResetItemsAndSearch(string constaint) {
            ResetItems();
            this.Filter.InvokeFilter(constaint);
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            var customerPackingSlipHolder = holder as CustomerPackingSlipView;
            if (customerPackingSlipHolder == null) {return;}

            var item = CustomersPackingSlips[position];
            customerPackingSlipHolder.CustomerPackingSlipEmployeName.Visibility = (item.IsIndividualDelivery) ? ViewStates.Invisible : ViewStates.Visible;
           
            customerPackingSlipHolder.CustomerPackingSlipName.Text = item.Name;
            if (item.IsDelivery) {
                customerPackingSlipHolder.CustomerPackingSlipName.Text = $"\t {item.Name}";
            }

            customerPackingSlipHolder.CustomerPackingSlipEmployeName.Text = "";

            var employes = "";
            if (item.EmployeIds != null && item.EmployeIds.Count > 0) {
                for (int i = 0; i < item.EmployeIds.Count; i++) {
                    employes += $"{item.EmployeIds[i]}";
                    if (i != item.EmployeIds.Count - 1) {
                        employes += " - ";
                    }
                    
                }
               customerPackingSlipHolder.CustomerPackingSlipEmployeName.Text = $"{employes}";
            }
            

            var qt = $"{item.Total}/{item.ExpectedTotal}";

            var borderInt = ContextCompat.GetDrawable(_context, Resource.Drawable.background_border);
            if (item.Total == item.ExpectedTotal) {
                borderInt = ContextCompat.GetDrawable(_context, Resource.Drawable.background_border_good);
            }

            if (item.Total > item.ExpectedTotal) {

                borderInt = ContextCompat.GetDrawable(_context, Resource.Drawable.background_border_warning);
            }

            if (item.IsIndividualDelivery) {
                if (item.IsGreen) {
                    borderInt = ContextCompat.GetDrawable(_context, Resource.Drawable.background_border_supplier);
                }
            }
            customerPackingSlipHolder.CustomerPackingSlipTotal.Text = qt;

           

            customerPackingSlipHolder.RelativeLayout.Background = borderInt;

        }
        

        private void OnClick(int position) {
            _itemClicked = CustomersPackingSlips.ElementAt(position);
            ItemClick?.Invoke(this, position);
        }

        public CustomerPackingSlip ItemClicked() {
            return _itemClicked;
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
            var itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.cardCustomer, parent, false);

            // Create a ViewHolder to hold view references inside the CardView:
            var customerPackingSlipHolder = new CustomerPackingSlipView(itemView, OnClick);
            return customerPackingSlipHolder;
        }

        public override int ItemCount => CustomersPackingSlips.Count;

        public class CustomerPackingSlipView : RecyclerView.ViewHolder {
            public View MainView { get; set; }
            public TextView CustomerPackingSlipName { get; set; }
            public TextView CustomerPackingSlipEmployeName { get; set; }
            public TextView CustomerPackingSlipTotal { get; set; }

          
            public CustomerPackingSlip CustomerPackingSlipCurrent { get; set; }

            public RelativeLayout RelativeLayout { get; set; }

            public CustomerPackingSlipView(View itemView, Action<int> listener) : base(itemView) {
                MainView = itemView;

                // Locate and cache view references:
                CustomerPackingSlipName = itemView.FindViewById<TextView>(Resource.Id.customerName);
                CustomerPackingSlipEmployeName = itemView.FindViewById<TextView>(Resource.Id.customerEmployeName);
                CustomerPackingSlipTotal = itemView.FindViewById<TextView>(Resource.Id.customerTotal);
                RelativeLayout = itemView.FindViewById<RelativeLayout>(Resource.Id.rlCustomers);

                itemView.Click += (sender, e) => listener(Position);
            }
        }
        /// <summary>
        /// Permet de filtrer les données via une recherche
        /// </summary>
        private class CustomerPackingSlipFilter : Filter {
            private readonly CustomerPackingSlipAdapter _adapter;
            private readonly List<CustomerPackingSlip> _filteredList;

            public CustomerPackingSlipFilter(CustomerPackingSlipAdapter adapter) {
                _adapter = adapter;
                _filteredList = new List<CustomerPackingSlip>();
            }

            protected override FilterResults PerformFiltering(ICharSequence constraint) {
                _filteredList.Clear();

                var results = new FilterResults();

                if (constraint.Length() == 0) {
                    _filteredList.AddRange(_adapter.OriginalCustomersPackingSlips);
                } else {
                    var filterPattern = constraint.ToString().ToLower().Trim();
                    _filteredList.AddRange(_adapter.OriginalCustomersPackingSlips.FindAll(it => it.Name.ToLower().Contains(filterPattern)));
                }

                results.Values = FromArray(_filteredList.Select(r => r.ToJavaObject()).ToArray());
                results.Count = _filteredList.Count;

                return results;
            }

            protected override void PublishResults(ICharSequence constraint, FilterResults results) {
                using (var values = results.Values) {
                    _adapter.CustomersPackingSlips = values.ToArray<Java.Lang.Object>()
                        .Select(r => r.ToNetObject<CustomerPackingSlip>()).ToList();
                }

                _adapter.NotifyDataSetChanged();
            }
        }
    }
}