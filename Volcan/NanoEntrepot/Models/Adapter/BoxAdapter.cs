﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Android.Support.V4.Content;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Java.Lang;
using Volcan.NanoApp.Entrepot.Helpers;

namespace Volcan.NanoApp.Entrepot.Models.Adapter {
    class BoxAdapter: RecyclerView.Adapter, IFilterable {
        protected List<Box> Boxes;
        protected List<Box> OriginalBoxes;
        private readonly Context _context;
        public event EventHandler<int> ItemClick;
        private Box _itemClicked;

        public Filter Filter { get; }

        public BoxAdapter(Context context, List<Box> boxes) {
            OriginalBoxes = boxes;
            Boxes = boxes;
            _context = context;
            Filter = new BoxFilter(this);
        }

        public void ResetItems() {
            Boxes = OriginalBoxes;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            var boxHolder = holder as BoxView;

            if (boxHolder == null) { return; }
            var item = Boxes[position];

            boxHolder.Item = item;
            boxHolder.Position = position;

            boxHolder.BoxName.Text = $"Boîte #{item.BoxNumber}";
            var qt = 0;
            if (item.ScannedStyles != null ) {
                if (item.ScannedStyles.Count == 0) {
                    qt = item.Total;
                } else {
                    foreach (var style in item.ScannedStyles) {
                        qt += style.Style.Total;
                    }
                }
                
                
            }
            boxHolder.BoxQte.Text = qt.ToString();

            boxHolder.SelectedButton.Visibility = item.IsPrintable ? ViewStates.Visible : ViewStates.Gone;
            boxHolder.SelectedButton.Checked = item.IsSelectedForPrint ;
 
            var backgroundRes = item.IsClosed ? Resource.Drawable.background_border_good : Resource.Drawable.background_border;

            boxHolder.RelativeLayout.Background = ContextCompat.GetDrawable(_context, backgroundRes);
            boxHolder.Listener = OnClick;
        }

    

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
            var itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.cardBoxes, parent, false);

            // Create a ViewHolder to hold view references inside the CardView:
            var boxHolder = new BoxView(itemView);
            return boxHolder;
        }

        private void OnClick(int position) {
            _itemClicked = Boxes.ElementAt(position);
            ItemClick?.Invoke(this, position);
        }

        public Box ItemClicked() {
            return _itemClicked;
        }

        public override int ItemCount => Boxes.Count;

        public class BoxView : RecyclerView.ViewHolder {
            public View MainView { get; set; }
            public CheckBox SelectedButton { get; set; }
            public TextView BoxName { get; set; }
            public TextView BoxQte { get; set; }
            public new int Position { get; set; }

            public RelativeLayout RelativeLayout { get; set; }
            public Box Item { get; set; }
            public Action<int> Listener { get; set; }

            public BoxView(View itemView) : base(itemView) {
                MainView = itemView;

                // Locate and cache view references:
                BoxName = itemView.FindViewById<TextView>(Resource.Id.BoxName);
                BoxQte = itemView.FindViewById<TextView>(Resource.Id.BoxStyleQuantity);
                SelectedButton = itemView.FindViewById<CheckBox>(Resource.Id.printSelected);
                RelativeLayout = itemView.FindViewById<RelativeLayout>(Resource.Id.rlBox);
                //MainView.Click += (sender, e) => listener(Position);

                SelectedButton.CheckedChange += SelectedButton_CheckedChange;

                MainView.Click += MainView_Click;
            }

            private void MainView_Click(object sender, EventArgs e) {
                if (Item != null && Item.IsPrintable) {
                    SelectedButton.Checked = !Item.IsSelectedForPrint;
                } else {
                    Listener(Position);
                }
            }

            private void SelectedButton_CheckedChange(object sender, CompoundButton.CheckedChangeEventArgs e) {
                if (Item != null) {
                    Item.IsSelectedForPrint = ((CheckBox) sender).Checked;
                }
            }
        }

        /// <summary>
        /// Permet de filtrer les données via une recherche
        /// </summary>
        private class BoxFilter : Filter {
            private readonly BoxAdapter _adapter;
            private readonly List<Box> _filteredList;

            public BoxFilter(BoxAdapter adapter) {
                _adapter = adapter;
                _filteredList = new List<Box>();
            }

            protected override FilterResults PerformFiltering(ICharSequence constraint) {
                _filteredList.Clear();

                var results = new FilterResults();

                if (constraint.Length() == 0) {
                    _filteredList.AddRange(_adapter.OriginalBoxes);
                } else {
                    var filterPattern = constraint.ToString().ToLower().Trim();
                    _filteredList.AddRange(_adapter.OriginalBoxes.FindAll(it => it.BoxNumber.ToString().ToLower().Contains(filterPattern)));
                }

                results.Values = FromArray(_filteredList.Select(r => r.ToJavaObject()).ToArray());
                results.Count = _filteredList.Count;

                return results;
            }

            protected override void PublishResults(ICharSequence constraint, FilterResults results) {
                using (var values = results.Values) {
                    _adapter.Boxes = values.ToArray<Java.Lang.Object>()
                        .Select(r => r.ToNetObject<Box>()).ToList();
                }

                _adapter.NotifyDataSetChanged();
            }
        }
    }
}