﻿using System;
using System.Collections.Generic;
using Android.Content;
using Android.Support.V4.Content;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Volcan.NanoApp.Entrepot.Fragments;

namespace Volcan.NanoApp.Entrepot.Models.Adapter {
    class ShipmentVoucherAdapter : RecyclerView.Adapter {

        private readonly List<ShipmentVoucher> _shipmentVouchers;
        public Supplier CurrentSupplier;
        private int _pos = 0;
        private readonly Android.Support.V4.App.FragmentManager _fragmentManager;
        public event EventHandler<int> ItemClick;
        private readonly Context _context;

        public ShipmentVoucherAdapter(List<ShipmentVoucher> shipmentVouchers, Supplier currentSupplier, Android.Support.V4.App.FragmentManager fragmentManager, Context context) {
            _shipmentVouchers = shipmentVouchers;
            CurrentSupplier = currentSupplier;
            _fragmentManager = fragmentManager;
            _context = context;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position) {

            var shipmentVoucherHolder = holder as ShipmentVoucherView;

            if (shipmentVoucherHolder == null) {return;}
            var item = _shipmentVouchers[position];

            shipmentVoucherHolder.ShipmentVoucherName.Text = item.Name;
            shipmentVoucherHolder.ShipmentVoucherTotal.Text = $"{item.Total}/{item.ExpectedTotal}";

          
            var borderInt = ContextCompat.GetDrawable(_context, Resource.Drawable.background_border);


            if (item.Total == item.ExpectedTotal) {
                borderInt = ContextCompat.GetDrawable(_context, Resource.Drawable.background_border_good);
            }

            if (item.Total > item.ExpectedTotal) {

                borderInt = ContextCompat.GetDrawable(_context, Resource.Drawable.background_border_warning);
            }

            shipmentVoucherHolder.ShipmentVoucherRelativeLayout.Background = borderInt;
        }

        public void ShipmentVoucherName_Click(object sender, EventArgs e) {

            Android.Support.V4.App.Fragment fragment = FragmentShipmentVoucherDetail.NewInstance($"{CurrentSupplier.Id} - {CurrentSupplier.Name} | {_shipmentVouchers[_pos].Name}" , _shipmentVouchers[_pos].Id);

            _fragmentManager.BeginTransaction()
                .Replace(Resource.Id.content_frame, fragment)
                .Commit();
        }


        public void OnClick(int position) {
            ItemClick?.Invoke(this, position);
        }
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
            var itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.cardShipmentVoucher, parent, false);

            // Create a ViewHolder to hold view references inside the CardView:
            var shipmentVoucherHolder = new ShipmentVoucherView(itemView, CurrentSupplier, OnClick);

            return shipmentVoucherHolder;
        }

        public override int ItemCount => _shipmentVouchers.Count;

        public class ShipmentVoucherView : RecyclerView.ViewHolder {
            public View MainView { get; set; }
            public TextView ShipmentVoucherName { get; set; }
            public TextView ShipmentVoucherTotal { get; set; }
            public RelativeLayout ShipmentVoucherRelativeLayout { get; set; }

            public Supplier Supplier { get; set; }


            public ShipmentVoucherView(View itemView, Supplier supplier, Action<int> listener) : base(itemView) {
                MainView = itemView;
                Supplier = supplier;
                // Locate and cache view references:
                ShipmentVoucherName = itemView.FindViewById<TextView>(Resource.Id.shipmentVoucherName);
                ShipmentVoucherTotal = itemView.FindViewById<TextView>(Resource.Id.shipmentVoucherTotal);
                ShipmentVoucherRelativeLayout = itemView.FindViewById<RelativeLayout>(Resource.Id.shipmentVoucherView);

                itemView.Click += (sender, e) => listener(Position);
            }

        }
    }
}