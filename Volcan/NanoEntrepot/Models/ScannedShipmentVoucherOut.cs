﻿namespace Volcan.NanoApp.Entrepot.Models {
    public class ScannedShipmentVoucherOut : RecyclerViewItem {
        public long Timestamp { get; set; }
        public string Upc { get; set; }
        public int Total { get; set; }
        public string ShipmentVoucherId { get; set; }

    }
}