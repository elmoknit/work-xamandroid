﻿using System.Collections.Generic;

namespace Volcan.NanoApp.Entrepot.Models {
    public class BoxesIn : In {
        public List<Box> Boxes { get; set; }
    }
}