﻿using System.Collections.Generic;

namespace Volcan.NanoApp.Entrepot.Models {
    public class BoxOut : Box {
        public int SeasonId { get; set; }
        public string CustomerId { get; set; }
        public string ShipmentId { get; set; }
        public int CmdType { get; set; }
       
        public BoxOut() { }

        public BoxOut(Box box) {
            Id = box.Id;
            BoxNumber = box.BoxNumber;
            Dimension = box.Dimension;
            IsClosed = box.IsClosed;
            IsPrintable = box.IsPrintable;
            IsSelectedForPrint = box.IsSelectedForPrint;
            Timestamp = box.Timestamp;
            CmdType = 0;

            Styles = new List<Style>();
            box.ScannedStyles.ForEach(it=> Styles.Add(it.Style));

        }
    }
}