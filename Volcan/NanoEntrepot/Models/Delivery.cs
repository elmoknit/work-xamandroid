﻿using System.Collections.Generic;

namespace Volcan.NanoApp.Entrepot.Models {
    public class Delivery: Base {
        
        public List<string> EmployeIds { get; set; }
        public string CustomerId { get; set; }
        public List<Style> Styles { get; set; }

 
    }
}