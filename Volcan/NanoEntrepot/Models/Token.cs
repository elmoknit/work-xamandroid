﻿namespace Volcan.NanoApp.Entrepot.Models {
    public class Token {
        public string Value { get; set; }
        public string ValidTo { get; set; }
    }
}