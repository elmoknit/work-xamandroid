﻿
using System.Collections.Generic;


namespace Volcan.NanoApp.Entrepot.Models {
    class StylesOut :In {
        public int SeasonId { get; set; }
        public string CustomerId { get; set; }
        public string ShipmentId { get; set; }
        public List<Style> Styles { get; set; }

    }
}