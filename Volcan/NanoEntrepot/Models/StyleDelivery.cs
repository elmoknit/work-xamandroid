﻿namespace Volcan.NanoApp.Entrepot.Models {
    public class StyleDelivery : Style {
        public int RealTotal { get; set; }
        public int BoxTotal { get; set; }
        public bool IsNeedPermission { get; set; }
    }
}