﻿using System.Collections.Generic;

namespace Volcan.NanoApp.Entrepot.Models {
    public class Supplier {

        public string Id { get; set; }
        public string Name { get; set; }

        public List<ShipmentVoucher> ShipmentVouchers { get; set; }
        
    }
}