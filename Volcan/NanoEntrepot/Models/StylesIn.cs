﻿using System.Collections.Generic;

namespace Volcan.NanoApp.Entrepot.Models {
    public class StylesIn : In {

        public List<StyleDelivery> Styles { get; set; }
    }
}