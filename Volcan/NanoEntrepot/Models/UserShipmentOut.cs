﻿namespace Volcan.NanoApp.Entrepot.Models {
    public class UserShipmentOut {
        public string CustomerId { get; set; }      
        public string ShipmentId { get; set; }      
        public int SeasonId { get; set; }      
        public string EmployeId { get; set; }
        public Employe Employe { get; set; }
        public int CmdType { get; set; }
    }
}