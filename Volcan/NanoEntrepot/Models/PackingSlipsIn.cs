﻿using System.Collections.Generic;

namespace Volcan.NanoApp.Entrepot.Models {
    public class PackingSlipsIn : In {
        public string CustomerId { get; set; }
        public List<Delivery> PackingSlips { get; set; }
    }
}