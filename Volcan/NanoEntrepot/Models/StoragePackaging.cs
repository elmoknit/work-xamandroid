﻿using System.Collections.Generic;

namespace Volcan.NanoApp.Entrepot.Models {
    public class StoragePackaging {
        public string DeliveryId { get; set; }
        public string CustomerId { get; set; }
        public int CmdType { get; set; }
        public  List<ScannedDelivery> InternalScannedDeliveries { get; set; }
        public List<Box> InternalBoxes { get; set; }
    }
}