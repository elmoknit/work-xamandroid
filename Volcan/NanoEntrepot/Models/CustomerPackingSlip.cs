﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Volcan.NanoApp.Entrepot.Models {
    public class CustomerPackingSlip : Base {
        public string CustomerId { get; set; }
        public bool IsIndividualDelivery { get; set; }
        public bool IsGreen { get; set; }
        public List<string> EmployeIds { get; set; }
        public bool NeedsTrustedRole { get; set; }
        public bool IsDelivery { get; set; }
    }
}