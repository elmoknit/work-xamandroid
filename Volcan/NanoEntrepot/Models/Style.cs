﻿namespace Volcan.NanoApp.Entrepot.Models {
    public class Style : Base {
      
        public string Size { get; set; }
        public string Color { get; set; }
        public string ImagePath { get; set; }
        public string Description { get; set; }
        public string Upc { get; set; }
        public int SameCount { get; set; }
    }
}