﻿namespace Volcan.NanoApp.Entrepot.Models {
    public class Base {
        public string Id { get; set; }
        public string Name { get; set; }
        public int ExpectedTotal { get; set; }
        public int Total { get; set; }
  

    }
}