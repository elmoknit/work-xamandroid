﻿using System.Collections.Generic;

namespace Volcan.NanoApp.Entrepot.Models {
    public class User : In {
        public string FullName { get; set; }
        public string LastName { get; set; }
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string OasisId { get; set; }
        public string Id { get; set; }
        public List<string> Roles { get; set; }

    }
}