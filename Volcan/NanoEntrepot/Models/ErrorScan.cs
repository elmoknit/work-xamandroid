﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Volcan.NanoApp.Entrepot.Models {
    public class ErrorScan {
        public string Name { get; set; }
        public Style Style { get; set; }
        public TypeError TypeError { get; set; }
    }

    public enum TypeError {
        WrongUpc,
        AddItem,
        NotOnPackingSlip,
        Server
    }
}