﻿namespace Volcan.NanoApp.Entrepot.Models {
    public class ScannedShipmentVoucher : ScannedShipmentVoucherOut{
      
        public Style Style { get; set; }
        public int ServerTotal { get; set; }

        public ScannedShipmentVoucher() {
            ServerTotal = 0;
            Style = null;
        }
    }
}