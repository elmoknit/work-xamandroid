﻿
namespace Volcan.NanoApp.Entrepot.Models {
    public class DeliveryOut {
        public string CustomerId { get; set; }
        public string ShipmentId { get; set; }
        public int SeasonId { get; set; }
        public int CmdType { get; set; }

    }
}