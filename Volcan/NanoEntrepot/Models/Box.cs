﻿using System.Collections.Generic;

namespace Volcan.NanoApp.Entrepot.Models {
    public class Box {

        public int Id { get; set; }
        public long Timestamp { get; set; }
        public int BoxNumber { get; set; }
        public int Total { get; set; }
        public List<ScannedDelivery> ScannedStyles { get; set; }
        public bool IsNeedPermission { get; set; }
        public bool IsClosed { get; set; }
        public bool IsSelectedForPrint { get; set; }
        public bool IsPrintable { get; set; }
        public Dimension Dimension { get; set; }

       public List<Style> Styles { get; set; }


    }
}