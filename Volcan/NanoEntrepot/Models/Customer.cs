﻿using System.Collections.Generic;

namespace Volcan.NanoApp.Entrepot.Models {
    public class Customer : Base {

        public Employe Employe { get; set; }
        public List<string> EmployeIds { get; set; }
        public bool IsIndividualDelivery { get; set; }
        public List<Delivery> Deliveries { get; set; }
        public bool NeedsTrustedRole { get; set; }

        
        public int CountTotalWithDelivery() {
            var count = 0;
            if (Deliveries != null) {
                foreach (var delivery in Deliveries) {
                    count += delivery.ExpectedTotal;
                }
            }
            return count;
        }

        public int CountnbScannedObjectsWithDelivery() {
            var count = 0;
            if (Deliveries != null) {
                foreach (var delivery in Deliveries) {
                    count += delivery.Total;
                }
            }
            return count;
        }
    }
}