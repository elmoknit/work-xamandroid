using System.Threading;
using Android.App;
using Android.Content.PM;
using Android.Graphics.Drawables;
using Android.Media;
using Android.Net;
using Android.OS;
using Android.Support.V4.Widget;
using Android.Views;
using Volcan.NanoApp.Entrepot.Fragments;
using Android.Support.V7.App;
using Android.Support.V4.View;
using Android.Support.Design.Widget;
using Android.Views.InputMethods;
using Android.Widget;
using Volcan.NanoApp.Entrepot.Fragments.Dialog;
using Volcan.NanoApp.Entrepot.Helpers;
using Volcan.NanoApp.Entrepot.Models;
using HockeyApp.Android;
using HockeyApp.Android.Metrics;

using Java.Util;
using FragmentTransaction = Android.Support.V4.App.FragmentTransaction;

namespace Volcan.NanoApp.Entrepot {
    [Activity(Label = "@string/app_name", LaunchMode = LaunchMode.SingleTop, Icon = "@drawable/Icon", ConfigurationChanges = (ConfigChanges.Keyboard | ConfigChanges.KeyboardHidden | ConfigChanges.Orientation | ConfigChanges.ScreenSize | ConfigChanges.Navigation))]
    public class MainActivity : AppCompatActivity {

        private DrawerLayout _drawerLayout;
        public NavigationView NavigationView;
        private IMenuItem _previousItem;
        private Android.Support.V7.Widget.Toolbar _toolbar;
        private TextView _userNameNav;
        private int _oldPosition = -1;
        private ProgressDialog _progress;
        private MediaPlayer _player;

        private bool _isBackArrow = false;

        private IMenuItem _menuItemSupplier;
        private IMenuItem _menuItemCustomer;
        private IMenuItem _menuItemCustomerRepeat;

        //CRASH REPORTER
        protected override void OnResume() {
            base.OnResume();
            CrashManager.Register(this, "b09e5736572b4117ac3ebf9e0bda17e2");
        }

        public override void OnBackPressed() {
            if (_drawerLayout != null && _drawerLayout.IsDrawerOpen(GravityCompat.Start)) {
                _drawerLayout.CloseDrawer(GravityCompat.Start);
                return;
            }

            var fragment = GetFragment();
            if (fragment == null) {
                ListItemClicked(0, Resource.Id.nav_home);
                return;
            }

            if (fragment is FragmentHome) {
                CallDialogOkCancel(Resources.GetString(Resource.String.titleQuit), Resources.GetString(Resource.String.msgQuit), Resources.GetString(Resource.String.btnOk), Resources.GetString(Resource.String.btnCancel), PositiveQuit, null);

                return;
            }

            SupportFragmentManager.PopBackStack();

        }

        private void PositiveQuit() {
            CallApi.CurrentUser = null;
            CallApi.CurrentToken = null;
            CallApi.CurrentSeason = null;
            FinishAffinity();
        }
        protected override void OnCreate(Bundle savedInstanceState) {

            base.OnCreate(savedInstanceState);
            MetricsManager.Register(Application, "b09e5736572b4117ac3ebf9e0bda17e2");


            SetContentView(Resource.Layout.main);
            _toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            if (_toolbar != null) {
                SetSupportActionBar(_toolbar);
                SupportActionBar.SetDisplayHomeAsUpEnabled(true);
                SupportActionBar.SetHomeButtonEnabled(true);
            }

            _drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);

            //Set hamburger items menu
            SetHomeOnNavigation();

            //setup navigation view
            NavigationView = FindViewById<NavigationView>(Resource.Id.nav_view);

            _menuItemSupplier = NavigationView.Menu.FindItem(Resource.Id.nav_supplier);
            _menuItemCustomer = NavigationView.Menu.FindItem(Resource.Id.nav_customer);
            

            //handle navigation
            NavigationView.NavigationItemSelected += (sender, e) => {
                _previousItem?.SetChecked(false);

                
                //navigationView.SetCheckedItem(e.MenuItem.ItemId);

                _previousItem = e.MenuItem;
                int idMenuClicked;
                switch (e.MenuItem.ItemId) {
                    case Resource.Id.nav_customer:
                        idMenuClicked = 1;
                        break;
                    case Resource.Id.nav_supplier:
                        idMenuClicked = 2;
                        break;
                    case Resource.Id.nav_logout:
                        idMenuClicked = 4;
                        break;
                    default:
                        idMenuClicked = 0;
                        break;
                }

                ListItemClicked(idMenuClicked, e.MenuItem.ItemId);

                _drawerLayout.CloseDrawers();



            };


            //if first time you will want to go ahead and click first item.
            if (savedInstanceState == null) {

                NavigationView.SetCheckedItem(Resource.Id.nav_home);
                ListItemClicked(0, Resource.Id.nav_home);
            }
            _progress = new ProgressDialog(this) { Indeterminate = true };
            _progress.SetCancelable(false);
            _progress.SetMessage(Resources.GetString(Resource.String.progressMessage));
            //_progress.SetProgressDrawable((AnimationDrawable)Resource.Animation.animate_loader);
            CheckForUpdates();

            _player = MediaPlayer.Create(this, Resource.Raw.ComputerError);
        }

        private void CheckForUpdates() {
            // Remove this for store builds!
            UpdateManager.Register(this, "b09e5736572b4117ac3ebf9e0bda17e2");
        }

        private void UnregisterManagers() {
            UpdateManager.Unregister();
        }
        protected override void OnPause() {
            base.OnPause();
            UnregisterManagers();
        }

        protected override void OnDestroy() {
            base.OnDestroy();
            UnregisterManagers();
        }

        public void ListItemClicked(int position, int menuId) {
            _oldPosition = position;
            Android.Support.V4.App.Fragment fragment = null;
            var nameFragement = "";

            ChangeNavigationSelected(menuId);

            //interogg� le fragment manager pour trouv� Fragment

            switch (position) {
                case 0:
                    fragment = SupportFragmentManager.FindFragmentByTag("home") ?? FragmentHome.NewInstance();
                    nameFragement = "Home";
                    break;
                case 1:
                    fragment = SupportFragmentManager.FindFragmentByTag("customer") ?? FragmentCustomer.NewInstance(Resources.GetString(Resource.String.customers));
                    nameFragement = "Customer";

                    break;
                case 2:
                    fragment = SupportFragmentManager.FindFragmentByTag("supplier") ?? FragmentSupplier.NewInstance(Resources.GetString(Resource.String.rInventaire));
                    nameFragement = "Supplier";
                    break;
                case 4:
                    Logout();
                    fragment = FragmentHome.NewInstance();
                    nameFragement = "Home";

                    break;
            }

            ReplaceFragment(fragment, nameFragement);

        }

        public void ChangeNavigationSelected(int menuId) {
            NavigationView.SetCheckedItem(menuId);
        }

        public void LockDrawerLayout() {
            _drawerLayout.SetDrawerLockMode(DrawerLayout.LockModeLockedClosed);
        }

        public void ReplaceFragment(Android.Support.V4.App.Fragment frag, string nameFragment) {
            SupportFragmentManager.BeginTransaction()
                .Replace(Resource.Id.content_frame, frag, nameFragment.ToLower()).AddToBackStack(nameFragment.ToLower())
                .Commit();
        }

        public override bool OnCreateOptionsMenu(IMenu menu) {
            MenuInflater.Inflate(Resource.Menu.action_menu, menu);
            menu.FindItem(Resource.Id.back).SetVisible(false);
            menu.FindItem(Resource.Id.action_search).SetVisible(false);
            menu.FindItem(Resource.Id.menu_scan).SetVisible(false);
            menu.FindItem(Resource.Id.menu_box).SetVisible(false);
            menu.FindItem(Resource.Id.menu_print).SetVisible(false);
            menu.FindItem(Resource.Id.menu_add).SetVisible(false);
            menu.FindItem(Resource.Id.menu_done).SetVisible(false);

            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item) {
            switch (item.ItemId) {
                case Android.Resource.Id.Home:
                    if (_isBackArrow) {
                        return false;
                    }
                    _drawerLayout.OpenDrawer(GravityCompat.Start);
                    return true;
            }

            return base.OnOptionsItemSelected(item);
        }


        public void ShowLoader() {
            _progress.Show();
        }

        public void HideLoader() {
            _progress.Dismiss();
        }

        public void HideToolbar() {
            RequestWindowFeature(WindowFeatures.NoTitle);
        }

        public void HideKeyboard(InputMethodManager inputManager) {
            if (inputManager.IsActive) {
                inputManager.ToggleSoftInput(0, HideSoftInputFlags.NotAlways);
            }
        }

        public void ChangeToolbarTitle(string title, string subtitle = null) {
            if (_toolbar == null) {
                return;
            }

            _toolbar.Title = title;
            if (subtitle == null) {
                _toolbar.Subtitle = CallApi.CurrentToken != null ? CallApi.CurrentSeason.Name : "";
            } else {
                _toolbar.Subtitle = subtitle;
            }    
        }

        public void ChangeCurrentUser() {
            var fragment = GetFragment();
            (fragment as FragmentHome)?.ChangeHelloUser();
            ChangeUserMenu();
            ChangerNavigationMenuLoggin();
        }

        private Android.Support.V4.App.Fragment GetFragment() {
            return SupportFragmentManager.FindFragmentById(Resource.Id.content_frame);
        }

        public void ChangeUserMenu() {
            _userNameNav = FindViewById<TextView>(Resource.Id.navUser);
            _userNameNav.Text = CallApi.CurrentUser.Name;
        }

        public void Logout() {
            CallApi.CurrentUser = null;
            CallApi.CurrentToken = null;
            CallApi.CurrentSeason = null;
            NavigationView.SetCheckedItem(Resource.Id.nav_home);
        }

        public void ErrorSound() {
            _player.Start();
            
            

        }

        public void RelaseSound() {
            _player.Release();
      
        }

        public void CallDialogLogin(ApiCallbackLogin.SuccessDelegate success) {
            var ft = FragmentManager.BeginTransaction();
            var prev = FragmentManager.FindFragmentByTag("dialog");
            if (prev != null) {
                ft.Remove(prev);
            }
            ft.AddToBackStack(null);
            var dialogLogin = DialogFragmentLogin.NewInstance(null, success);

            dialogLogin.Cancelable = false;
            dialogLogin.Show(ft, "dialog");

        }

        public void CallDialogOkCancel(string title, string msg, string btnOk, string btnCancel, DialogCallbackButton.PositiveDelegate positive, DialogCallbackButton.NegativeDelegate negative, bool makeSound = true) {
         
            var ft = FragmentManager.BeginTransaction();

            var prev = FragmentManager.FindFragmentByTag("dialogOkCancel");

            if (prev != null) {
                DialogFragment df = (DialogFragment)prev;
                df.Dismiss();
            }

            ft.AddToBackStack(null);
            var dialogOkCancel = DialogFragmentOkCancel.NewInstance(title, msg, btnOk, btnCancel, positive, negative);

            dialogOkCancel.Cancelable = false;
            dialogOkCancel.Show(ft, "dialogOkCancel");

            if (makeSound) {
                ErrorSound();
            }
            
        }


        public void CallDialogValidate(string title, string msg, string key, DialogCallbackButton.PositiveDelegate positive, DialogCallbackButton.NegativeDelegate negative) {
            var ft = FragmentManager.BeginTransaction();
            var prev = FragmentManager.FindFragmentByTag("dialogValidate");
            if (prev != null) {
                DialogFragment df = (DialogFragment)prev;
                df.Dismiss();
            }

            ft.AddToBackStack(null);
            var dialogValidate = DialogFragmentValidateEditText.NewInstance(title, msg, key, positive, negative);

            dialogValidate.Cancelable = false;
            dialogValidate.Show(ft, "dialogValidate");
        }

        public void CallDialogDimension(ApiCallbackLogin.SuccessWithResultDelegate success, Dimension dimension) {
            var ft = FragmentManager.BeginTransaction();
            var prev = FragmentManager.FindFragmentByTag("dialog");
            if (prev != null) {
                DialogFragment df = (DialogFragment)prev;
                df.Dismiss();
            }
            ft.AddToBackStack(null);
            var dialogDimensions = DialogFragmentDimensions.NewInstance(null, success, dimension);

            dialogDimensions.Cancelable = true;
            dialogDimensions.Show(ft, "dialog");

        }

        public void CallDialogPermission(string msg, DialogCallbackButton.PositiveDelegate positive, DialogCallbackButton.NegativeDelegate negative) {
            var ft = FragmentManager.BeginTransaction();
            var prev = FragmentManager.FindFragmentByTag("dialog");
            if (prev != null) {
                DialogFragment df = (DialogFragment)prev;
                df.Dismiss();
            }
            ft.AddToBackStack(null);
            var dialogPermission = DialogFragmentPermission.NewInstance(msg, positive, negative);

            dialogPermission.Cancelable = false;
            dialogPermission.Show(ft, "dialog");

        }

        public void SetBackOnNavigation() {
            //Set hamburger items menu
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_arrow_back);
            _isBackArrow = true;

        }

        public void SetHomeOnNavigation() {
            //Set hamburger items menu
            _isBackArrow = false;
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_menu);
            
        }

        public void ChangerNavigationMenuLoggin() {
            if (CallApi.CurrentUser == null) { return; }

            if (CallApi.CurrentUser.Roles.Contains("Admin")) {
                _menuItemSupplier.SetVisible(true);
            } else if (CallApi.CurrentUser.Roles.Contains("Employe") || CallApi.CurrentUser.Roles.Contains("TrustedEmploye")) {
                _menuItemSupplier.SetVisible(false);
            } else {
                _menuItemSupplier.SetVisible(false);
                _menuItemCustomer.SetVisible(false);
                _menuItemCustomerRepeat.SetVisible(false);
            }
        }
    }

}
