﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Android.Content;
using Android.Net;
using Newtonsoft.Json;
using Volcan.NanoApp.Entrepot.Models;
using Uri = System.Uri;

namespace Volcan.NanoApp.Entrepot.Helpers {

    public static class CallApi {

        //private const string StartUri = "http://api.nano.cv.dsi.loc/api/";
        //private const string StartUri = "http://api-test.nano.dev.volcan.design:8092/api/";
        private const string StartUri = "https://api.nanocollection.com/api/";
        //private const string StartUri = "http://api-test.nano.cv.dsi.loc/api/";
        //private const string StartUri = "http://api-test.nano.mv.dsi.loc/api/";
        private const int WaitingTime = 100;
        public static string CurrentToken { get; set; }
        public static User CurrentUser { get; set; }
        public static Season CurrentSeason { get; set; }

        public static CustomersType CurrentCustomersTypes { get; set; }

        private static bool CheckConnectivity(MainActivity mainActivity, ApiCallback.ErrorDelegate error) {
            var connectivityManager = (ConnectivityManager)mainActivity.GetSystemService(Context.ConnectivityService);

            var networkInfo = connectivityManager?.ActiveNetworkInfo;
            if (networkInfo != null) {
                return true;
            }

            error(null, "Aucune connexion Internet");
            return false;
        }

        public static async Task GetToken(MainActivity mainActivity, string noEmp, string pwd, ApiCallback.SuccessDelegate success, ApiCallback.ErrorDelegate error, ApiCallback.UpdateDelegate update) {

            if (!CheckConnectivity(mainActivity, error)) {
                return;
            }

            mainActivity.ShowLoader();
            var authInfo = noEmp + ":" + pwd;
                authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));

                var client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authInfo);

                var uri = new Uri($"{StartUri}auth/GetToken?isForInternalUse=true", UriKind.Absolute);
                var content = new StringContent("");

                client.Timeout = TimeSpan.FromSeconds(WaitingTime);
                try {
                    var response = await client.PostAsync(uri, content);
                    await DoAction<Token>(mainActivity, response, success, error, update, null);

                } catch (Exception) {
                    error(null, "Problème de connexion");
                }
           

        }

        public static async Task GetUserRole(MainActivity mainActivity, string noEmp, string pwd, ApiCallback.SuccessDelegate success, ApiCallback.ErrorDelegate error, ApiCallback.UpdateDelegate update) {

            if (!CheckConnectivity(mainActivity, error)) {
                return;
            }

            mainActivity.ShowLoader();
            var authInfo = noEmp + ":" + pwd;
            authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));

            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authInfo);

            var uri = new Uri($"{StartUri}auth/GetUserInfo", UriKind.Absolute);
            var content = new StringContent("");

            client.Timeout = TimeSpan.FromSeconds(WaitingTime);
            try {
                var response = await client.PostAsync(uri, content);
                await DoAction<UserIn>(mainActivity, response, success, error, update, null);

            } catch (Exception) {
                error(null, "Problème de connexion");
            }


        }

        public static async Task Get<T>(MainActivity mainActivity, string endUri, ApiCallback.SuccessDelegate success, ApiCallback.ErrorDelegate error, ApiCallback.UpdateDelegate update, T objOut = default(T), bool showLoader = true) {

            if (!CheckConnectivity(mainActivity, error)) {
                return;
            }
            if (showLoader) {
                mainActivity.ShowLoader();
            }
           
            var client = BeginningClient();
            var uri = new Uri($"{StartUri}{endUri}", UriKind.Absolute);
            client.Timeout = TimeSpan.FromSeconds(WaitingTime);
            try {
                var response = await client.GetAsync(uri);

                await DoAction(mainActivity, response, success, error, update, objOut);
            } catch (Exception ex) {
                error(null, "Problème de connexion");
            }

        }

        public static async Task Put<T, TOut>(MainActivity mainActivity, string enduri, TOut objOut, ApiCallback.SuccessDelegate success, ApiCallback.ErrorDelegate error, ApiCallback.UpdateDelegate update, T obj = default(T)) {

            if (!CheckConnectivity(mainActivity, error)) {
                return;
            }

            var client = BeginningClient();
            var uri = new Uri($"{StartUri}{enduri}", UriKind.Absolute);

            var json = JsonConvert.SerializeObject(objOut);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            client.Timeout = TimeSpan.FromSeconds(WaitingTime);

            try {
                var response = await client.PutAsync(uri, content);
                await DoAction(mainActivity, response, success, error, update, obj);

            } catch (Exception) {
                error(objOut, "Problème de connexion");
            }


        }

        public static async Task Post<T, TOut>(MainActivity mainActivity, string enduri, TOut objOut, ApiCallback.SuccessDelegate success, ApiCallback.ErrorDelegate error, ApiCallback.UpdateDelegate update, T obj = default(T)) {

            if (!CheckConnectivity(mainActivity, error)) {
                return;
            }

            var client = BeginningClient();
            var uri = new Uri($"{StartUri}{enduri}", UriKind.Absolute);

            var json = JsonConvert.SerializeObject(objOut);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            client.Timeout = TimeSpan.FromSeconds(WaitingTime);
            try {
                var response = await client.PostAsync(uri, content);

                await DoAction(mainActivity, response, success, error, update, obj);
            } catch (Exception) {
                error(objOut, "Problème de connexion");
            }


        }

        public static async Task Delete<T>(MainActivity mainActivity, string enduri, T obj, ApiCallback.SuccessDelegate success, ApiCallback.ErrorDelegate error, ApiCallback.UpdateDelegate update) {

            if (!CheckConnectivity(mainActivity, error)) {
                return;
            }

            var client = BeginningClient();
            var uri = new Uri($"{StartUri}{enduri}", UriKind.Absolute);

            client.Timeout = TimeSpan.FromSeconds(WaitingTime);
            try {
                var response = await client.DeleteAsync(uri);

                await DoAction<T>(mainActivity, response, success, error, update, obj);
            } catch (Exception) {
                error(obj, "Problème de connexion");
            }
        }



        private static HttpClient BeginningClient() {

            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", CurrentToken);
            return client;
        }

        private static async Task DoAction<T>(MainActivity main, HttpResponseMessage response, ApiCallback.SuccessDelegate success, ApiCallback.ErrorDelegate error, ApiCallback.UpdateDelegate update, T objOut) {
            var responseContent = await response.Content.ReadAsStringAsync();
            var objIn = string.IsNullOrEmpty(responseContent) ? objOut : JsonConvert.DeserializeObject<T>(responseContent);

            switch (response.StatusCode) {
                case HttpStatusCode.OK:
                    success(objIn);
                    break;
                case HttpStatusCode.Unauthorized:
                    CurrentToken = null;
                    CurrentUser = null;
                    error(null, "Il y a une erreur dans votre nom d'usager ou votre mot de passe.");
                    main.HideLoader();
                    return;
                case HttpStatusCode.NotFound:
                    error(objIn, "Serveur API introuvable");
                    main.HideLoader();
                    return;
                case HttpStatusCode.BadRequest:
                    error(objIn, "Mauvaise requête");
                    main.HideLoader();
                    return;
                default:
                    error(objIn, response.ReasonPhrase);
                    main.HideLoader();
                    return;
            }
            main.HideLoader();
            update?.Invoke();

        }
        
    }
}

