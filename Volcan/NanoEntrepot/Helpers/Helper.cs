﻿using System;
using System.Net;
using Android.Content;
using Android.Graphics;
using Android.Net;

namespace Volcan.NanoApp.Entrepot.Helpers {
    public static class Helper {
        public static long GetTimestamp() {
            return new DateTimeOffset(DateTime.UtcNow, TimeSpan.Zero).ToUnixTimeMilliseconds();
        }

        public static bool CheckConnectivity(MainActivity mainActivity) {
            ConnectivityManager connectivityManager = (ConnectivityManager)mainActivity.GetSystemService(Context.ConnectivityService);
            NetworkInfo networkInfo = connectivityManager.ActiveNetworkInfo;
            return networkInfo != null;
        }

        public static Bitmap GetImageBitmapFromUrl(string url, MainActivity ma) {
            if (!CheckConnectivity(ma)) {
                return null;
            }
            Bitmap imageBitmap = null;
           
            using (var webClient = new WebClient()) {
                byte[] imageBytes;
                try {
                    imageBytes = webClient.DownloadData(url);
                } catch (WebException ex) {
                    imageBytes = webClient.DownloadData("https://static.nanocollection.com:444/assets/app/defaultimage.jpg");
                }

                if (imageBytes != null && imageBytes.Length > 0) {
                    imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                }
            }
            return imageBitmap;

        }

        public static string FindGoodErrorMessage(int i) {
            switch (i) {
                case 10:
                    //CodeError.DbErrorPrimaryKeyViolation
                    return "Erreur serveur #10";
                case 20:
                    //CodeError.DbErrorSaveError
                    return "Erreur serveur lors de la sauvegarde";
                case 30:
                    //CodeError.DbErrorOnDelete
                    return "Erreur serveur lors de la supression";
                case 40:
                    //CodeError.DbErrorForeignKeyViolation
                    return "Erreur serveur #40";
                case 90:
                    //CodeError.DbErrorUnknown
                    return "Erreur serveur inconnu (#90)";
                case 100:
                    //CodeError.RequiredFieldsMissing
                    return "Information manquante";
                case 110:
                    //CodeError.ItemNotFound
                    return "Item introuvable";
                case 500:
                    //CodeError.ScanUpcUnknown
                    return "Mauvais code UPC";
                case 510:
                    //CodeError.ScanUpcNotPartOfVoucher
                    return "Cet UPC n'est pas dans ce bon de réception";
                case 520:
                    //CodeError.ScanItemNotPartOfVoucher
                    return "Cet item n'est pas dans ce bon de réception";
                case 700:
                    //CodeError.SeasonNotFound
                    return "Saison introuvable";
                case 710:
                    //CodeError.EmployeNotFound
                    return "Employé introuvable";
                case 800:
                    //CodeError.EmailProblem;
                    return "Un problème est survenu lors de l'envoi du courriel";
                default:
                    return null;
            }

        }
    }
}