﻿using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PCLStorage;
using Volcan.NanoApp.Entrepot.Models;

namespace Volcan.NanoApp.Entrepot.Helpers {
    public static class StorageHelper {

        //public static StoragePackaging StoragePackaging { get; set; }

        public static string JsonSerialize(StoragePackaging storage) {
            return JsonConvert.SerializeObject(storage);
        }

        public static StoragePackaging JsonDesialize(string json) {
            return JsonConvert.DeserializeObject<StoragePackaging>(json);
        }

        public static bool IsFileExistAsync(string fileName, IFolder rootFolder = null) {
            // get hold of the file system
            var folder = rootFolder ?? FileSystem.Current.LocalStorage;
            var folderexist = folder.CheckExistsAsync(fileName).Result;
            // already run at least once, don't overwrite what's there
            return folderexist == ExistenceCheckResult.FileExists;
        }

        public static bool IsFolderExistAsync(string folderName, IFolder rootFolder = null) {
            // get hold of the file system
            var folder = rootFolder ?? FileSystem.Current.LocalStorage;
            var folderexist = folder.CheckExistsAsync(folderName).Result;
            // already run at least once, don't overwrite what's there
            return folderexist == ExistenceCheckResult.FolderExists;
        }

        public static IFolder CreateFolder(string folderName, IFolder rootFolder = null) {
            var folder = rootFolder ?? FileSystem.Current.LocalStorage;
            folder = folder.CreateFolderAsync(folderName, CreationCollisionOption.ReplaceExisting).Result;
            return folder;
        }

        public static IFile CreateFile(string filename, IFolder rootFolder = null) {
            var folder = rootFolder ?? FileSystem.Current.LocalStorage;
            var file = folder.CreateFileAsync(filename, CreationCollisionOption.ReplaceExisting).Result;
            return file;
        }

        public static async Task<bool> WriteTextAllAsync(string filename, string content = "", IFolder rootFolder = null) {
            var file = CreateFile(filename, rootFolder);
            if (file == null) {
                //TODO: Faire quelque chose si CreateFile pas fonctionné
                Console.WriteLine("CreateFile not working");
                return false;
            }

            var task = file.WriteAllTextAsync(content);
            await Task.WhenAll(task);

            return !task.IsFaulted;
        }

        public static string ReadAllTextAsync(string fileName, IFolder rootFolder = null) {
            var content = "";
            var folder = rootFolder ?? FileSystem.Current.LocalStorage;
            var exist = IsFileExistAsync(fileName, folder);
            if (!exist) {
                return content;
            }
            var file = folder.GetFileAsync(fileName).Result;
            content = file.ReadAllTextAsync().Result;
            //try {
                
            //} catch (AggregateException ae) {
            //    return ReadAllTextAsync(fileName, rootFolder);
            //}
            return content;
        }
        public static bool DeleteFile(string fileName, IFolder rootFolder = null) {
            var folder = rootFolder ?? FileSystem.Current.LocalStorage;
            var exist = IsFileExistAsync(fileName, folder);
            if (!exist) {
                return false;
            }
            var file = folder.GetFileAsync(fileName).Result;
            file.DeleteAsync();
            return true;
        }


    }
}