﻿namespace Volcan.NanoApp.Entrepot.Helpers {
    public class ApiCallback {
        public delegate void SuccessDelegate(object obj);
        public delegate void ErrorDelegate(object obj, string msg);
        public delegate void UpdateDelegate();
    }

    public class ApiCallbackLogin {
        public delegate void SuccessDelegate();
        public delegate void SuccessWithResultDelegate(object result);
        public delegate void ErrorDelegate();
    }
}